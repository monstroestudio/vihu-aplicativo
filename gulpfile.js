//////////////////////////////////////////////////
// REQUIRE
//////////////////////////////////////////////////

var gulp = require('gulp'),
    gutil = require('gulp-util'),
    bower = require('bower'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    minifyCss = require('gulp-minify-css'),
    prefix = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    watch = require('gulp-watch'),
    plumber = require('gulp-plumber'),
    sh = require('shelljs');

//////////////////////////////////////////////////
// PATHS
//////////////////////////////////////////////////

var paths = {
    assets: 'assets',
    assets_vendor: 'assets/vendor',
    styles: 'assets/styles',
    scripts: 'assets/scripts',
    public: 'www',
    bower: 'assets/bower_components'
};

var onError = function (err) {
    console.log(err.message);
    this.emit('end');
};

//////////////////////////////////////////////////
// CSS Tasks
//////////////////////////////////////////////////

// SASS BUILD
gulp.task('sass', function () {
    return gulp.src([
        paths.styles + '/styles.scss'
    ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass({
            paths: [
                paths.bower,
                paths.vendor,
                paths.styles
            ]
        }))
        .pipe(gulp.dest(paths.public + '/css'));
});

// DEVELOPMENT BUILD
gulp.task('css', ['sass'], function () {
    return gulp.src([
        paths.public + '/css/styles.css'
    ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(prefix('last 2 versions'))
        .pipe(gulp.dest(paths.public + '/css'));
});

//////////////////////////////////////////////////
// COPY ASSETS
//////////////////////////////////////////////////
gulp.task('assets:copy', function () {
    // FONTS
    gulp.src([
        paths.bower + '/ionic/fonts/**',
    ])
        .pipe(gulp.dest(paths.public + '/fonts'));
});

//////////////////////////////////////////////////
// JAVASCRIPT Tasks
//////////////////////////////////////////////////

// DEVELOPMENT BUILD
gulp.task('js:vendor', function () {
    return gulp.src([
        paths.bower + '/jqlite/jqlite.1.1.1.min.js',
        paths.bower + '/ionic/js/ionic.bundle.js',
        paths.bower + '/moment/min/moment.min.js',
        paths.bower + '/ionic-datepicker/dist/ionic-datepicker.bundle.min.js',
        paths.bower + '/angular-input-masks/angular-input-masks-standalone.min.js',
        paths.bower + '/angular-tooltips/dist/angular-tooltips.min.js',
        paths.bower + '/lodash/dist/lodash.js',
        'node_modules/ionic-native-transitions/dist/ionic-native-transitions.min.js'
    ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.public + '/js'));
});

gulp.task('js:dev', function () {
    return gulp.src([
        paths.scripts + '/**/*.js'
    ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(concat('app.js'))
        .pipe(gulp.dest(paths.public + '/js'));
});

gulp.task('js:prod', ['js:dev'], function () {
    return gulp.src([
        paths.scripts + '/js/app.js'
    ])
        .pipe(plumber({errorHandler: onError}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.public + '/js'));
});

//////////////////////////////////////////////////
// WATCH TASKS
//////////////////////////////////////////////////

gulp.task('watch', function () {
    gulp.watch(paths.styles + '/**/*.scss', function () {
        gulp.start('css')
    });

    gulp.watch(paths.scripts + '/**/*.js', function () {
        gulp.start('js:dev')
    });
});

gulp.task('watch:vendor', function () {
    gulp.watch(paths.assets_vendor + '/**/*.js', ['js:vendor']);
});

//////////////////////////////////////////////////
// BUILD TASKS
//////////////////////////////////////////////////

gulp.task('default', ['css', 'js:vendor', 'js:dev', 'watch']);
gulp.task('copy', ['assets:copy']);
gulp.task('vendor', ['watch:vendor']);