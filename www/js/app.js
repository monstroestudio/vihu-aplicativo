/**
 * ==================  angular-ios9-uiwebview.patch.js v1.1.1 ==================
 *
 * This patch works around iOS9 UIWebView regression that causes infinite digest
 * errors in Angular.
 *
 * The patch can be applied to Angular 1.2.0 – 1.4.5. Newer versions of Angular
 * have the workaround baked in.
 *
 * To apply this patch load/bundle this file with your application and add a
 * dependency on the "ngIOS9UIWebViewPatch" module to your main app module.
 *
 * For example:
 *
 * ```
 * angular.module('myApp', ['ngRoute'])`
 * ```
 *
 * becomes
 *
 * ```
 * angular.module('myApp', ['ngRoute', 'ngIOS9UIWebViewPatch'])
 * ```
 *
 *
 * More info:
 * - https://openradar.appspot.com/22186109
 * - https://github.com/angular/angular.js/issues/12241
 * - https://github.com/driftyco/ionic/issues/4082
 *
 *
 * @license AngularJS
 * (c) 2010-2015 Google, Inc. http://angularjs.org
 * License: MIT
 */

angular.module('ngIOS9UIWebViewPatch', ['ng']).config(['$provide', function($provide) {
  'use strict';

  $provide.decorator('$browser', ['$delegate', '$window', function($delegate, $window) {

    if (isIOS9UIWebView($window.navigator.userAgent)) {
      return applyIOS9Shim($delegate);
    }

    return $delegate;

    function isIOS9UIWebView(userAgent) {
      return /(iPhone|iPad|iPod).* OS 9_\d/.test(userAgent) && !/Version\/9\./.test(userAgent);
    }

    function applyIOS9Shim(browser) {
      var pendingLocationUrl = null;
      var originalUrlFn= browser.url;

      browser.url = function() {
        if (arguments.length) {
          pendingLocationUrl = arguments[0];
          return originalUrlFn.apply(browser, arguments);
        }

        return pendingLocationUrl || originalUrlFn.apply(browser, arguments);
      };

      window.addEventListener('popstate', clearPendingLocationUrl, false);
      window.addEventListener('hashchange', clearPendingLocationUrl, false);

      function clearPendingLocationUrl() {
        pendingLocationUrl = null;
      }

      return browser;
    }
  }]);
}]);
function clog(param) {
    console.warn(JSON.stringify(param, null, 2));
}

var app = angular.module('vihu',
    ['ionic',
        'ngCordova',
        'ionic-datepicker',
        'ngIOS9UIWebViewPatch',
        'ui.utils.masks',
        '720kb.tooltips',
        'components',
        'ionic-native-transitions'
    ]);



var geolocationSuccess = function (position) {
    console.log('Latitude: ' + position.coords.latitude + '\n' +
        'Longitude: ' + position.coords.longitude + '\n' +
        'Altitude: ' + position.coords.altitude + '\n' +
        'Accuracy: ' + position.coords.accuracy + '\n' +
        'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
        'Heading: ' + position.coords.heading + '\n' +
        'Speed: ' + position.coords.speed + '\n' +
        'Timestamp: ' + position.timestamp + '\n');
};

var geolocationError = function (error) {
    console.log('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
};

var device;
//var isMobile = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|Windows Phone)/);
var isMobile = navigator.userAgent.match(/(iPhone)/); //truque pra funcionar no Chrome

document.addEventListener('deviceready', function onDeviceReady() {
    device = device.uuid;
    // console.log(navigator.contacts.find);
}, true);

app.run(function ($ionicPlatform, $rootScope, $http, $ionicPopup, PanicService) {
    $ionicPlatform.ready(function () {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });

    $rootScope.generalPanicTypes = ["Chamar supervisor", "Assalto em andamento", "Problema de saúde"];
    $rootScope.visitorsSelected = [];
    $rootScope.accessVisitorsSelected = {};
    $rootScope.panicPress = false;

    window.rs = $rootScope;

    $rootScope.Panic = function (type) {
        $rootScope.panicPopup.close();

        console.log(type)

        // PanicService.call(type);


        // return;
        // $scope.$scope.panicPopup.close();

        // this.disabled = true;

        var user = JSON.parse(localStorage.getItem('user')).data;
        var panic = JSON.parse(localStorage.getItem('panic'));


        var amountPhones = [];

        angular.forEach(panic, function (_panic) {
            console.warn('Dentro do forEach, $rootScope.generalPanicTypes[type] = ', $rootScope.generalPanicTypes[type - 1])
            angular.forEach($filter('filter')(_panic.panictypes, {
                label: $rootScope.generalPanicTypes[type - 1],
                val: true
            }), function (panic) {
                amountPhones.push(panic)
            });
        });

        console.warn('amountPhones', amountPhones, 'length=>', amountPhones.length)


        if (panic.length == 0) {
            $rootScope.panicWarning();
            return;
        }


        if (amountPhones.length == 0) {
            $rootScope.panicWarning();
            return;
        }

        var phones = [];
        _.each(panic, function (obj) {
            phones.push({
                'phone': obj.phone,
                'actions': _.map(_.filter(obj.panictypes, {val: true}), 'label')
            });
        });


        var data = {
            userObj: user,
            user: user,
            panic: phones,
            action: 'panic'
        }

        $http({
            url: domain + '/panic/' + type,
            method: 'POST',
            data: data
        }).then(function (data) {

            console.log('Vim do webservice após pressionamento do botão de pânico', data);
            //if (typeof data === "object") {
            //    console.warn('Data received:', data.data);
            //    $ionicPopup.alert({
            //        title: 'O botão de pânico foi acionado. <br> Fique calmo(a)! Em breve, alguém entrará em contato com você.',
            //        cssClass: 'popStyle'
            //    });
            //}
        }, function (response) {
            //$ionicPopup.alert({
            //    title: 'Ocorreu um erro.',
            //    cssClass: 'popStyle'
            //});
        })

        console.log('BOTÃO DE PANICO CLICADO >' + type, user);

        return true;
    }
});

// var domain = 'http://vihu.api.monstroestudio.com.br/api';
// var domain = 'http://vihu.monstro.io/api';
var domain = 'http://api.vihuwebservice.info/api';
// var domain = 'http://vihuwebservices.app/api';


/*!
 * ngCordova
 * v0.1.20-alpha
 * Copyright 2015 Drifty Co. http://drifty.com/
 * See LICENSE in this repository for license information
 */
!function(){angular.module("ngCordova",["ngCordova.plugins"]),angular.module("ngCordova.plugins.actionSheet",[]).factory("$cordovaActionSheet",["$q","$window",function(e,n){return{show:function(t){var r=e.defer();return n.plugins.actionsheet.show(t,function(e){r.resolve(e)}),r.promise},hide:function(){return n.plugins.actionsheet.hide()}}}]),angular.module("ngCordova.plugins.adMob",[]).factory("$cordovaAdMob",["$q","$window",function(e,n){return{createBannerView:function(t){var r=e.defer();return n.plugins.AdMob.createBannerView(t,function(){r.resolve()},function(){r.reject()}),r.promise},createInterstitialView:function(t){var r=e.defer();return n.plugins.AdMob.createInterstitialView(t,function(){r.resolve()},function(){r.reject()}),r.promise},requestAd:function(t){var r=e.defer();return n.plugins.AdMob.requestAd(t,function(){r.resolve()},function(){r.reject()}),r.promise},showAd:function(t){var r=e.defer();return n.plugins.AdMob.showAd(t,function(){r.resolve()},function(){r.reject()}),r.promise},requestInterstitialAd:function(t){var r=e.defer();return n.plugins.AdMob.requestInterstitialAd(t,function(){r.resolve()},function(){r.reject()}),r.promise}}}]),angular.module("ngCordova.plugins.appAvailability",[]).factory("$cordovaAppAvailability",["$q",function(e){return{check:function(n){var t=e.defer();return appAvailability.check(n,function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise}}}]),angular.module("ngCordova.plugins.appRate",[]).provider("$cordovaAppRate",[function(){this.setPreferences=function(e){e&&angular.isObject(e)&&(AppRate.preferences.useLanguage=e.language||null,AppRate.preferences.displayAppName=e.appName||"",AppRate.preferences.promptAgainForEachNewVersion=e.promptForNewVersion||!0,AppRate.preferences.openStoreInApp=e.openStoreInApp||!1,AppRate.preferences.usesUntilPrompt=e.usesUntilPrompt||3,AppRate.preferences.useCustomRateDialog=e.useCustomRateDialog||!1,AppRate.preferences.storeAppURL.ios=e.iosURL||null,AppRate.preferences.storeAppURL.android=e.androidURL||null,AppRate.preferences.storeAppURL.blackberry=e.blackberryURL||null,AppRate.preferences.storeAppURL.windows8=e.windowsURL||null)},this.setCustomLocale=function(e){var n={title:"Rate %@",message:"If you enjoy using %@, would you mind taking a moment to rate it? It won’t take more than a minute. Thanks for your support!",cancelButtonLabel:"No, Thanks",laterButtonLabel:"Remind Me Later",rateButtonLabel:"Rate It Now"};n=angular.extend(n,e),AppRate.preferences.customLocale=n},this.$get=["$q",function(e){return{promptForRating:function(n){var t=e.defer(),r=AppRate.promptForRating(n);return t.resolve(r),t.promise},navigateToAppStore:function(){var n=e.defer(),t=AppRate.navigateToAppStore();return n.resolve(t),n.promise},onButtonClicked:function(e){AppRate.onButtonClicked=function(n){e.call(this,n)}},onRateDialogShow:function(e){AppRate.onRateDialogShow=e()}}}]}]),angular.module("ngCordova.plugins.appVersion",[]).factory("$cordovaAppVersion",["$q",function(e){return{getVersionNumber:function(){var n=e.defer();return cordova.getAppVersion.getVersionNumber(function(e){n.resolve(e)}),n.promise},getVersionCode:function(){var n=e.defer();return cordova.getAppVersion.getVersionCode(function(e){n.resolve(e)}),n.promise}}}]),angular.module("ngCordova.plugins.backgroundGeolocation",[]).factory("$cordovaBackgroundGeolocation",["$q","$window",function(e,n){return{init:function(){n.navigator.geolocation.getCurrentPosition(function(e){return e})},configure:function(t){this.init();var r=e.defer();return n.plugins.backgroundGeoLocation.configure(function(e){r.notify(e),n.plugins.backgroundGeoLocation.finish()},function(e){r.reject(e)},t),this.start(),r.promise},start:function(){var t=e.defer();return n.plugins.backgroundGeoLocation.start(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},stop:function(){var t=e.defer();return n.plugins.backgroundGeoLocation.stop(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise}}}]),angular.module("ngCordova.plugins.badge",[]).factory("$cordovaBadge",["$q",function(e){return{hasPermission:function(){var n=e.defer();return cordova.plugins.notification.badge.hasPermission(function(e){e?n.resolve(!0):n.reject("You do not have permission")}),n.promise},promptForPermission:function(){return cordova.plugins.notification.badge.promptForPermission()},set:function(n,t,r){var o=e.defer();return cordova.plugins.notification.badge.hasPermission(function(e){e?o.resolve(cordova.plugins.notification.badge.set(n,t,r)):o.reject("You do not have permission to set Badge")}),o.promise},get:function(){var n=e.defer();return cordova.plugins.notification.badge.hasPermission(function(e){e?cordova.plugins.notification.badge.get(function(e){n.resolve(e)}):n.reject("You do not have permission to get Badge")}),n.promise},clear:function(n,t){var r=e.defer();return cordova.plugins.notification.badge.hasPermission(function(e){e?r.resolve(cordova.plugins.notification.badge.clear(n,t)):r.reject("You do not have permission to clear Badge")}),r.promise},increase:function(n,t,r){var o=e.defer();return this.hasPermission().then(function(){o.resolve(cordova.plugins.notification.badge.increase(n,t,r))},function(){o.reject("You do not have permission to increase Badge")}),o.promise},decrease:function(n,t,r){var o=e.defer();return this.hasPermission().then(function(){o.resolve(cordova.plugins.notification.badge.decrease(n,t,r))},function(){o.reject("You do not have permission to decrease Badge")}),o.promise},configure:function(e){return cordova.plugins.notification.badge.configure(e)}}}]),angular.module("ngCordova.plugins.barcodeScanner",[]).factory("$cordovaBarcodeScanner",["$q",function(e){return{scan:function(n){var t=e.defer();return cordova.plugins.barcodeScanner.scan(function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise},encode:function(n,t){var r=e.defer();return n=n||"TEXT_TYPE",cordova.plugins.barcodeScanner.encode(n,t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise}}}]),angular.module("ngCordova.plugins.batteryStatus",[]).factory("$cordovaBatteryStatus",["$rootScope","$window","$timeout",function(e,n,t){var r=function(n){t(function(){e.$broadcast("$cordovaBatteryStatus:status",n)})},o=function(n){t(function(){e.$broadcast("$cordovaBatteryStatus:critical",n)})},i=function(n){t(function(){e.$broadcast("$cordovaBatteryStatus:low",n)})};return document.addEventListener("deviceready",function(){navigator.battery&&(n.addEventListener("batterystatus",r,!1),n.addEventListener("batterycritical",o,!1),n.addEventListener("batterylow",i,!1))},!1),!0}]).run(["$injector",function(e){e.get("$cordovaBatteryStatus")}]),angular.module("ngCordova.plugins.ble",[]).factory("$cordovaBLE",["$q","$timeout",function(e,n){return{scan:function(t,r){var o=e.defer();return ble.startScan(t,function(e){o.notify(e)},function(e){o.reject(e)}),n(function(){ble.stopScan(function(){o.resolve()},function(e){o.reject(e)})},1e3*r),o.promise},connect:function(n){var t=e.defer();return ble.connect(n,function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},disconnect:function(n){var t=e.defer();return ble.disconnect(n,function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},read:function(n,t,r){var o=e.defer();return ble.read(n,t,r,function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise},write:function(n,t,r,o){var i=e.defer();return ble.write(n,t,r,o,function(e){i.resolve(e)},function(e){i.reject(e)}),i.promise},writeCommand:function(n,t,r,o){var i=e.defer();return ble.writeCommand(n,t,r,o,function(e){i.resolve(e)},function(e){i.reject(e)}),i.promise},startNotification:function(n,t,r){var o=e.defer();return ble.startNotification(n,t,r,function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise},stopNotification:function(n,t,r){var o=e.defer();return ble.stopNotification(n,t,r,function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise},isConnected:function(n){var t=e.defer();return ble.isConnected(n,function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},isEnabled:function(){var n=e.defer();return ble.isEnabled(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise}}}]),angular.module("ngCordova.plugins.bluetoothSerial",[]).factory("$cordovaBluetoothSerial",["$q","$window",function(e,n){return{connect:function(t){var r=e.defer(),o=e.defer(),i=!1;return n.bluetoothSerial.connect(t,function(){i=!0,r.resolve(o)},function(e){i===!1&&o.reject(e),r.reject(e)}),r.promise},connectInsecure:function(t){var r=e.defer();return n.bluetoothSerial.connectInsecure(t,function(){r.resolve()},function(e){r.reject(e)}),r.promise},disconnect:function(){var t=e.defer();return n.bluetoothSerial.disconnect(function(){t.resolve()},function(e){t.reject(e)}),t.promise},list:function(){var t=e.defer();return n.bluetoothSerial.list(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},discoverUnpaired:function(){var t=e.defer();return n.bluetoothSerial.discoverUnpaired(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},setDeviceDiscoveredListener:function(){var t=e.defer();return n.bluetoothSerial.setDeviceDiscoveredListener(function(e){t.notify(e)}),t.promise},clearDeviceDiscoveredListener:function(){n.bluetoothSerial.clearDeviceDiscoveredListener()},showBluetoothSettings:function(){var t=e.defer();return n.bluetoothSerial.showBluetoothSettings(function(){t.resolve()},function(e){t.reject(e)}),t.promise},isEnabled:function(){var t=e.defer();return n.bluetoothSerial.isEnabled(function(){t.resolve()},function(){t.reject()}),t.promise},enable:function(){var t=e.defer();return n.bluetoothSerial.enable(function(){t.resolve()},function(){t.reject()}),t.promise},isConnected:function(){var t=e.defer();return n.bluetoothSerial.isConnected(function(){t.resolve()},function(){t.reject()}),t.promise},available:function(){var t=e.defer();return n.bluetoothSerial.available(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},read:function(){var t=e.defer();return n.bluetoothSerial.read(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},readUntil:function(t){var r=e.defer();return n.bluetoothSerial.readUntil(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},write:function(t){var r=e.defer();return n.bluetoothSerial.write(t,function(){r.resolve()},function(e){r.reject(e)}),r.promise},subscribe:function(t){var r=e.defer();return n.bluetoothSerial.subscribe(t,function(e){r.notify(e)},function(e){r.reject(e)}),r.promise},subscribeRawData:function(){var t=e.defer();return n.bluetoothSerial.subscribeRawData(function(e){t.notify(e)},function(e){t.reject(e)}),t.promise},unsubscribe:function(){var t=e.defer();return n.bluetoothSerial.unsubscribe(function(){t.resolve()},function(e){t.reject(e)}),t.promise},unsubscribeRawData:function(){var t=e.defer();return n.bluetoothSerial.unsubscribeRawData(function(){t.resolve()},function(e){t.reject(e)}),t.promise},clear:function(){var t=e.defer();return n.bluetoothSerial.clear(function(){t.resolve()},function(e){t.reject(e)}),t.promise},readRSSI:function(){var t=e.defer();return n.bluetoothSerial.readRSSI(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise}}}]),angular.module("ngCordova.plugins.brightness",[]).factory("$cordovaBrightness",["$q","$window",function(e,n){return{get:function(){var t=e.defer();return n.cordova?n.cordova.plugins.brightness.getBrightness(function(e){t.resolve(e)},function(e){t.reject(e)}):t.reject("Not supported without cordova.js"),t.promise},set:function(t){var r=e.defer();return n.cordova?n.cordova.plugins.brightness.setBrightness(t,function(e){r.resolve(e)},function(e){r.reject(e)}):r.reject("Not supported without cordova.js"),r.promise},setKeepScreenOn:function(t){var r=e.defer();return n.cordova?n.cordova.plugins.brightness.setKeepScreenOn(t,function(e){r.resolve(e)},function(e){r.reject(e)}):r.reject("Not supported without cordova.js"),r.promise}}}]),angular.module("ngCordova.plugins.calendar",[]).factory("$cordovaCalendar",["$q","$window",function(e,n){return{createCalendar:function(t){var r=e.defer(),o=n.plugins.calendar.getCreateCalendarOptions();return"string"==typeof t?o.calendarName=t:o=angular.extend(o,t),n.plugins.calendar.createCalendar(o,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},deleteCalendar:function(t){var r=e.defer();return n.plugins.calendar.deleteCalendar(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},createEvent:function(t){var r=e.defer(),o={title:null,location:null,notes:null,startDate:null,endDate:null};return o=angular.extend(o,t),n.plugins.calendar.createEvent(o.title,o.location,o.notes,new Date(o.startDate),new Date(o.endDate),function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},createEventWithOptions:function(t){var r=e.defer(),o=[],i=window.plugins.calendar.getCalendarOptions(),a={title:null,location:null,notes:null,startDate:null,endDate:null};o=Object.keys(a);for(var c in t)-1===o.indexOf(c)?i[c]=t[c]:a[c]=t[c];return n.plugins.calendar.createEventWithOptions(a.title,a.location,a.notes,new Date(a.startDate),new Date(a.endDate),i,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},createEventInteractively:function(t){var r=e.defer(),o={title:null,location:null,notes:null,startDate:null,endDate:null};return o=angular.extend(o,t),n.plugins.calendar.createEventInteractively(o.title,o.location,o.notes,new Date(o.startDate),new Date(o.endDate),function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},createEventInNamedCalendar:function(t){var r=e.defer(),o={title:null,location:null,notes:null,startDate:null,endDate:null,calendarName:null};return o=angular.extend(o,t),n.plugins.calendar.createEventInNamedCalendar(o.title,o.location,o.notes,new Date(o.startDate),new Date(o.endDate),o.calendarName,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},findEvent:function(t){var r=e.defer(),o={title:null,location:null,notes:null,startDate:null,endDate:null};return o=angular.extend(o,t),n.plugins.calendar.findEvent(o.title,o.location,o.notes,new Date(o.startDate),new Date(o.endDate),function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},listEventsInRange:function(t,r){var o=e.defer();return n.plugins.calendar.listEventsInRange(t,r,function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise},listCalendars:function(){var t=e.defer();return n.plugins.calendar.listCalendars(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},findAllEventsInNamedCalendar:function(t){var r=e.defer();return n.plugins.calendar.findAllEventsInNamedCalendar(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},modifyEvent:function(t){var r=e.defer(),o={title:null,location:null,notes:null,startDate:null,endDate:null,newTitle:null,newLocation:null,newNotes:null,newStartDate:null,newEndDate:null};return o=angular.extend(o,t),n.plugins.calendar.modifyEvent(o.title,o.location,o.notes,new Date(o.startDate),new Date(o.endDate),o.newTitle,o.newLocation,o.newNotes,new Date(o.newStartDate),new Date(o.newEndDate),function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},deleteEvent:function(t){var r=e.defer(),o={newTitle:null,location:null,notes:null,startDate:null,endDate:null};return o=angular.extend(o,t),n.plugins.calendar.deleteEvent(o.newTitle,o.location,o.notes,new Date(o.startDate),new Date(o.endDate),function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise}}}]),angular.module("ngCordova.plugins.camera",[]).factory("$cordovaCamera",["$q",function(e){return{getPicture:function(n){var t=e.defer();return navigator.camera?(navigator.camera.getPicture(function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise):(t.resolve(null),t.promise)},cleanup:function(){var n=e.defer();return navigator.camera.cleanup(function(){n.resolve()},function(e){n.reject(e)}),n.promise}}}]),angular.module("ngCordova.plugins.capture",[]).factory("$cordovaCapture",["$q",function(e){return{captureAudio:function(n){var t=e.defer();return navigator.device.capture?(navigator.device.capture.captureAudio(function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise):(t.resolve(null),t.promise)},captureImage:function(n){var t=e.defer();return navigator.device.capture?(navigator.device.capture.captureImage(function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise):(t.resolve(null),t.promise)},captureVideo:function(n){var t=e.defer();return navigator.device.capture?(navigator.device.capture.captureVideo(function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise):(t.resolve(null),t.promise)}}}]),angular.module("ngCordova.plugins.cardIO",[]).provider("$cordovaNgCardIO",[function(){var e=["card_type","redacted_card_number","card_number","expiry_month","expiry_year","short_expiry_year","cvv","zip"],n={expiry:!0,cvv:!0,zip:!1,suppressManual:!1,suppressConfirm:!1,hideLogo:!0};this.setCardIOResponseFields=function(n){n&&angular.isArray(n)&&(e=n)},this.setScanerConfig=function(e){e&&angular.isObject(e)&&(n.expiry=e.expiry||!0,n.cvv=e.cvv||!0,n.zip=e.zip||!1,n.suppressManual=e.suppressManual||!1,n.suppressConfirm=e.suppressConfirm||!1,n.hideLogo=e.hideLogo||!0)},this.$get=["$q",function(t){return{scanCard:function(){var r=t.defer();return CardIO.scan(n,function(n){if(null===n)r.reject(null);else{for(var t={},o=0,i=e.length;i>o;o++){var a=e[o];"short_expiry_year"===a?t[a]=String(n.expiry_year).substr(2,2)||"":t[a]=n[a]||""}r.resolve(t)}},function(){r.reject(null)}),r.promise}}}]}]),angular.module("ngCordova.plugins.clipboard",[]).factory("$cordovaClipboard",["$q","$window",function(e,n){return{copy:function(t){var r=e.defer();return n.cordova.plugins.clipboard.copy(t,function(){r.resolve()},function(){r.reject()}),r.promise},paste:function(){var t=e.defer();return n.cordova.plugins.clipboard.paste(function(e){t.resolve(e)},function(){t.reject()}),t.promise}}}]),angular.module("ngCordova.plugins.contacts",[]).factory("$cordovaContacts",["$q",function(e){return{save:function(n){var t=e.defer(),r=navigator.contacts.create(n);return r.save(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},remove:function(n){var t=e.defer(),r=navigator.contacts.create(n);return r.remove(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},clone:function(e){var n=navigator.contacts.create(e);return n.clone(e)},find:function(n){var t=e.defer(),r=n.fields||["id","displayName"];return delete n.fields,0===Object.keys(n).length?navigator.contacts.find(r,function(e){t.resolve(e)},function(e){t.reject(e)}):navigator.contacts.find(r,function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise},pickContact:function(){var n=e.defer();return navigator.contacts.pickContact(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise}}}]),angular.module("ngCordova.plugins.datePicker",[]).factory("$cordovaDatePicker",["$window","$q",function(e,n){return{show:function(t){var r=n.defer();return t=t||{date:new Date,mode:"date"},e.datePicker.show(t,function(e){r.resolve(e)}),r.promise}}}]),angular.module("ngCordova.plugins.device",[]).factory("$cordovaDevice",[function(){return{getDevice:function(){return device},getCordova:function(){return device.cordova},getModel:function(){return device.model},getName:function(){return device.name},getPlatform:function(){return device.platform},getUUID:function(){return device.uuid},getVersion:function(){return device.version},getManufacturer:function(){return device.manufacturer}}}]),angular.module("ngCordova.plugins.deviceMotion",[]).factory("$cordovaDeviceMotion",["$q",function(e){return{getCurrentAcceleration:function(){var n=e.defer();return(angular.isUndefined(navigator.accelerometer)||!angular.isFunction(navigator.accelerometer.getCurrentAcceleration))&&n.reject("Device do not support watchAcceleration"),navigator.accelerometer.getCurrentAcceleration(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise},watchAcceleration:function(n){var t=e.defer();(angular.isUndefined(navigator.accelerometer)||!angular.isFunction(navigator.accelerometer.watchAcceleration))&&t.reject("Device do not support watchAcceleration");var r=navigator.accelerometer.watchAcceleration(function(e){t.notify(e)},function(e){t.reject(e)},n);return t.promise.cancel=function(){navigator.accelerometer.clearWatch(r)},t.promise.clearWatch=function(e){navigator.accelerometer.clearWatch(e||r)},t.promise.watchID=r,t.promise},clearWatch:function(e){return navigator.accelerometer.clearWatch(e)}}}]),angular.module("ngCordova.plugins.deviceOrientation",[]).factory("$cordovaDeviceOrientation",["$q",function(e){var n={frequency:3e3};return{getCurrentHeading:function(){var n=e.defer();return navigator.compass?(navigator.compass.getCurrentHeading(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise):(n.reject("No compass on Device"),n.promise)},watchHeading:function(t){var r=e.defer();if(!navigator.compass)return r.reject("No compass on Device"),r.promise;var o=angular.extend(n,t),i=navigator.compass.watchHeading(function(e){r.notify(e)},function(e){r.reject(e)},o);return r.promise.cancel=function(){navigator.compass.clearWatch(i)},r.promise.clearWatch=function(e){navigator.compass.clearWatch(e||i)},r.promise.watchID=i,r.promise},clearWatch:function(e){return navigator.compass.clearWatch(e)}}}]),angular.module("ngCordova.plugins.dialogs",[]).factory("$cordovaDialogs",["$q","$window",function(e,n){return{alert:function(t,r,o){var i=e.defer();return n.navigator.notification?navigator.notification.alert(t,function(){i.resolve()},r,o):(n.alert(t),i.resolve()),i.promise},confirm:function(t,r,o){var i=e.defer();return n.navigator.notification?navigator.notification.confirm(t,function(e){i.resolve(e)},r,o):n.confirm(t)?i.resolve(1):i.resolve(2),i.promise},prompt:function(t,r,o,i){var a=e.defer();if(n.navigator.notification)navigator.notification.prompt(t,function(e){a.resolve(e)},r,o,i);else{var c=n.prompt(t,i);null!==c?a.resolve({input1:c,buttonIndex:1}):a.resolve({input1:c,buttonIndex:2})}return a.promise},beep:function(e){return navigator.notification.beep(e)}}}]),angular.module("ngCordova.plugins.emailComposer",[]).factory("$cordovaEmailComposer",["$q",function(e){return{isAvailable:function(){var n=e.defer();return cordova.plugins.email.isAvailable(function(e){e?n.resolve():n.reject()}),n.promise},open:function(n){var t=e.defer();return cordova.plugins.email.open(n,function(){t.reject()}),t.promise},addAlias:function(e,n){cordova.plugins.email.addAlias(e,n)}}}]),angular.module("ngCordova.plugins.facebook",[]).provider("$cordovaFacebook",[function(){this.browserInit=function(e,n){this.appID=e,this.appVersion=n||"v2.0",facebookConnectPlugin.browserInit(this.appID,this.appVersion)},this.$get=["$q",function(e){return{login:function(n){var t=e.defer();return facebookConnectPlugin.login(n,function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},showDialog:function(n){var t=e.defer();return facebookConnectPlugin.showDialog(n,function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},api:function(n,t){var r=e.defer();return facebookConnectPlugin.api(n,t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},getAccessToken:function(){var n=e.defer();return facebookConnectPlugin.getAccessToken(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise},getLoginStatus:function(){var n=e.defer();return facebookConnectPlugin.getLoginStatus(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise},logout:function(){var n=e.defer();return facebookConnectPlugin.logout(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise}}}]}]),angular.module("ngCordova.plugins.facebookAds",[]).factory("$cordovaFacebookAds",["$q","$window",function(e,n){return{setOptions:function(t){var r=e.defer();return n.FacebookAds.setOptions(t,function(){r.resolve()},function(){r.reject()}),r.promise},createBanner:function(t){var r=e.defer();return n.FacebookAds.createBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},removeBanner:function(){var t=e.defer();return n.FacebookAds.removeBanner(function(){t.resolve()},function(){t.reject()}),t.promise},showBanner:function(t){var r=e.defer();return n.FacebookAds.showBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},showBannerAtXY:function(t,r){var o=e.defer();return n.FacebookAds.showBannerAtXY(t,r,function(){o.resolve()},function(){o.reject()}),o.promise},hideBanner:function(){var t=e.defer();return n.FacebookAds.hideBanner(function(){t.resolve()},function(){t.reject()}),t.promise},prepareInterstitial:function(t){var r=e.defer();return n.FacebookAds.prepareInterstitial(t,function(){r.resolve()},function(){r.reject()}),r.promise},showInterstitial:function(){var t=e.defer();return n.FacebookAds.showInterstitial(function(){t.resolve()},function(){t.reject()}),t.promise}}}]),angular.module("ngCordova.plugins.file",[]).constant("$cordovaFileError",{1:"NOT_FOUND_ERR",2:"SECURITY_ERR",3:"ABORT_ERR",4:"NOT_READABLE_ERR",5:"ENCODING_ERR",6:"NO_MODIFICATION_ALLOWED_ERR",7:"INVALID_STATE_ERR",8:"SYNTAX_ERR",9:"INVALID_MODIFICATION_ERR",10:"QUOTA_EXCEEDED_ERR",11:"TYPE_MISMATCH_ERR",12:"PATH_EXISTS_ERR"}).provider("$cordovaFile",[function(){this.$get=["$q","$window","$cordovaFileError",function(e,n,t){return{getFreeDiskSpace:function(){var n=e.defer();return cordova.exec(function(e){n.resolve(e)},function(e){n.reject(e)},"File","getFreeDiskSpace",[]),n.promise},checkDir:function(r,o){var i=e.defer();/^\//.test(o)&&i.reject("directory cannot start with /");try{var a=r+o;n.resolveLocalFileSystemURL(a,function(e){e.isDirectory===!0?i.resolve(e):i.reject({code:13,message:"input is not a directory"})},function(e){e.message=t[e.code],i.reject(e)})}catch(c){c.message=t[c.code],i.reject(c)}return i.promise},checkFile:function(r,o){var i=e.defer();/^\//.test(o)&&i.reject("directory cannot start with /");try{var a=r+o;n.resolveLocalFileSystemURL(a,function(e){e.isFile===!0?i.resolve(e):i.reject({code:13,message:"input is not a file"})},function(e){e.message=t[e.code],i.reject(e)})}catch(c){c.message=t[c.code],i.reject(c)}return i.promise},createDir:function(r,o,i){var a=e.defer();/^\//.test(o)&&a.reject("directory cannot start with /"),i=i?!1:!0;var c={create:!0,exclusive:i};try{n.resolveLocalFileSystemURL(r,function(e){e.getDirectory(o,c,function(e){a.resolve(e)},function(e){e.message=t[e.code],a.reject(e)})},function(e){e.message=t[e.code],a.reject(e)})}catch(s){s.message=t[s.code],a.reject(s)}return a.promise},createFile:function(r,o,i){var a=e.defer();/^\//.test(o)&&a.reject("file-name cannot start with /"),i=i?!1:!0;var c={create:!0,exclusive:i};try{n.resolveLocalFileSystemURL(r,function(e){e.getFile(o,c,function(e){a.resolve(e)},function(e){e.message=t[e.code],a.reject(e)})},function(e){e.message=t[e.code],a.reject(e)})}catch(s){s.message=t[s.code],a.reject(s)}return a.promise},removeDir:function(r,o){var i=e.defer();/^\//.test(o)&&i.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getDirectory(o,{create:!1},function(e){e.remove(function(){i.resolve({success:!0,fileRemoved:e})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})}catch(a){a.message=t[a.code],i.reject(a)}return i.promise},removeFile:function(r,o){var i=e.defer();/^\//.test(o)&&i.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getFile(o,{create:!1},function(e){e.remove(function(){i.resolve({success:!0,fileRemoved:e})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})}catch(a){a.message=t[a.code],i.reject(a)}return i.promise},removeRecursively:function(r,o){var i=e.defer();/^\//.test(o)&&i.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getDirectory(o,{create:!1},function(e){e.removeRecursively(function(){i.resolve({success:!0,fileRemoved:e})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})}catch(a){a.message=t[a.code],i.reject(a)}return i.promise},writeFile:function(r,o,i,a){var c=e.defer();/^\//.test(o)&&c.reject("file-name cannot start with /"),a=a?!1:!0;var s={create:!0,exclusive:a};try{n.resolveLocalFileSystemURL(r,function(e){e.getFile(o,s,function(e){e.createWriter(function(e){s.append===!0&&e.seek(e.length),s.truncate&&e.truncate(s.truncate),e.onwriteend=function(e){this.error?c.reject(this.error):c.resolve(e)},e.write(i),c.promise.abort=function(){e.abort()}})},function(e){e.message=t[e.code],c.reject(e)})},function(e){e.message=t[e.code],c.reject(e)})}catch(u){u.message=t[u.code],c.reject(u)}return c.promise},writeExistingFile:function(r,o,i){var a=e.defer();/^\//.test(o)&&a.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getFile(o,{create:!1},function(e){e.createWriter(function(e){e.seek(e.length),e.onwriteend=function(e){this.error?a.reject(this.error):a.resolve(e)},e.write(i),a.promise.abort=function(){e.abort()}})},function(e){e.message=t[e.code],a.reject(e)})},function(e){e.message=t[e.code],a.reject(e)})}catch(c){c.message=t[c.code],a.reject(c)}return a.promise},readAsText:function(r,o){var i=e.defer();/^\//.test(o)&&i.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getFile(o,{create:!1},function(e){e.file(function(e){var n=new FileReader;n.onloadend=function(e){void 0!==e.target.result||null!==e.target.result?i.resolve(e.target.result):void 0!==e.target.error||null!==e.target.error?i.reject(e.target.error):i.reject({code:null,message:"READER_ONLOADEND_ERR"})},n.readAsText(e)})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})}catch(a){a.message=t[a.code],i.reject(a)}return i.promise},readAsDataURL:function(r,o){var i=e.defer();/^\//.test(o)&&i.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getFile(o,{create:!1},function(e){e.file(function(e){var n=new FileReader;n.onloadend=function(e){void 0!==e.target.result||null!==e.target.result?i.resolve(e.target.result):void 0!==e.target.error||null!==e.target.error?i.reject(e.target.error):i.reject({code:null,message:"READER_ONLOADEND_ERR"})},n.readAsDataURL(e)})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})}catch(a){a.message=t[a.code],i.reject(a)}return i.promise},readAsBinaryString:function(r,o){var i=e.defer();/^\//.test(o)&&i.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getFile(o,{create:!1},function(e){e.file(function(e){var n=new FileReader;n.onloadend=function(e){void 0!==e.target.result||null!==e.target.result?i.resolve(e.target.result):void 0!==e.target.error||null!==e.target.error?i.reject(e.target.error):i.reject({code:null,message:"READER_ONLOADEND_ERR"})},n.readAsBinaryString(e)})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})}catch(a){a.message=t[a.code],i.reject(a)}return i.promise},readAsArrayBuffer:function(r,o){var i=e.defer();/^\//.test(o)&&i.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getFile(o,{create:!1},function(e){e.file(function(e){var n=new FileReader;n.onloadend=function(e){void 0!==e.target.result||null!==e.target.result?i.resolve(e.target.result):void 0!==e.target.error||null!==e.target.error?i.reject(e.target.error):i.reject({code:null,message:"READER_ONLOADEND_ERR"})},n.readAsArrayBuffer(e)})},function(e){e.message=t[e.code],i.reject(e)})},function(e){e.message=t[e.code],i.reject(e)})}catch(a){a.message=t[a.code],i.reject(a)}return i.promise},moveFile:function(t,r,o,i){var a=e.defer();i=i||r,(/^\//.test(r)||/^\//.test(i))&&a.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(t,function(e){e.getFile(r,{create:!1},function(e){n.resolveLocalFileSystemURL(o,function(n){e.moveTo(n,i,function(e){a.resolve(e)},function(e){a.reject(e)})},function(e){a.reject(e)})},function(e){a.reject(e);
})},function(e){a.reject(e)})}catch(c){a.reject(c)}return a.promise},moveDir:function(t,r,o,i){var a=e.defer();i=i||r,(/^\//.test(r)||/^\//.test(i))&&a.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(t,function(e){e.getDirectory(r,{create:!1},function(e){n.resolveLocalFileSystemURL(o,function(n){e.moveTo(n,i,function(e){a.resolve(e)},function(e){a.reject(e)})},function(e){a.reject(e)})},function(e){a.reject(e)})},function(e){a.reject(e)})}catch(c){a.reject(c)}return a.promise},copyDir:function(r,o,i,a){var c=e.defer();a=a||o,(/^\//.test(o)||/^\//.test(a))&&c.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getDirectory(o,{create:!1,exclusive:!1},function(e){n.resolveLocalFileSystemURL(i,function(n){e.copyTo(n,a,function(e){c.resolve(e)},function(e){e.message=t[e.code],c.reject(e)})},function(e){e.message=t[e.code],c.reject(e)})},function(e){e.message=t[e.code],c.reject(e)})},function(e){e.message=t[e.code],c.reject(e)})}catch(s){s.message=t[s.code],c.reject(s)}return c.promise},copyFile:function(r,o,i,a){var c=e.defer();a=a||o,/^\//.test(o)&&c.reject("file-name cannot start with /");try{n.resolveLocalFileSystemURL(r,function(e){e.getFile(o,{create:!1,exclusive:!1},function(e){n.resolveLocalFileSystemURL(i,function(n){e.copyTo(n,a,function(e){c.resolve(e)},function(e){e.message=t[e.code],c.reject(e)})},function(e){e.message=t[e.code],c.reject(e)})},function(e){e.message=t[e.code],c.reject(e)})},function(e){e.message=t[e.code],c.reject(e)})}catch(s){s.message=t[s.code],c.reject(s)}return c.promise}}}]}]),angular.module("ngCordova.plugins.fileOpener2",[]).factory("$cordovaFileOpener2",["$q",function(e){return{open:function(n,t){var r=e.defer();return cordova.plugins.fileOpener2.open(n,t,{error:function(e){r.reject(e)},success:function(){r.resolve()}}),r.promise},uninstall:function(n){var t=e.defer();return cordova.plugins.fileOpener2.uninstall(n,{error:function(e){t.reject(e)},success:function(){t.resolve()}}),t.promise},appIsInstalled:function(n){var t=e.defer();return cordova.plugins.fileOpener2.appIsInstalled(n,{success:function(e){t.resolve(e)}}),t.promise}}}]),angular.module("ngCordova.plugins.fileTransfer",[]).factory("$cordovaFileTransfer",["$q","$timeout",function(e,n){return{download:function(t,r,o,i){var a=e.defer(),c=new FileTransfer,s=o&&o.encodeURI===!1?t:encodeURI(t);return o&&void 0!==o.timeout&&null!==o.timeout&&(n(function(){c.abort()},o.timeout),o.timeout=null),c.onprogress=function(e){a.notify(e)},a.promise.abort=function(){c.abort()},c.download(s,r,a.resolve,a.reject,i,o),a.promise},upload:function(t,r,o,i){var a=e.defer(),c=new FileTransfer,s=o&&o.encodeURI===!1?t:encodeURI(t);return o&&void 0!==o.timeout&&null!==o.timeout&&(n(function(){c.abort()},o.timeout),o.timeout=null),c.onprogress=function(e){a.notify(e)},a.promise.abort=function(){c.abort()},c.upload(r,s,a.resolve,a.reject,o,i),a.promise}}}]),angular.module("ngCordova.plugins.flashlight",[]).factory("$cordovaFlashlight",["$q","$window",function(e,n){return{available:function(){var t=e.defer();return n.plugins.flashlight.available(function(e){t.resolve(e)}),t.promise},switchOn:function(){var t=e.defer();return n.plugins.flashlight.switchOn(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},switchOff:function(){var t=e.defer();return n.plugins.flashlight.switchOff(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},toggle:function(){var t=e.defer();return n.plugins.flashlight.toggle(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise}}}]),angular.module("ngCordova.plugins.flurryAds",[]).factory("$cordovaFlurryAds",["$q","$window",function(e,n){return{setOptions:function(t){var r=e.defer();return n.FlurryAds.setOptions(t,function(){r.resolve()},function(){r.reject()}),r.promise},createBanner:function(t){var r=e.defer();return n.FlurryAds.createBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},removeBanner:function(){var t=e.defer();return n.FlurryAds.removeBanner(function(){t.resolve()},function(){t.reject()}),t.promise},showBanner:function(t){var r=e.defer();return n.FlurryAds.showBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},showBannerAtXY:function(t,r){var o=e.defer();return n.FlurryAds.showBannerAtXY(t,r,function(){o.resolve()},function(){o.reject()}),o.promise},hideBanner:function(){var t=e.defer();return n.FlurryAds.hideBanner(function(){t.resolve()},function(){t.reject()}),t.promise},prepareInterstitial:function(t){var r=e.defer();return n.FlurryAds.prepareInterstitial(t,function(){r.resolve()},function(){r.reject()}),r.promise},showInterstitial:function(){var t=e.defer();return n.FlurryAds.showInterstitial(function(){t.resolve()},function(){t.reject()}),t.promise}}}]),angular.module("ngCordova.plugins.ga",[]).factory("$cordovaGA",["$q","$window",function(e,n){return{init:function(t,r){var o=e.defer();return r=r>=0?r:10,n.plugins.gaPlugin.init(function(e){o.resolve(e)},function(e){o.reject(e)},t,r),o.promise},trackEvent:function(t,r,o,i,a,c){var s=e.defer();return n.plugins.gaPlugin.trackEvent(function(e){s.resolve(e)},function(e){s.reject(e)},o,i,a,c),s.promise},trackPage:function(t,r,o){var i=e.defer();return n.plugins.gaPlugin.trackPage(function(e){i.resolve(e)},function(e){i.reject(e)},o),i.promise},setVariable:function(t,r,o,i){var a=e.defer();return n.plugins.gaPlugin.setVariable(function(e){a.resolve(e)},function(e){a.reject(e)},o,i),a.promise},exit:function(){var t=e.defer();return n.plugins.gaPlugin.exit(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise}}}]),angular.module("ngCordova.plugins.geolocation",[]).factory("$cordovaGeolocation",["$q",function(e){return{getCurrentPosition:function(n){var t=e.defer();return navigator.geolocation.getCurrentPosition(function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise},watchPosition:function(n){var t=e.defer(),r=navigator.geolocation.watchPosition(function(e){t.notify(e)},function(e){t.reject(e)},n);return t.promise.cancel=function(){navigator.geolocation.clearWatch(r)},t.promise.clearWatch=function(e){navigator.geolocation.clearWatch(e||r)},t.promise.watchID=r,t.promise},clearWatch:function(e){return navigator.geolocation.clearWatch(e)}}}]),angular.module("ngCordova.plugins.globalization",[]).factory("$cordovaGlobalization",["$q",function(e){return{getPreferredLanguage:function(){var n=e.defer();return navigator.globalization.getPreferredLanguage(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise},getLocaleName:function(){var n=e.defer();return navigator.globalization.getLocaleName(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise},getFirstDayOfWeek:function(){var n=e.defer();return navigator.globalization.getFirstDayOfWeek(function(e){n.resolve(e)},function(e){n.reject(e)}),n.promise},dateToString:function(n,t){var r=e.defer();return navigator.globalization.dateToString(n,function(e){r.resolve(e)},function(e){r.reject(e)},t),r.promise},stringToDate:function(n,t){var r=e.defer();return navigator.globalization.stringToDate(n,function(e){r.resolve(e)},function(e){r.reject(e)},t),r.promise},getDatePattern:function(n){var t=e.defer();return navigator.globalization.getDatePattern(function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise},getDateNames:function(n){var t=e.defer();return navigator.globalization.getDateNames(function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise},isDayLightSavingsTime:function(n){var t=e.defer();return navigator.globalization.isDayLightSavingsTime(n,function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},numberToString:function(n,t){var r=e.defer();return navigator.globalization.numberToString(n,function(e){r.resolve(e)},function(e){r.reject(e)},t),r.promise},stringToNumber:function(n,t){var r=e.defer();return navigator.globalization.stringToNumber(n,function(e){r.resolve(e)},function(e){r.reject(e)},t),r.promise},getNumberPattern:function(n){var t=e.defer();return navigator.globalization.getNumberPattern(function(e){t.resolve(e)},function(e){t.reject(e)},n),t.promise},getCurrencyPattern:function(n){var t=e.defer();return navigator.globalization.getCurrencyPattern(n,function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise}}}]),angular.module("ngCordova.plugins.googleAds",[]).factory("$cordovaGoogleAds",["$q","$window",function(e,n){return{setOptions:function(t){var r=e.defer();return n.AdMob.setOptions(t,function(){r.resolve()},function(){r.reject()}),r.promise},createBanner:function(t){var r=e.defer();return n.AdMob.createBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},removeBanner:function(){var t=e.defer();return n.AdMob.removeBanner(function(){t.resolve()},function(){t.reject()}),t.promise},showBanner:function(t){var r=e.defer();return n.AdMob.showBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},showBannerAtXY:function(t,r){var o=e.defer();return n.AdMob.showBannerAtXY(t,r,function(){o.resolve()},function(){o.reject()}),o.promise},hideBanner:function(){var t=e.defer();return n.AdMob.hideBanner(function(){t.resolve()},function(){t.reject()}),t.promise},prepareInterstitial:function(t){var r=e.defer();return n.AdMob.prepareInterstitial(t,function(){r.resolve()},function(){r.reject()}),r.promise},showInterstitial:function(){var t=e.defer();return n.AdMob.showInterstitial(function(){t.resolve()},function(){t.reject()}),t.promise}}}]),angular.module("ngCordova.plugins.googleAnalytics",[]).factory("$cordovaGoogleAnalytics",["$q","$window",function(e,n){return{startTrackerWithId:function(t){var r=e.defer();return n.analytics.startTrackerWithId(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},setUserId:function(t){var r=e.defer();return n.analytics.setUserId(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},debugMode:function(){var t=e.defer();return n.analytics.debugMode(function(e){t.resolve(e)},function(){t.reject()}),t.promise},trackView:function(t){var r=e.defer();return n.analytics.trackView(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},addCustomDimension:function(t,r){var o=e.defer();return n.analytics.addCustomDimension(t,r,function(){o.resolve()},function(e){o.reject(e)}),o.promise},trackEvent:function(t,r,o,i){var a=e.defer();return n.analytics.trackEvent(t,r,o,i,function(e){a.resolve(e)},function(e){a.reject(e)}),a.promise},trackException:function(t,r){var o=e.defer();return n.analytics.trackException(t,r,function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise},trackTiming:function(t,r,o,i){var a=e.defer();return n.analytics.trackTiming(t,r,o,i,function(e){a.resolve(e)},function(e){a.reject(e)}),a.promise},addTransaction:function(t,r,o,i,a,c){var s=e.defer();return n.analytics.addTransaction(t,r,o,i,a,c,function(e){s.resolve(e)},function(e){s.reject(e)}),s.promise},addTransactionItem:function(t,r,o,i,a,c,s){var u=e.defer();return n.analytics.addTransactionItem(t,r,o,i,a,c,s,function(e){u.resolve(e)},function(e){u.reject(e)}),u.promise}}}]),angular.module("ngCordova.plugins.googleMap",[]).factory("$cordovaGoogleMap",["$q","$window",function(e,n){var t=null;return{getMap:function(r){var o=e.defer();if(n.plugin.google.maps){var i=document.getElementById("map_canvas");t=n.plugin.google.maps.Map.getMap(r),t.setDiv(i),o.resolve(t)}else o.reject(null);return o.promise},isMapLoaded:function(){return!!t},addMarker:function(n){var r=e.defer();return t.addMarker(n,function(e){r.resolve(e)}),r.promise},getMapTypeIds:function(){return n.plugin.google.maps.mapTypeId},setVisible:function(n){var r=e.defer();return t.setVisible(n),r.promise},cleanup:function(){t=null}}}]),angular.module("ngCordova.plugins.googlePlayGame",[]).factory("$cordovaGooglePlayGame",["$q",function(e){return{auth:function(){var n=e.defer();return googleplaygame.auth(function(e){return n.resolve(e)},function(e){return n.reject(e)}),n.promise},signout:function(){var n=e.defer();return googleplaygame.signout(function(e){return n.resolve(e)},function(e){return n.reject(e)}),n.promise},isSignedIn:function(){var n=e.defer();return googleplaygame.isSignedIn(function(e){return n.resolve(e)},function(e){return n.reject(e)}),n.promise},showPlayer:function(){var n=e.defer();return googleplaygame.showPlayer(function(e){return n.resolve(e)},function(e){return n.reject(e)}),n.promise},submitScore:function(n){var t=e.defer();return googleplaygame.submitScore(n,function(e){return t.resolve(e)},function(e){return t.reject(e)}),t.promise},showAllLeaderboards:function(){var n=e.defer();return googleplaygame.showAllLeaderboards(function(e){return n.resolve(e)},function(e){return n.reject(e)}),n.promise},showLeaderboard:function(n){var t=e.defer();return googleplaygame.showLeaderboard(n,function(e){return t.resolve(e)},function(e){return t.reject(e)}),t.promise},unlockAchievement:function(n){var t=e.defer();return googleplaygame.unlockAchievement(n,function(e){return t.resolve(e)},function(e){return t.reject(e)}),t.promise},incrementAchievement:function(n){var t=e.defer();return googleplaygame.incrementAchievement(n,function(e){return t.resolve(e)},function(e){return t.reject(e)}),t.promise},showAchievements:function(){var n=e.defer();return googleplaygame.showAchievements(function(e){return n.resolve(e)},function(e){return n.reject(e)}),n.promise}}}]),angular.module("ngCordova.plugins.googlePlus",[]).factory("$cordovaGooglePlus",["$q","$window",function(e,n){return{login:function(t){var r=e.defer();return void 0===t&&(t={}),n.plugins.googleplus.login({iOSApiKey:t},function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},silentLogin:function(t){var r=e.defer();return void 0===t&&(t={}),n.plugins.googleplus.trySilentLogin({iOSApiKey:t},function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},logout:function(){var t=e.defer();n.plugins.googleplus.logout(function(e){t.resolve(e)})},disconnect:function(){var t=e.defer();n.plugins.googleplus.disconnect(function(e){t.resolve(e)})},isAvailable:function(){var t=e.defer();return n.plugins.googleplus.isAvailable(function(e){e?t.resolve(e):t.reject(e)}),t.promise}}}]),angular.module("ngCordova.plugins.healthKit",[]).factory("$cordovaHealthKit",["$q","$window",function(e,n){return{isAvailable:function(){var t=e.defer();return n.plugins.healthkit.available(function(e){t.resolve(e)},function(e){t.reject(e)}),t.promise},checkAuthStatus:function(t){var r=e.defer();return t=t||"HKQuantityTypeIdentifierHeight",n.plugins.healthkit.checkAuthStatus({type:t},function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},requestAuthorization:function(t,r){var o=e.defer();return t=t||["HKCharacteristicTypeIdentifierDateOfBirth","HKQuantityTypeIdentifierActiveEnergyBurned","HKQuantityTypeIdentifierHeight"],r=r||["HKQuantityTypeIdentifierActiveEnergyBurned","HKQuantityTypeIdentifierHeight","HKQuantityTypeIdentifierDistanceCycling"],n.plugins.healthkit.requestAuthorization({readTypes:t,writeTypes:r},function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise},readDateOfBirth:function(){var t=e.defer();return n.plugins.healthkit.readDateOfBirth(function(e){t.resolve(e)},function(e){t.resolve(e)}),t.promise},readGender:function(){var t=e.defer();return n.plugins.healthkit.readGender(function(e){t.resolve(e)},function(e){t.resolve(e)}),t.promise},saveWeight:function(t,r,o){var i=e.defer();return n.plugins.healthkit.saveWeight({unit:r||"lb",amount:t,date:o||new Date},function(e){i.resolve(e)},function(e){i.resolve(e)}),i.promise},readWeight:function(t){var r=e.defer();return n.plugins.healthkit.readWeight({unit:t||"lb"},function(e){r.resolve(e)},function(e){r.resolve(e)}),r.promise},saveHeight:function(t,r,o){var i=e.defer();return n.plugins.healthkit.saveHeight({unit:r||"in",amount:t,date:o||new Date},function(e){i.resolve(e)},function(e){i.resolve(e)}),i.promise},readHeight:function(t){var r=e.defer();return n.plugins.healthkit.readHeight({unit:t||"in"},function(e){r.resolve(e)},function(e){r.resolve(e)}),r.promise},findWorkouts:function(){var t=e.defer();return n.plugins.healthkit.findWorkouts({},function(e){t.resolve(e)},function(e){t.resolve(e)}),t.promise},saveWorkout:function(t){var r=e.defer();return n.plugins.healthkit.saveWorkout(t,function(e){r.resolve(e)},function(e){r.resolve(e)}),r.promise},querySampleType:function(t){var r=e.defer();return n.plugins.healthkit.querySampleType(t,function(e){r.resolve(e)},function(e){r.resolve(e)}),r.promise}}}]),angular.module("ngCordova.plugins.httpd",[]).factory("$cordovaHttpd",["$q",function(e){return{startServer:function(n){var t=e.defer();return cordova.plugins.CorHttpd.startServer(n,function(){t.resolve()},function(){t.reject()}),t.promise},stopServer:function(){var n=e.defer();return cordova.plugins.CorHttpd.stopServer(function(){n.resolve()},function(){n.reject()}),n.promise},getURL:function(){var n=e.defer();return cordova.plugins.CorHttpd.getURL(function(e){n.resolve(e)},function(){n.reject()}),n.promise},getLocalPath:function(){var n=e.defer();return cordova.plugins.CorHttpd.getLocalPath(function(e){n.resolve(e)},function(){n.reject()}),n.promise}}}]),angular.module("ngCordova.plugins.iAd",[]).factory("$cordovaiAd",["$q","$window",function(e,n){return{setOptions:function(t){var r=e.defer();return n.iAd.setOptions(t,function(){r.resolve()},function(){r.reject()}),r.promise},createBanner:function(t){var r=e.defer();return n.iAd.createBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},removeBanner:function(){var t=e.defer();return n.iAd.removeBanner(function(){t.resolve()},function(){t.reject()}),t.promise},showBanner:function(t){var r=e.defer();return n.iAd.showBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},showBannerAtXY:function(t,r){var o=e.defer();return n.iAd.showBannerAtXY(t,r,function(){o.resolve()},function(){o.reject()}),o.promise},hideBanner:function(){var t=e.defer();return n.iAd.hideBanner(function(){t.resolve()},function(){t.reject()}),t.promise},prepareInterstitial:function(t){var r=e.defer();return n.iAd.prepareInterstitial(t,function(){r.resolve()},function(){r.reject()}),r.promise},showInterstitial:function(){var t=e.defer();return n.iAd.showInterstitial(function(){t.resolve()},function(){t.reject()}),t.promise}}}]),angular.module("ngCordova.plugins.imagePicker",[]).factory("$cordovaImagePicker",["$q","$window",function(e,n){return{getPictures:function(t){var r=e.defer();return n.imagePicker.getPictures(function(e){r.resolve(e)},function(e){r.reject(e)},t),r.promise}}}]),angular.module("ngCordova.plugins.inAppBrowser",[]).provider("$cordovaInAppBrowser",[function(){var e,n=this.defaultOptions={};this.setDefaultOptions=function(e){n=angular.extend(n,e)},this.$get=["$rootScope","$q","$window","$timeout",function(t,r,o,i){return{open:function(a,c,s){var u=r.defer();if(s&&!angular.isObject(s))return u.reject("options must be an object"),u.promise;var l=angular.extend({},n,s),d=[];angular.forEach(l,function(e,n){d.push(n+"="+e)});var f=d.join();return e=o.open(a,c,f),e.addEventListener("loadstart",function(e){i(function(){t.$broadcast("$cordovaInAppBrowser:loadstart",e)})},!1),e.addEventListener("loadstop",function(e){u.resolve(e),i(function(){t.$broadcast("$cordovaInAppBrowser:loadstop",e)})},!1),e.addEventListener("loaderror",function(e){u.reject(e),i(function(){t.$broadcast("$cordovaInAppBrowser:loaderror",e)})},!1),e.addEventListener("exit",function(e){i(function(){t.$broadcast("$cordovaInAppBrowser:exit",e)})},!1),u.promise},close:function(){e.close(),e=null},show:function(){e.show()},executeScript:function(n){var t=r.defer();return e.executeScript(n,function(e){t.resolve(e)}),t.promise},insertCSS:function(n){var t=r.defer();return e.insertCSS(n,function(e){t.resolve(e)}),t.promise}}}]}]),angular.module("ngCordova.plugins.insomnia",[]).factory("$cordovaInsomnia",["$window",function(e){return{keepAwake:function(){return e.plugins.insomnia.keepAwake()},allowSleepAgain:function(){return e.plugins.insomnia.allowSleepAgain()}}}]),angular.module("ngCordova.plugins.instagram",[]).factory("$cordovaInstagram",["$q",function(e){return{share:function(n){var t=e.defer();return window.Instagram?(Instagram.share(n.image,n.caption,function(e){e?t.reject(e):t.resolve(!0)}),t.promise):(console.error("Tried to call Instagram.share but the Instagram plugin isn't installed!"),t.resolve(null),t.promise)},isInstalled:function(){var n=e.defer();return window.Instagram?(Instagram.isInstalled(function(e,t){e?n.reject(e):n.resolve(t||!0)}),n.promise):(console.error("Tried to call Instagram.isInstalled but the Instagram plugin isn't installed!"),n.resolve(null),n.promise)}}}]),angular.module("ngCordova.plugins.keyboard",[]).factory("$cordovaKeyboard",["$rootScope",function(e){var n=function(){e.$evalAsync(function(){e.$broadcast("$cordovaKeyboard:show")})},t=function(){e.$evalAsync(function(){e.$broadcast("$cordovaKeyboard:hide")})};return document.addEventListener("deviceready",function(){cordova.plugins.Keyboard&&(window.addEventListener("native.keyboardshow",n,!1),window.addEventListener("native.keyboardhide",t,!1))}),{hideAccessoryBar:function(e){return cordova.plugins.Keyboard.hideKeyboardAccessoryBar(e)},close:function(){return cordova.plugins.Keyboard.close()},show:function(){return cordova.plugins.Keyboard.show()},disableScroll:function(e){return cordova.plugins.Keyboard.disableScroll(e)},isVisible:function(){return cordova.plugins.Keyboard.isVisible},clearShowWatch:function(){document.removeEventListener("native.keyboardshow",n),e.$$listeners["$cordovaKeyboard:show"]=[]},clearHideWatch:function(){document.removeEventListener("native.keyboardhide",t),e.$$listeners["$cordovaKeyboard:hide"]=[]}}}]),angular.module("ngCordova.plugins.keychain",[]).factory("$cordovaKeychain",["$q",function(e){return{getForKey:function(n,t){var r=e.defer(),o=new Keychain;return o.getForKey(r.resolve,r.reject,n,t),r.promise},setForKey:function(n,t,r){var o=e.defer(),i=new Keychain;return i.setForKey(o.resolve,o.reject,n,t,r),o.promise},removeForKey:function(n,t){var r=e.defer(),o=new Keychain;return o.removeForKey(r.resolve,r.reject,n,t),r.promise}}}]),angular.module("ngCordova.plugins.launchNavigator",[]).factory("$cordovaLaunchNavigator",["$q",function(e){return{navigate:function(n,t,r,o,i){var a=e.defer();return launchnavigator.navigate(n,t,function(){a.resolve()},function(e){a.reject(e)},i),a.promise}}}]),angular.module("ngCordova.plugins.localNotification",[]).factory("$cordovaLocalNotification",["$q","$window","$rootScope","$timeout",function(e,n,t,r){return document.addEventListener("deviceready",function(){n.cordova&&n.cordova.plugins&&n.cordova.plugins.notification&&n.cordova.plugins.notification.local&&(n.cordova.plugins.notification.local.on("schedule",function(e,n){r(function(){t.$broadcast("$cordovaLocalNotification:schedule",e,n)})}),n.cordova.plugins.notification.local.on("trigger",function(e,n){r(function(){t.$broadcast("$cordovaLocalNotification:trigger",e,n)})}),n.cordova.plugins.notification.local.on("update",function(e,n){r(function(){t.$broadcast("$cordovaLocalNotification:update",e,n)})}),n.cordova.plugins.notification.local.on("clear",function(e,n){r(function(){t.$broadcast("$cordovaLocalNotification:clear",e,n)})}),n.cordova.plugins.notification.local.on("clearall",function(e){r(function(){t.$broadcast("$cordovaLocalNotification:clearall",e)})}),n.cordova.plugins.notification.local.on("cancel",function(e,n){r(function(){t.$broadcast("$cordovaLocalNotification:cancel",e,n)})}),n.cordova.plugins.notification.local.on("cancelall",function(e){r(function(){t.$broadcast("$cordovaLocalNotification:cancelall",e)})}),n.cordova.plugins.notification.local.on("click",function(e,n){r(function(){t.$broadcast("$cordovaLocalNotification:click",e,n)})}))},!1),{schedule:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.schedule(t,function(e){o.resolve(e)},r),o.promise},add:function(t,r){console.warn('Deprecated: use "schedule" instead.');var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.schedule(t,function(e){o.resolve(e)},r),o.promise},update:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.update(t,function(e){o.resolve(e)},r),o.promise},clear:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.clear(t,function(e){o.resolve(e)},r),o.promise},clearAll:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.clearAll(function(e){r.resolve(e)},t),r.promise},cancel:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.cancel(t,function(e){o.resolve(e)},r),o.promise},cancelAll:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.cancelAll(function(e){r.resolve(e)},t),r.promise},isPresent:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.isPresent(t,function(e){o.resolve(e)},r),o.promise},isScheduled:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.isScheduled(t,function(e){o.resolve(e)},r),o.promise},isTriggered:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.isTriggered(t,function(e){o.resolve(e)},r),o.promise},hasPermission:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.hasPermission(function(e){e?r.resolve(e):r.reject(e)},t),r.promise},registerPermission:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.registerPermission(function(e){e?r.resolve(e):r.reject(e)},t),r.promise},promptForPermission:function(t){console.warn('Deprecated: use "registerPermission" instead.');var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.registerPermission(function(e){e?r.resolve(e):r.reject(e)},t),r.promise},getAllIds:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.getAllIds(function(e){r.resolve(e)},t),r.promise},getIds:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.getIds(function(e){r.resolve(e)},t),r.promise},getScheduledIds:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.getScheduledIds(function(e){r.resolve(e)},t),r.promise},getTriggeredIds:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.getTriggeredIds(function(e){r.resolve(e)},t),r.promise},get:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.get(t,function(e){o.resolve(e)},r),o.promise},getAll:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.getAll(function(e){r.resolve(e)},t),r.promise},getScheduled:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.getScheduled(t,function(e){o.resolve(e)},r),o.promise},getAllScheduled:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.getAllScheduled(function(e){r.resolve(e)},t),r.promise},getTriggered:function(t,r){var o=e.defer();return r=r||null,n.cordova.plugins.notification.local.getTriggered(t,function(e){o.resolve(e)},r),o.promise},getAllTriggered:function(t){var r=e.defer();return t=t||null,n.cordova.plugins.notification.local.getAllTriggered(function(e){r.resolve(e)},t),r.promise},getDefaults:function(){return n.cordova.plugins.notification.local.getDefaults()},setDefaults:function(e){n.cordova.plugins.notification.local.setDefaults(e)}}}]),angular.module("ngCordova.plugins.mMediaAds",[]).factory("$cordovaMMediaAds",["$q","$window",function(e,n){return{setOptions:function(t){var r=e.defer();return n.mMedia.setOptions(t,function(){r.resolve()},function(){r.reject()}),r.promise},createBanner:function(t){var r=e.defer();return n.mMedia.createBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},removeBanner:function(){var t=e.defer();return n.mMedia.removeBanner(function(){t.resolve()},function(){t.reject()}),t.promise},showBanner:function(t){var r=e.defer();return n.mMedia.showBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},showBannerAtXY:function(t,r){var o=e.defer();return n.mMedia.showBannerAtXY(t,r,function(){o.resolve()},function(){o.reject()}),o.promise},hideBanner:function(){var t=e.defer();return n.mMedia.hideBanner(function(){t.resolve()},function(){t.reject()}),t.promise},prepareInterstitial:function(t){var r=e.defer();return n.mMedia.prepareInterstitial(t,function(){r.resolve()},function(){r.reject()}),r.promise},showInterstitial:function(){var t=e.defer();return n.mMedia.showInterstitial(function(){t.resolve()},function(){t.reject()}),t.promise}}}]),angular.module("ngCordova.plugins.media",[]).service("NewMedia",["$q","$interval",function(e,n){function t(e){angular.isDefined(u)||(u=n(function(){0>f&&(f=e.getDuration(),a&&f>0&&a.notify({duration:f})),e.getCurrentPosition(function(e){e>-1&&(d=e)},function(e){console.log("Error getting pos="+e)}),a&&a.notify({position:d})},1e3))}function r(){angular.isDefined(u)&&(n.cancel(u),u=void 0)}function o(){d=-1,f=-1}function i(e){this.media=new Media(e,function(e){r(),o(),a.resolve(e)},function(e){r(),o(),a.reject(e)},function(e){l=e,a.notify({status:l})})}var a,c,s,u,l=null,d=-1,f=-1;return i.prototype.play=function(n){return a=e.defer(),"object"!=typeof n&&(n={}),this.media.play(n),t(this.media),a.promise},i.prototype.pause=function(){r(),this.media.pause()},i.prototype.stop=function(){this.media.stop()},i.prototype.release=function(){this.media.release(),this.media=void 0},i.prototype.seekTo=function(e){this.media.seekTo(e)},i.prototype.setVolume=function(e){this.media.setVolume(e)},i.prototype.startRecord=function(){this.media.startRecord()},i.prototype.stopRecord=function(){this.media.stopRecord()},i.prototype.currentTime=function(){return c=e.defer(),this.media.getCurrentPosition(function(e){c.resolve(e)}),c.promise},i.prototype.getDuration=function(){return s=e.defer(),this.media.getDuration(function(e){s.resolve(e)}),s.promise},i}]).factory("$cordovaMedia",["NewMedia",function(e){return{newMedia:function(n){return new e(n)}}}]),angular.module("ngCordova.plugins.mobfoxAds",[]).factory("$cordovaMobFoxAds",["$q","$window",function(e,n){return{setOptions:function(t){var r=e.defer();return n.MobFox.setOptions(t,function(){r.resolve()},function(){r.reject()}),r.promise},createBanner:function(t){var r=e.defer();return n.MobFox.createBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},removeBanner:function(){var t=e.defer();return n.MobFox.removeBanner(function(){t.resolve()},function(){t.reject()}),t.promise},showBanner:function(t){var r=e.defer();return n.MobFox.showBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},showBannerAtXY:function(t,r){var o=e.defer();return n.MobFox.showBannerAtXY(t,r,function(){o.resolve()},function(){o.reject()}),o.promise},hideBanner:function(){var t=e.defer();return n.MobFox.hideBanner(function(){t.resolve()},function(){t.reject()}),t.promise},prepareInterstitial:function(t){var r=e.defer();return n.MobFox.prepareInterstitial(t,function(){r.resolve()},function(){r.reject()}),r.promise},showInterstitial:function(){var t=e.defer();return n.MobFox.showInterstitial(function(){t.resolve()},function(){t.reject()}),t.promise}}}]),angular.module("ngCordova.plugins",["ngCordova.plugins.actionSheet","ngCordova.plugins.adMob","ngCordova.plugins.appAvailability","ngCordova.plugins.appRate","ngCordova.plugins.appVersion","ngCordova.plugins.backgroundGeolocation","ngCordova.plugins.badge","ngCordova.plugins.barcodeScanner","ngCordova.plugins.batteryStatus","ngCordova.plugins.ble","ngCordova.plugins.bluetoothSerial","ngCordova.plugins.brightness","ngCordova.plugins.calendar","ngCordova.plugins.camera","ngCordova.plugins.capture","ngCordova.plugins.clipboard","ngCordova.plugins.contacts","ngCordova.plugins.datePicker","ngCordova.plugins.device","ngCordova.plugins.deviceMotion","ngCordova.plugins.deviceOrientation","ngCordova.plugins.dialogs","ngCordova.plugins.emailComposer","ngCordova.plugins.facebook","ngCordova.plugins.facebookAds","ngCordova.plugins.file","ngCordova.plugins.fileTransfer","ngCordova.plugins.fileOpener2","ngCordova.plugins.flashlight","ngCordova.plugins.flurryAds","ngCordova.plugins.ga","ngCordova.plugins.geolocation","ngCordova.plugins.globalization","ngCordova.plugins.googleAds","ngCordova.plugins.googleAnalytics","ngCordova.plugins.googleMap","ngCordova.plugins.googlePlayGame","ngCordova.plugins.googlePlus","ngCordova.plugins.healthKit","ngCordova.plugins.httpd","ngCordova.plugins.iAd","ngCordova.plugins.imagePicker","ngCordova.plugins.inAppBrowser","ngCordova.plugins.instagram","ngCordova.plugins.keyboard","ngCordova.plugins.keychain","ngCordova.plugins.launchNavigator","ngCordova.plugins.localNotification","ngCordova.plugins.media","ngCordova.plugins.mMediaAds","ngCordova.plugins.mobfoxAds","ngCordova.plugins.mopubAds","ngCordova.plugins.nativeAudio","ngCordova.plugins.network","ngCordovaOauth","ngCordova.plugins.pinDialog","ngCordova.plugins.prefs","ngCordova.plugins.printer","ngCordova.plugins.progressIndicator","ngCordova.plugins.push","ngCordova.plugins.sms","ngCordova.plugins.socialSharing","ngCordova.plugins.spinnerDialog","ngCordova.plugins.splashscreen","ngCordova.plugins.sqlite","ngCordova.plugins.statusbar","ngCordova.plugins.toast","ngCordova.plugins.touchid","ngCordova.plugins.vibration","ngCordova.plugins.videoCapturePlus","ngCordova.plugins.zip","ngCordova.plugins.insomnia"]),
angular.module("ngCordova.plugins.mopubAds",[]).factory("$cordovaMoPubAds",["$q","$window",function(e,n){return{setOptions:function(t){var r=e.defer();return n.MoPub.setOptions(t,function(){r.resolve()},function(){r.reject()}),r.promise},createBanner:function(t){var r=e.defer();return n.MoPub.createBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},removeBanner:function(){var t=e.defer();return n.MoPub.removeBanner(function(){t.resolve()},function(){t.reject()}),t.promise},showBanner:function(t){var r=e.defer();return n.MoPub.showBanner(t,function(){r.resolve()},function(){r.reject()}),r.promise},showBannerAtXY:function(t,r){var o=e.defer();return n.MoPub.showBannerAtXY(t,r,function(){o.resolve()},function(){o.reject()}),o.promise},hideBanner:function(){var t=e.defer();return n.MoPub.hideBanner(function(){t.resolve()},function(){t.reject()}),t.promise},prepareInterstitial:function(t){var r=e.defer();return n.MoPub.prepareInterstitial(t,function(){r.resolve()},function(){r.reject()}),r.promise},showInterstitial:function(){var t=e.defer();return n.MoPub.showInterstitial(function(){t.resolve()},function(){t.reject()}),t.promise}}}]),angular.module("ngCordova.plugins.nativeAudio",[]).factory("$cordovaNativeAudio",["$q","$window",function(e,n){return{preloadSimple:function(t,r){var o=e.defer();return n.plugins.NativeAudio.preloadSimple(t,r,function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise},preloadComplex:function(t,r,o,i){var a=e.defer();return n.plugins.NativeAudio.preloadComplex(t,r,o,i,function(e){a.resolve(e)},function(e){a.reject(e)}),a.promise},play:function(t,r){var o=e.defer();return n.plugins.NativeAudio.play(t,r,function(e){o.reject(e)},function(e){o.resolve(e)}),o.promise},stop:function(t){var r=e.defer();return n.plugins.NativeAudio.stop(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},loop:function(t){var r=e.defer();return n.plugins.NativeAudio.loop(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},unload:function(t){var r=e.defer();return n.plugins.NativeAudio.unload(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},setVolumeForComplexAsset:function(t,r){var o=e.defer();return n.plugins.NativeAudio.setVolumeForComplexAsset(t,r,function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise}}}]),angular.module("ngCordova.plugins.network",[]).factory("$cordovaNetwork",["$rootScope","$timeout",function(e,n){var t=function(){var t=navigator.connection.type;n(function(){e.$broadcast("$cordovaNetwork:offline",t)})},r=function(){var t=navigator.connection.type;n(function(){e.$broadcast("$cordovaNetwork:online",t)})};return document.addEventListener("deviceready",function(){navigator.connection&&(document.addEventListener("offline",t,!1),document.addEventListener("online",r,!1))}),{getNetwork:function(){return navigator.connection.type},isOnline:function(){var e=navigator.connection.type;return e!==Connection.UNKNOWN&&e!==Connection.NONE},isOffline:function(){var e=navigator.connection.type;return e===Connection.UNKNOWN||e===Connection.NONE},clearOfflineWatch:function(){document.removeEventListener("offline",t),e.$$listeners["$cordovaNetwork:offline"]=[]},clearOnlineWatch:function(){document.removeEventListener("online",r),e.$$listeners["$cordovaNetwork:online"]=[]}}}]).run(["$injector",function(e){e.get("$cordovaNetwork")}]),angular.module("ngCordova.plugins.pinDialog",[]).factory("$cordovaPinDialog",["$q","$window",function(e,n){return{prompt:function(t,r,o){var i=e.defer();return n.plugins.pinDialog.prompt(t,function(e){i.resolve(e)},r,o),i.promise}}}]),angular.module("ngCordova.plugins.prefs",[]).factory("$cordovaPreferences",["$window","$q",function(e,n){return{set:function(t,r){var o=n.defer();return e.appgiraffe.plugins.applicationPreferences.set(t,r,function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise},get:function(t){var r=n.defer();return e.appgiraffe.plugins.applicationPreferences.get(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise}}}]),angular.module("ngCordova.plugins.printer",[]).factory("$cordovaPrinter",["$q","$window",function(e,n){return{isAvailable:function(){var t=e.defer();return n.plugin.printer.isAvailable(function(e){t.resolve(e)}),t.promise},print:function(t,r){var o=e.defer();return n.plugin.printer.print(t,r,function(){o.resolve()}),o.promise}}}]),angular.module("ngCordova.plugins.progressIndicator",[]).factory("$cordovaProgress",[function(){return{show:function(e){var n=e||"Please wait...";return ProgressIndicator.show(n)},showSimple:function(e){var n=e||!1;return ProgressIndicator.showSimple(n)},showSimpleWithLabel:function(e,n){var t=e||!1,r=n||"Loading...";return ProgressIndicator.showSimpleWithLabel(t,r)},showSimpleWithLabelDetail:function(e,n,t){var r=e||!1,o=n||"Loading...",i=t||"Please wait";return ProgressIndicator.showSimpleWithLabelDetail(r,o,i)},showDeterminate:function(e,n){var t=e||!1,r=n||5e4;return ProgressIndicator.showDeterminate(t,r)},showDeterminateWithLabel:function(e,n,t){var r=e||!1,o=n||5e4,i=t||"Loading...";return ProgressIndicator.showDeterminateWithLabel(r,o,i)},showAnnular:function(e,n){var t=e||!1,r=n||5e4;return ProgressIndicator.showAnnular(t,r)},showAnnularWithLabel:function(e,n,t){var r=e||!1,o=n||5e4,i=t||"Loading...";return ProgressIndicator.showAnnularWithLabel(r,o,i)},showBar:function(e,n){var t=e||!1,r=n||5e4;return ProgressIndicator.showBar(t,r)},showBarWithLabel:function(e,n,t){var r=e||!1,o=n||5e4,i=t||"Loading...";return ProgressIndicator.showBarWithLabel(r,o,i)},showSuccess:function(e,n){var t=e||!1,r=n||"Success";return ProgressIndicator.showSuccess(t,r)},showText:function(e,n,t){var r=e||!1,o=n||"Warning",i=t||"center";return ProgressIndicator.showText(r,o,i)},hide:function(){return ProgressIndicator.hide()}}}]),angular.module("ngCordova.plugins.push",[]).factory("$cordovaPush",["$q","$window","$rootScope","$timeout",function(e,n,t,r){return{onNotification:function(e){r(function(){t.$broadcast("$cordovaPush:notificationReceived",e)})},register:function(t){var r,o=e.defer();return void 0!==t&&void 0===t.ecb&&(r=null===document.querySelector("[ng-app]")?"document.body":"document.querySelector('[ng-app]')",t.ecb="angular.element("+r+").injector().get('$cordovaPush').onNotification"),n.plugins.pushNotification.register(function(e){o.resolve(e)},function(e){o.reject(e)},t),o.promise},unregister:function(t){var r=e.defer();return n.plugins.pushNotification.unregister(function(e){r.resolve(e)},function(e){r.reject(e)},t),r.promise},setBadgeNumber:function(t){var r=e.defer();return n.plugins.pushNotification.setApplicationIconBadgeNumber(function(e){r.resolve(e)},function(e){r.reject(e)},t),r.promise}}}]),angular.module("ngCordova.plugins.sms",[]).factory("$cordovaSms",["$q",function(e){return{send:function(n,t,r){var o=e.defer();return sms.send(n,t,r,function(e){o.resolve(e)},function(e){o.reject(e)}),o.promise}}}]),angular.module("ngCordova.plugins.socialSharing",[]).factory("$cordovaSocialSharing",["$q","$window",function(e,n){return{share:function(t,r,o,i){var a=e.defer();return r=r||null,o=o||null,i=i||null,n.plugins.socialsharing.share(t,r,o,i,function(){a.resolve(!0)},function(){a.reject(!1)}),a.promise},shareViaTwitter:function(t,r,o){var i=e.defer();return r=r||null,o=o||null,n.plugins.socialsharing.shareViaTwitter(t,r,o,function(){i.resolve(!0)},function(){i.reject(!1)}),i.promise},shareViaWhatsApp:function(t,r,o){var i=e.defer();return r=r||null,o=o||null,n.plugins.socialsharing.shareViaWhatsApp(t,r,o,function(){i.resolve(!0)},function(){i.reject(!1)}),i.promise},shareViaFacebook:function(t,r,o){var i=e.defer();return t=t||null,r=r||null,o=o||null,n.plugins.socialsharing.shareViaFacebook(t,r,o,function(){i.resolve(!0)},function(){i.reject(!1)}),i.promise},shareViaFacebookWithPasteMessageHint:function(t,r,o,i){var a=e.defer();return r=r||null,o=o||null,n.plugins.socialsharing.shareViaFacebookWithPasteMessageHint(t,r,o,i,function(){a.resolve(!0)},function(){a.reject(!1)}),a.promise},shareViaSMS:function(t,r){var o=e.defer();return n.plugins.socialsharing.shareViaSMS(t,r,function(){o.resolve(!0)},function(){o.reject(!1)}),o.promise},shareViaEmail:function(t,r,o,i,a,c){var s=e.defer();return o=o||null,i=i||null,a=a||null,c=c||null,n.plugins.socialsharing.shareViaEmail(t,r,o,i,a,c,function(){s.resolve(!0)},function(){s.reject(!1)}),s.promise},shareVia:function(t,r,o,i,a){var c=e.defer();return r=r||null,o=o||null,i=i||null,a=a||null,n.plugins.socialsharing.shareVia(t,r,o,i,a,function(){c.resolve(!0)},function(){c.reject(!1)}),c.promise},canShareViaEmail:function(){var t=e.defer();return n.plugins.socialsharing.canShareViaEmail(function(){t.resolve(!0)},function(){t.reject(!1)}),t.promise},canShareVia:function(t,r,o,i,a){var c=e.defer();return n.plugins.socialsharing.canShareVia(t,r,o,i,a,function(e){c.resolve(e)},function(e){c.reject(e)}),c.promise},available:function(){var n=e.defer();window.plugins.socialsharing.available(function(e){e?n.resolve():n.reject()})}}}]),angular.module("ngCordova.plugins.spinnerDialog",[]).factory("$cordovaSpinnerDialog",["$window",function(e){return{show:function(n,t,r){return r=r||!1,e.plugins.spinnerDialog.show(n,t,r)},hide:function(){return e.plugins.spinnerDialog.hide()}}}]),angular.module("ngCordova.plugins.splashscreen",[]).factory("$cordovaSplashscreen",[function(){return{hide:function(){return navigator.splashscreen.hide()},show:function(){return navigator.splashscreen.show()}}}]),angular.module("ngCordova.plugins.sqlite",[]).factory("$cordovaSQLite",["$q","$window",function(e,n){return{openDB:function(e,t){return"object"!=typeof e&&(e={name:e}),"undefined"!=typeof t&&(e.bgType=t),n.sqlitePlugin.openDatabase(e)},execute:function(n,t,r){var o=e.defer();return n.transaction(function(e){e.executeSql(t,r,function(e,n){o.resolve(n)},function(e,n){o.reject(n)})}),o.promise},insertCollection:function(n,t,r){var o=e.defer(),i=r.slice(0);return n.transaction(function(e){!function n(){var r=i.splice(0,1)[0];try{e.executeSql(t,r,function(e,t){0===i.length?o.resolve(t):n()},function(e,n){o.reject(n)})}catch(a){o.reject(a)}}()}),o.promise},nestedExecute:function(n,t,r,o,i){var a=e.defer();return n.transaction(function(e){e.executeSql(t,o,function(e,n){a.resolve(n),e.executeSql(r,i,function(e,n){a.resolve(n)})})},function(e,n){a.reject(n)}),a.promise},deleteDB:function(t){var r=e.defer();return n.sqlitePlugin.deleteDatabase(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise}}}]),angular.module("ngCordova.plugins.statusbar",[]).factory("$cordovaStatusbar",[function(){return{overlaysWebView:function(e){return StatusBar.overlaysWebView(!!e)},STYLES:{DEFAULT:0,LIGHT_CONTENT:1,BLACK_TRANSLUCENT:2,BLACK_OPAQUE:3},style:function(e){switch(e){case 0:return StatusBar.styleDefault();case 1:return StatusBar.styleLightContent();case 2:return StatusBar.styleBlackTranslucent();case 3:return StatusBar.styleBlackOpaque();default:return StatusBar.styleDefault()}},styleColor:function(e){return StatusBar.backgroundColorByName(e)},styleHex:function(e){return StatusBar.backgroundColorByHexString(e)},hide:function(){return StatusBar.hide()},show:function(){return StatusBar.show()},isVisible:function(){return StatusBar.isVisible}}}]),angular.module("ngCordova.plugins.toast",[]).factory("$cordovaToast",["$q","$window",function(e,n){return{showShortTop:function(t){var r=e.defer();return n.plugins.toast.showShortTop(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},showShortCenter:function(t){var r=e.defer();return n.plugins.toast.showShortCenter(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},showShortBottom:function(t){var r=e.defer();return n.plugins.toast.showShortBottom(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},showLongTop:function(t){var r=e.defer();return n.plugins.toast.showLongTop(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},showLongCenter:function(t){var r=e.defer();return n.plugins.toast.showLongCenter(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},showLongBottom:function(t){var r=e.defer();return n.plugins.toast.showLongBottom(t,function(e){r.resolve(e)},function(e){r.reject(e)}),r.promise},show:function(t,r,o){var i=e.defer();return n.plugins.toast.show(t,r,o,function(e){i.resolve(e)},function(e){i.reject(e)}),i.promise}}}]),angular.module("ngCordova.plugins.touchid",[]).factory("$cordovaTouchID",["$q",function(e){return{checkSupport:function(){var n=e.defer();return window.cordova?touchid.checkSupport(function(e){n.resolve(e)},function(e){n.reject(e)}):n.reject("Not supported without cordova.js"),n.promise},authenticate:function(n){var t=e.defer();return window.cordova?touchid.authenticate(function(e){t.resolve(e)},function(e){t.reject(e)},n):t.reject("Not supported without cordova.js"),t.promise}}}]),angular.module("ngCordova.plugins.vibration",[]).factory("$cordovaVibration",[function(){return{vibrate:function(e){return navigator.notification.vibrate(e)},vibrateWithPattern:function(e,n){return navigator.notification.vibrateWithPattern(e,n)},cancelVibration:function(){return navigator.notification.cancelVibration()}}}]),angular.module("ngCordova.plugins.videoCapturePlus",[]).provider("$cordovaVideoCapturePlus",[function(){var e={};this.setLimit=function(n){e.limit=n},this.setMaxDuration=function(n){e.duration=n},this.setHighQuality=function(n){e.highquality=n},this.useFrontCamera=function(n){e.frontcamera=n},this.setPortraitOverlay=function(n){e.portraitOverlay=n},this.setLandscapeOverlay=function(n){e.landscapeOverlay=n},this.setOverlayText=function(n){e.overlayText=n},this.$get=["$q","$window",function(n,t){return{captureVideo:function(r){var o=n.defer();return t.plugins.videocaptureplus?(t.plugins.videocaptureplus.captureVideo(o.resolve,o.reject,angular.extend({},e,r)),o.promise):(o.resolve(null),o.promise)}}}]}]),angular.module("ngCordova.plugins.zip",[]).factory("$cordovaZip",["$q","$window",function(e,n){return{unzip:function(t,r){var o=e.defer();return n.zip.unzip(t,r,function(e){0===e?o.resolve():o.reject()},function(e){o.notify(e)}),o.promise}}}]),angular.module("oauth.providers",["oauth.utils"]).factory("$cordovaOauth",["$q","$http","$cordovaOauthUtility",function(e,n,t){return{adfs:function(r,o,i){var a=e.defer();if(window.cordova){var c=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(c)===!0){var s=window.open(o+"/adfs/oauth2/authorize?response_type=code&client_id="+r+"&redirect_uri=http://localhost/callback&resource="+i,"_blank","location=no");s.addEventListener("loadstart",function(e){if(0===e.url.indexOf("http://localhost/callback")){var t=e.url.split("code=")[1];n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:o+"/adfs/oauth2/token",data:"client_id="+r+"&code="+t+"&redirect_uri=http://localhost/callback&grant_type=authorization_code"}).success(function(e){a.resolve(e)}).error(function(e,n){a.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){s.close()},10)})}}),s.addEventListener("exit",function(e){a.reject("The sign in flow was canceled")})}else a.reject("Could not find InAppBrowser plugin")}else a.reject("Cannot authenticate via a web browser");return a.promise},dropbox:function(n,r){var o=e.defer();if(window.cordova){var i=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(i)===!0){var a="http://localhost/callback";void 0!==r&&r.hasOwnProperty("redirect_uri")&&(a=r.redirect_uri);var c=window.open("https://www.dropbox.com/1/oauth2/authorize?client_id="+n+"&redirect_uri="+a+"&response_type=token","_blank","location=no,clearsessioncache=yes,clearcache=yes");c.addEventListener("loadstart",function(e){if(0===e.url.indexOf(a)){c.removeEventListener("exit",function(e){}),c.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],i=0;i<t.length;i++)r[t[i].split("=")[0]]=t[i].split("=")[1];void 0!==r.access_token&&null!==r.access_token?o.resolve({access_token:r.access_token,token_type:r.token_type,uid:r.uid}):o.reject("Problem authenticating")}}),c.addEventListener("exit",function(e){o.reject("The sign in flow was canceled")})}else o.reject("Could not find InAppBrowser plugin")}else o.reject("Cannot authenticate via a web browser");return o.promise},digitalOcean:function(r,o,i){var a=e.defer();if(window.cordova){var c=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(c)===!0){var s="http://localhost/callback";void 0!==i&&i.hasOwnProperty("redirect_uri")&&(s=i.redirect_uri);var u=window.open("https://cloud.digitalocean.com/v1/oauth/authorize?client_id="+r+"&redirect_uri="+s+"&response_type=code&scope=read%20write","_blank","location=no,clearsessioncache=yes,clearcache=yes");u.addEventListener("loadstart",function(e){if(0===e.url.indexOf(s)){var t=e.url.split("code=")[1];n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:"https://cloud.digitalocean.com/v1/oauth/token",data:"client_id="+r+"&client_secret="+o+"&redirect_uri="+s+"&grant_type=authorization_code&code="+t}).success(function(e){a.resolve(e)}).error(function(e,n){a.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){u.close()},10)})}}),u.addEventListener("exit",function(e){a.reject("The sign in flow was canceled")})}else a.reject("Could not find InAppBrowser plugin")}else a.reject("Cannot authenticate via a web browser");return a.promise},google:function(n,r,o){var i=e.defer();if(window.cordova){var a=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(a)===!0){var c="http://localhost/callback";void 0!==o&&o.hasOwnProperty("redirect_uri")&&(c=o.redirect_uri);var s=window.open("https://accounts.google.com/o/oauth2/auth?client_id="+n+"&redirect_uri="+c+"&scope="+r.join(" ")+"&approval_prompt=force&response_type=token","_blank","location=no,clearsessioncache=yes,clearcache=yes");s.addEventListener("loadstart",function(e){if(0===e.url.indexOf(c)){s.removeEventListener("exit",function(e){}),s.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],o=0;o<t.length;o++)r[t[o].split("=")[0]]=t[o].split("=")[1];void 0!==r.access_token&&null!==r.access_token?i.resolve({access_token:r.access_token,token_type:r.token_type,expires_in:r.expires_in}):i.reject("Problem authenticating")}}),s.addEventListener("exit",function(e){i.reject("The sign in flow was canceled")})}else i.reject("Could not find InAppBrowser plugin")}else i.reject("Cannot authenticate via a web browser");return i.promise},github:function(r,o,i,a){var c=e.defer();if(window.cordova){var s=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(s)===!0){var u="http://localhost/callback";void 0!==a&&a.hasOwnProperty("redirect_uri")&&(u=a.redirect_uri);var l=window.open("https://github.com/login/oauth/authorize?client_id="+r+"&redirect_uri="+u+"&scope="+i.join(","),"_blank","location=no,clearsessioncache=yes,clearcache=yes");l.addEventListener("loadstart",function(e){0===e.url.indexOf(u)&&(requestToken=e.url.split("code=")[1],n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n.defaults.headers.post.accept="application/json",n({method:"post",url:"https://github.com/login/oauth/access_token",data:"client_id="+r+"&client_secret="+o+"&redirect_uri="+u+"&code="+requestToken}).success(function(e){c.resolve(e)}).error(function(e,n){c.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){l.close()},10)}))}),l.addEventListener("exit",function(e){c.reject("The sign in flow was canceled")})}else c.reject("Could not find InAppBrowser plugin")}else c.reject("Cannot authenticate via a web browser");return c.promise},facebook:function(n,r,o){var i=e.defer();if(window.cordova){var a=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(a)===!0){var c="http://localhost/callback";void 0!==o&&o.hasOwnProperty("redirect_uri")&&(c=o.redirect_uri);var s="https://www.facebook.com/v2.0/dialog/oauth?client_id="+n+"&redirect_uri="+c+"&response_type=token&scope="+r.join(",");void 0!==o&&o.hasOwnProperty("auth_type")&&(s+="&auth_type="+o.auth_type);var u=window.open(s,"_blank","location=no,clearsessioncache=yes,clearcache=yes");u.addEventListener("loadstart",function(e){if(0===e.url.indexOf(c)){u.removeEventListener("exit",function(e){}),u.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],o=0;o<t.length;o++)r[t[o].split("=")[0]]=t[o].split("=")[1];void 0!==r.access_token&&null!==r.access_token?i.resolve({access_token:r.access_token,expires_in:r.expires_in}):0!==e.url.indexOf("error_code=100")?i.reject("Facebook returned error_code=100: Invalid permissions"):i.reject("Problem authenticating")}}),u.addEventListener("exit",function(e){i.reject("The sign in flow was canceled")})}else i.reject("Could not find InAppBrowser plugin")}else i.reject("Cannot authenticate via a web browser");return i.promise},linkedin:function(r,o,i,a,c){var s=e.defer();if(window.cordova){var u=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(u)===!0){var l="http://localhost/callback";void 0!==c&&c.hasOwnProperty("redirect_uri")&&(l=c.redirect_uri);var d=window.open("https://www.linkedin.com/uas/oauth2/authorization?client_id="+r+"&redirect_uri="+l+"&scope="+i.join(" ")+"&response_type=code&state="+a,"_blank","location=no,clearsessioncache=yes,clearcache=yes");d.addEventListener("loadstart",function(e){0===e.url.indexOf(l)&&(requestToken=e.url.split("code=")[1],n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:"https://www.linkedin.com/uas/oauth2/accessToken",data:"client_id="+r+"&client_secret="+o+"&redirect_uri="+l+"&grant_type=authorization_code&code="+requestToken}).success(function(e){s.resolve(e)}).error(function(e,n){s.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){d.close()},10)}))}),d.addEventListener("exit",function(e){s.reject("The sign in flow was canceled")})}else s.reject("Could not find InAppBrowser plugin")}else s.reject("Cannot authenticate via a web browser");return s.promise},instagram:function(n,r,o){var i=e.defer(),a={code:"?",token:"#"};if(window.cordova){var c=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(c)===!0){var s="http://localhost/callback",u="token";void 0!==o&&(o.hasOwnProperty("redirect_uri")&&(s=o.redirect_uri),o.hasOwnProperty("response_type")&&(u=o.response_type));var l=window.open("https://api.instagram.com/oauth/authorize/?client_id="+n+"&redirect_uri="+s+"&scope="+r.join(" ")+"&response_type="+u,"_blank","location=no,clearsessioncache=yes,clearcache=yes");l.addEventListener("loadstart",function(e){if(0===e.url.indexOf(s)){l.removeEventListener("exit",function(e){}),l.close();var n=e.url.split(a[u])[1],r=t.parseResponseParameters(n);void 0!==r.access_token&&null!==r.access_token?i.resolve({access_token:r.access_token}):void 0!==r.code&&null!==r.code?i.resolve({code:r.code}):i.reject("Problem authenticating")}}),l.addEventListener("exit",function(e){i.reject("The sign in flow was canceled")})}else i.reject("Could not find InAppBrowser plugin")}else i.reject("Cannot authenticate via a web browser");return i.promise},box:function(r,o,i,a){var c=e.defer();if(window.cordova){var s=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(s)===!0){var u="http://localhost/callback";void 0!==a&&a.hasOwnProperty("redirect_uri")&&(u=a.redirect_uri);var l=window.open("https://app.box.com/api/oauth2/authorize/?client_id="+r+"&redirect_uri="+u+"&state="+i+"&response_type=code","_blank","location=no,clearsessioncache=yes,clearcache=yes");l.addEventListener("loadstart",function(e){0===e.url.indexOf(u)&&(requestToken=e.url.split("code=")[1],n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:"https://app.box.com/api/oauth2/token",data:"client_id="+r+"&client_secret="+o+"&redirect_uri="+u+"&grant_type=authorization_code&code="+requestToken}).success(function(e){c.resolve(e)}).error(function(e,n){c.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){l.close()},10)}))}),l.addEventListener("exit",function(e){c.reject("The sign in flow was canceled")})}else c.reject("Could not find InAppBrowser plugin")}else c.reject("Cannot authenticate via a web browser");return c.promise},reddit:function(r,o,i,a,c){var s=e.defer();if(window.cordova){var u=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(u)===!0){var l="http://localhost/callback";void 0!==c&&c.hasOwnProperty("redirect_uri")&&(l=c.redirect_uri);var d=window.open("https://ssl.reddit.com/api/v1/authorize?client_id="+r+"&redirect_uri="+l+"&duration=permanent&state=ngcordovaoauth&scope="+i.join(",")+"&response_type=code","_blank","location=no,clearsessioncache=yes,clearcache=yes");d.addEventListener("loadstart",function(e){0===e.url.indexOf(l)&&(requestToken=e.url.split("code=")[1],n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n.defaults.headers.post.Authorization="Basic "+btoa(r+":"+o),n({method:"post",url:"https://ssl.reddit.com/api/v1/access_token",data:"redirect_uri="+l+"&grant_type=authorization_code&code="+requestToken}).success(function(e){s.resolve(e)}).error(function(e,n){s.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){d.close()},10)}))}),d.addEventListener("exit",function(e){s.reject("The sign in flow was canceled")})}else s.reject("Could not find InAppBrowser plugin")}else s.reject("Cannot authenticate via a web browser");return s.promise},twitter:function(r,o,i){var a=e.defer();if(window.cordova){var c=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(c)===!0){var s="http://localhost/callback";if(void 0!==i&&i.hasOwnProperty("redirect_uri")&&(s=i.redirect_uri),"undefined"!=typeof jsSHA){var u={oauth_consumer_key:r,oauth_nonce:t.createNonce(10),oauth_signature_method:"HMAC-SHA1",oauth_timestamp:Math.round((new Date).getTime()/1e3),oauth_version:"1.0"},l=t.createSignature("POST","https://api.twitter.com/oauth/request_token",u,{oauth_callback:s},o);n({method:"post",url:"https://api.twitter.com/oauth/request_token",headers:{Authorization:l.authorization_header,"Content-Type":"application/x-www-form-urlencoded"},data:"oauth_callback="+encodeURIComponent(s)}).success(function(e){for(var r=e.split("&"),i={},c=0;c<r.length;c++)i[r[c].split("=")[0]]=r[c].split("=")[1];i.hasOwnProperty("oauth_token")===!1&&a.reject("Oauth request token was not received");var l=window.open("https://api.twitter.com/oauth/authenticate?oauth_token="+i.oauth_token,"_blank","location=no,clearsessioncache=yes,clearcache=yes");l.addEventListener("loadstart",function(e){if(0===e.url.indexOf(s)){for(var r=e.url.split("?")[1],i=r.split("&"),c={},d=0;d<i.length;d++)c[i[d].split("=")[0]]=i[d].split("=")[1];c.hasOwnProperty("oauth_verifier")===!1&&a.reject("Browser authentication failed to complete.  No oauth_verifier was returned"),delete u.oauth_signature,u.oauth_token=c.oauth_token;var f=t.createSignature("POST","https://api.twitter.com/oauth/access_token",u,{oauth_verifier:c.oauth_verifier},o);n({method:"post",url:"https://api.twitter.com/oauth/access_token",headers:{Authorization:f.authorization_header},params:{oauth_verifier:c.oauth_verifier}}).success(function(e){for(var n=e.split("&"),t={},r=0;r<n.length;r++)t[n[r].split("=")[0]]=n[r].split("=")[1];t.hasOwnProperty("oauth_token_secret")===!1&&a.reject("Oauth access token was not received"),a.resolve(t)}).error(function(e){a.reject(e)})["finally"](function(){setTimeout(function(){l.close()},10)})}}),l.addEventListener("exit",function(e){a.reject("The sign in flow was canceled")})}).error(function(e){a.reject(e)})}else a.reject("Missing jsSHA JavaScript library")}else a.reject("Could not find InAppBrowser plugin")}else a.reject("Cannot authenticate via a web browser");return a.promise},meetup:function(n,r){var o=e.defer();if(window.cordova){var i=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(i)===!0){var a="http://localhost/callback";void 0!==r&&r.hasOwnProperty("redirect_uri")&&(a=r.redirect_uri);var c=window.open("https://secure.meetup.com/oauth2/authorize/?client_id="+n+"&redirect_uri="+a+"&response_type=token","_blank","location=no,clearsessioncache=yes,clearcache=yes");c.addEventListener("loadstart",function(e){if(0===e.url.indexOf(a)){c.removeEventListener("exit",function(e){}),c.close();for(var n=e.url.split("#")[1],t=n.split("&"),r={},i=0;i<t.length;i++)r[t[i].split("=")[0]]=t[i].split("=")[1];void 0!==r.access_token&&null!==r.access_token?o.resolve(r):o.reject("Problem authenticating")}}),c.addEventListener("exit",function(e){o.reject("The sign in flow was canceled")})}else o.reject("Could not find InAppBrowser plugin")}else o.reject("Cannot authenticate via a web browser");return o.promise},salesforce:function(n,r){var o="http://localhost/callback",i=function(e,n,t){return e+"services/oauth2/authorize?display=touch&response_type=token&client_id="+escape(n)+"&redirect_uri="+escape(t)},a=function(e,n){return e.substr(0,n.length)===n},c=e.defer();if(window.cordova){var s=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(s)===!0){var u=window.open(i(n,r,o),"_blank","location=no,clearsessioncache=yes,clearcache=yes");u.addEventListener("loadstart",function(e){if(a(e.url,o)){var n={},t=e.url.split("#")[1];if(t){var r=t.split("&");for(var i in r){var s=r[i].split("=");n[s[0]]=unescape(s[1])}}"undefined"==typeof n||"undefined"==typeof n.access_token?c.reject("Problem authenticating"):c.resolve(n),setTimeout(function(){u.close()},10)}}),u.addEventListener("exit",function(e){c.reject("The sign in flow was canceled")})}else c.reject("Could not find InAppBrowser plugin")}else c.reject("Cannot authenticate via a web browser");return c.promise},strava:function(r,o,i,a){var c=e.defer();if(window.cordova){var s=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(s)===!0){var u="http://localhost/callback";void 0!==a&&a.hasOwnProperty("redirect_uri")&&(u=a.redirect_uri);var l=window.open("https://www.strava.com/oauth/authorize?client_id="+r+"&redirect_uri="+u+"&scope="+i.join(",")+"&response_type=code&approval_prompt=force","_blank","location=no,clearsessioncache=yes,clearcache=yes");l.addEventListener("loadstart",function(e){0===e.url.indexOf(u)&&(requestToken=e.url.split("code=")[1],n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:"https://www.strava.com/oauth/token",data:"client_id="+r+"&client_secret="+o+"&code="+requestToken}).success(function(e){c.resolve(e)}).error(function(e,n){c.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){l.close()},10)}))}),l.addEventListener("exit",function(e){c.reject("The sign in flow was canceled")})}else c.reject("Could not find InAppBrowser plugin")}else c.reject("Cannot authenticate via a web browser");return c.promise},withings:function(r,o){var i=e.defer();if(window.cordova){var a=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(a)===!0)if("undefined"!=typeof jsSHA){var c=t.generateOauthParametersInstance(r);c.oauth_callback="http://localhost/callback";var s="https://oauth.withings.com/account/request_token",u=t.createSignature("GET",s,{},c,o);c.oauth_signature=u.signature;var l=t.generateUrlParameters(c);n({method:"get",url:s+"?"+l}).success(function(e){var a=t.parseResponseParameters(e);a.hasOwnProperty("oauth_token")===!1&&i.reject("Oauth request token was not received");var c=t.generateOauthParametersInstance(r);c.oauth_token=a.oauth_token;var s=a.oauth_token_secret,u="https://oauth.withings.com/account/authorize",l=t.createSignature("GET",u,{},c,o);c.oauth_signature=l.signature;var d=t.generateUrlParameters(c),f=window.open(u+"?"+d,"_blank","location=no,clearsessioncache=yes,clearcache=yes");f.addEventListener("loadstart",function(e){
if(0===e.url.indexOf("http://localhost/callback")){var c=e.url.split("?")[1];a=t.parseResponseParameters(c),a.hasOwnProperty("oauth_verifier")===!1&&i.reject("Browser authentication failed to complete.  No oauth_verifier was returned");var u=t.generateOauthParametersInstance(r);u.oauth_token=a.oauth_token;var l="https://oauth.withings.com/account/access_token",d=t.createSignature("GET",l,{},u,o,s);u.oauth_signature=d.signature;var p=t.generateUrlParameters(u);n({method:"get",url:l+"?"+p}).success(function(e){var n=t.parseResponseParameters(e);n.hasOwnProperty("oauth_token_secret")===!1&&i.reject("Oauth access token was not received"),i.resolve(n)}).error(function(e){i.reject(e)})["finally"](function(){setTimeout(function(){f.close()},10)})}}),f.addEventListener("exit",function(e){i.reject("The sign in flow was canceled")})}).error(function(e){i.reject(e)})}else i.reject("Missing jsSHA JavaScript library");else i.reject("Could not find InAppBrowser plugin")}else i.reject("Cannot authenticate via a web browser");return i.promise},foursquare:function(n,r){var o=e.defer();if(window.cordova){var i=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(i)===!0){var a="http://localhost/callback";void 0!==r&&r.hasOwnProperty("redirect_uri")&&(a=r.redirect_uri);var c=window.open("https://foursquare.com/oauth2/authenticate?client_id="+n+"&redirect_uri="+a+"&response_type=token","_blank","location=no,clearsessioncache=yes,clearcache=yes");c.addEventListener("loadstart",function(e){if(0===e.url.indexOf(a)){c.removeEventListener("exit",function(e){}),c.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],i=0;i<t.length;i++)r[t[i].split("=")[0]]=t[i].split("=")[1];if(void 0!==r.access_token&&null!==r.access_token){var s={access_token:r.access_token,expires_in:r.expires_in};o.resolve(s)}else o.reject("Problem authenticating")}}),c.addEventListener("exit",function(e){o.reject("The sign in flow was canceled")})}else o.reject("Could not find InAppBrowser plugin")}else o.reject("Cannot authenticate via a web browser");return o.promise},magento:function(r,o,i){var a=e.defer();if(window.cordova){var c=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(c)===!0)if("undefined"!=typeof jsSHA){var s={oauth_callback:"http://localhost/callback",oauth_consumer_key:o,oauth_nonce:t.createNonce(5),oauth_signature_method:"HMAC-SHA1",oauth_timestamp:Math.round((new Date).getTime()/1e3),oauth_version:"1.0"},u=t.createSignature("POST",r+"/oauth/initiate",s,{oauth_callback:"http://localhost/callback"},i);n.defaults.headers.post.Authorization=u.authorization_header,n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:r+"/oauth/initiate",data:"oauth_callback=http://localhost/callback"}).success(function(e){for(var o=e.split("&"),c={},u=0;u<o.length;u++)c[o[u].split("=")[0]]=o[u].split("=")[1];c.hasOwnProperty("oauth_token")===!1&&a.reject("Oauth request token was not received");var l=c.oauth_token_secret,d=window.open(r+"/oauth/authorize?oauth_token="+c.oauth_token,"_blank","location=no,clearsessioncache=yes,clearcache=yes");d.addEventListener("loadstart",function(e){if(0===e.url.indexOf("http://localhost/callback")){for(var o=e.url.split("?")[1],c=o.split("&"),u={},f=0;f<c.length;f++)u[c[f].split("=")[0]]=c[f].split("=")[1];u.hasOwnProperty("oauth_verifier")===!1&&a.reject("Browser authentication failed to complete.  No oauth_verifier was returned"),delete s.oauth_signature,delete s.oauth_callback,s.oauth_token=u.oauth_token,s.oauth_nonce=t.createNonce(5),s.oauth_verifier=u.oauth_verifier;var p=t.createSignature("POST",r+"/oauth/token",s,{},i,l);n.defaults.headers.post.Authorization=p.authorization_header,n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:r+"/oauth/token"}).success(function(e){for(var n=e.split("&"),t={},r=0;r<n.length;r++)t[n[r].split("=")[0]]=n[r].split("=")[1];t.hasOwnProperty("oauth_token_secret")===!1&&a.reject("Oauth access token was not received"),a.resolve(t)}).error(function(e){a.reject(e)})["finally"](function(){setTimeout(function(){d.close()},10)})}}),d.addEventListener("exit",function(e){a.reject("The sign in flow was canceled")})}).error(function(e){a.reject(e)})}else a.reject("Missing jsSHA JavaScript library");else a.reject("Could not find InAppBrowser plugin")}else a.reject("Cannot authenticate via a web browser");return a.promise},vkontakte:function(n,r){var o=e.defer();if(window.cordova){var i=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(i)===!0){var a=window.open("https://oauth.vk.com/authorize?client_id="+n+"&redirect_uri=http://oauth.vk.com/blank.html&response_type=token&scope="+r.join(",")+"&display=touch&response_type=token","_blank","location=no,clearsessioncache=yes,clearcache=yes");a.addEventListener("loadstart",function(e){var n=e.url.split("#");if("https://oauth.vk.com/blank.html"==n[0]||"http://oauth.vk.com/blank.html"==n[0]){a.removeEventListener("exit",function(e){}),a.close();for(var t=e.url.split("#")[1],r=t.split("&"),i=[],c=0;c<r.length;c++)i[r[c].split("=")[0]]=r[c].split("=")[1];if(void 0!==i.access_token&&null!==i.access_token){var s={access_token:i.access_token,expires_in:i.expires_in};void 0!==i.email&&null!==i.email&&(s.email=i.email),o.resolve(s)}else o.reject("Problem authenticating")}}),a.addEventListener("exit",function(e){o.reject("The sign in flow was canceled")})}else o.reject("Could not find InAppBrowser plugin")}else o.reject("Cannot authenticate via a web browser");return o.promise},odnoklassniki:function(n,r){var o=e.defer();if(window.cordova){var i=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(i)===!0){var a=window.open("http://www.odnoklassniki.ru/oauth/authorize?client_id="+n+"&scope="+r.join(",")+"&response_type=token&redirect_uri=http://localhost/callback&layout=m","_blank","location=no,clearsessioncache=yes,clearcache=yes");a.addEventListener("loadstart",function(e){if(0===e.url.indexOf("http://localhost/callback")){for(var n=e.url.split("#")[1],t=n.split("&"),r=[],i=0;i<t.length;i++)r[t[i].split("=")[0]]=t[i].split("=")[1];void 0!==r.access_token&&null!==r.access_token?o.resolve({access_token:r.access_token,session_secret_key:r.session_secret_key}):o.reject("Problem authenticating"),setTimeout(function(){a.close()},10)}}),a.addEventListener("exit",function(e){o.reject("The sign in flow was canceled")})}else o.reject("Could not find InAppBrowser plugin")}else o.reject("Cannot authenticate via a web browser");return o.promise},imgur:function(n,r){var o=e.defer();if(window.cordova){var i=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(i)===!0){var a="http://localhost/callback";void 0!==r&&r.hasOwnProperty("redirect_uri")&&(a=r.redirect_uri);var c=window.open("https://api.imgur.com/oauth2/authorize?client_id="+n+"&response_type=token","_blank","location=no,clearsessioncache=yes,clearcache=yes");c.addEventListener("loadstart",function(e){if(0===e.url.indexOf(a)){c.removeEventListener("exit",function(e){}),c.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],i=0;i<t.length;i++)r[t[i].split("=")[0]]=t[i].split("=")[1];void 0!==r.access_token&&null!==r.access_token?o.resolve({access_token:r.access_token,expires_in:r.expires_in,account_username:r.account_username}):o.reject("Problem authenticating")}}),c.addEventListener("exit",function(e){o.reject("The sign in flow was canceled")})}else o.reject("Could not find InAppBrowser plugin")}else o.reject("Cannot authenticate via a web browser");return o.promise},spotify:function(n,r,o){var i=e.defer();if(window.cordova){var a=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(a)===!0){var c="http://localhost/callback";void 0!==o&&o.hasOwnProperty("redirect_uri")&&(c=o.redirect_uri);var s=window.open("https://accounts.spotify.com/authorize?client_id="+n+"&redirect_uri="+c+"&response_type=token&scope="+r.join(" "),"_blank","location=no,clearsessioncache=yes,clearcache=yes");s.addEventListener("loadstart",function(e){if(0===e.url.indexOf(c)){s.removeEventListener("exit",function(e){}),s.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],o=0;o<t.length;o++)r[t[o].split("=")[0]]=t[o].split("=")[1];void 0!==r.access_token&&null!==r.access_token?i.resolve({access_token:r.access_token,expires_in:r.expires_in,account_username:r.account_username}):i.reject("Problem authenticating")}}),s.addEventListener("exit",function(e){i.reject("The sign in flow was canceled")})}else i.reject("Could not find InAppBrowser plugin")}else i.reject("Cannot authenticate via a web browser");return i.promise},uber:function(n,r,o){var i=e.defer();if(window.cordova){var a=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(a)===!0){var c="http://localhost/callback";void 0!==o&&o.hasOwnProperty("redirect_uri")&&(c=o.redirect_uri);var s=window.open("https://login.uber.com/oauth/authorize?client_id="+n+"&redirect_uri="+c+"&response_type=token&scope="+r.join(" "),"_blank","location=no,clearsessioncache=yes,clearcache=yes");s.addEventListener("loadstart",function(e){if(0===e.url.indexOf(c)){s.removeEventListener("exit",function(e){}),s.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],o=0;o<t.length;o++)r[t[o].split("=")[0]]=t[o].split("=")[1];void 0!==r.access_token&&null!==r.access_token?i.resolve({access_token:r.access_token,token_type:r.token_type,expires_in:r.expires_in,scope:r.scope}):i.reject("Problem authenticating")}}),s.addEventListener("exit",function(e){i.reject("The sign in flow was canceled")})}else i.reject("Could not find InAppBrowser plugin")}else i.reject("Cannot authenticate via a web browser");return i.promise},windowsLive:function(n,r,o){var i=e.defer();if(window.cordova){var a=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(a)===!0){var c="https://login.live.com/oauth20_desktop.srf";void 0!==o&&o.hasOwnProperty("redirect_uri")&&(c=o.redirect_uri);var s=window.open("https://login.live.com/oauth20_authorize.srf?client_id="+n+"&scope="+r.join(",")+"&response_type=token&display=touch&redirect_uri="+c,"_blank","location=no,clearsessioncache=yes,clearcache=yes");s.addEventListener("loadstart",function(e){if(0===e.url.indexOf(c)){s.removeEventListener("exit",function(e){}),s.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],o=0;o<t.length;o++)r[t[o].split("=")[0]]=t[o].split("=")[1];void 0!==r.access_token&&null!==r.access_token?i.resolve({access_token:r.access_token,expires_in:r.expires_in}):i.reject("Problem authenticating")}}),s.addEventListener("exit",function(e){i.reject("The sign in flow was canceled")})}else i.reject("Could not find InAppBrowser plugin")}else i.reject("Cannot authenticate via a web browser");return i.promise},yammer:function(n,r){var o=e.defer();if(window.cordova){var i=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(i)===!0){var a="http://localhost/callback";void 0!==r&&r.hasOwnProperty("redirect_uri")&&(a=r.redirect_uri);var c=window.open("https://www.yammer.com/dialog/oauth?client_id="+n+"&redirect_uri="+a+"&response_type=token","_blank","location=no,clearsessioncache=yes,clearcache=yes");c.addEventListener("loadstart",function(e){if(0===e.url.indexOf(a)){c.removeEventListener("exit",function(e){}),c.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],i=0;i<t.length;i++)r[t[i].split("=")[0]]=t[i].split("=")[1];void 0!==r.access_token&&null!==r.access_token?o.resolve({access_token:r.access_token}):o.reject("Problem authenticating")}}),c.addEventListener("exit",function(e){o.reject("The sign in flow was canceled")})}else o.reject("Could not find InAppBrowser plugin")}else o.reject("Cannot authenticate via a web browser");return o.promise},venmo:function(n,r,o){var i=e.defer();if(window.cordova){var a=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(a)===!0){var c="http://localhost/callback";void 0!==o&&o.hasOwnProperty("redirect_uri")&&(c=o.redirect_uri);var s=window.open("https://api.venmo.com/v1/oauth/authorize?client_id="+n+"&redirect_uri="+c+"&response_type=token&scope="+r.join(" "),"_blank","location=no,clearsessioncache=yes,clearcache=yes");s.addEventListener("loadstart",function(e){if(0===e.url.indexOf(c)){s.removeEventListener("exit",function(e){}),s.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],o=0;o<t.length;o++)r[t[o].split("=")[0]]=t[o].split("=")[1];void 0!==r.access_token&&null!==r.access_token?i.resolve({access_token:r.access_token,expires_in:r.expires_in}):i.reject("Problem authenticating")}}),s.addEventListener("exit",function(e){i.reject("The sign in flow was canceled")})}else i.reject("Could not find InAppBrowser plugin")}else i.reject("Cannot authenticate via a web browser");return i.promise},stripe:function(r,o,i,a){var c=e.defer();if(window.cordova){var s=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(s)===!0){var u="http://localhost/callback";void 0!==a&&a.hasOwnProperty("redirect_uri")&&(u=a.redirect_uri);var l=window.open("https://connect.stripe.com/oauth/authorize?client_id="+r+"&redirect_uri="+u+"&scope="+i+"&response_type=code","_blank","location=no,clearsessioncache=yes,clearcache=yes");l.addEventListener("loadstart",function(e){0===e.url.indexOf("http://localhost/callback")&&(requestToken=e.url.split("code=")[1],n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:"https://connect.stripe.com/oauth/token",data:"client_id="+r+"&client_secret="+o+"&redirect_uri="+u+"&grant_type=authorization_code&code="+requestToken}).success(function(e){c.resolve(e)}).error(function(e,n){c.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){l.close()},10)}))}),l.addEventListener("exit",function(e){c.reject("The sign in flow was canceled")})}else c.reject("Could not find InAppBrowser plugin")}else c.reject("Cannot authenticate via a web browser");return c.promise},rally:function(r,o,i,a){var c=e.defer();if(window.cordova){var s=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(s)===!0){var u="http://localhost/callback";void 0!==a&&a.hasOwnProperty("redirect_uri")&&(u=a.redirect_uri);var l=window.open("https://rally1.rallydev.com/login/oauth2/auth?client_id="+r+"&redirect_uri="+u+"&scope="+i+"&response_type=code","_blank","location=no,clearsessioncache=yes,clearcache=yes");l.addEventListener("loadstart",function(e){0===e.url.indexOf("http://localhost/callback")&&(requestToken=e.url.split("code=")[1],n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:"https://rally1.rallydev.com/login/oauth2/auth",data:"client_id="+r+"&client_secret="+o+"&redirect_uri="+u+"&grant_type=authorization_code&code="+requestToken}).success(function(e){c.resolve(e)}).error(function(e,n){c.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){l.close()},10)}))}),l.addEventListener("exit",function(e){c.reject("The sign in flow was canceled")})}else c.reject("Could not find InAppBrowser plugin")}else c.reject("Cannot authenticate via a web browser");return c.promise},familySearch:function(t,r,o){var i=e.defer();if(window.cordova){var a=cordova.require("cordova/plugin_list").metadata;if(a.hasOwnProperty("cordova-plugin-inappbrowser")===!0){var c="http://localhost/callback";void 0!==o&&o.hasOwnProperty("redirect_uri")&&(c=o.redirect_uri);var s=window.open("https://ident.familysearch.org/cis-web/oauth2/v3/authorization?client_id="+t+"&redirect_uri="+c+"&response_type=code&state="+r,"_blank","location=no,clearsessioncache=yes,clearcache=yes");s.addEventListener("loadstart",function(e){if(0===e.url.indexOf(c)){var r=e.url.split("code=")[1];n.defaults.headers.post["Content-Type"]="application/x-www-form-urlencoded",n({method:"post",url:"https://ident.familysearch.org/cis-web/oauth2/v3/token",data:"client_id="+t+"&redirect_uri="+c+"&grant_type=authorization_code&code="+r}).success(function(e){i.resolve(e)}).error(function(e,n){i.reject("Problem authenticating")})["finally"](function(){setTimeout(function(){s.close()},10)})}}),s.addEventListener("exit",function(e){i.reject("The sign in flow was canceled")})}else i.reject("Could not find InAppBrowser plugin")}else i.reject("Cannot authenticate via a web browser");return i.promise},envato:function(n,r){var o=e.defer();if(window.cordova){var i=cordova.require("cordova/plugin_list").metadata;if(t.isInAppBrowserInstalled(i)===!0){var a="http://localhost/callback";void 0!==r&&r.hasOwnProperty("redirect_uri")&&(a=r.redirect_uri);var c=window.open("https://api.envato.com/authorization?client_id="+n+"&redirect_uri="+a+"&response_type=token","_blank","location=no,clearsessioncache=yes,clearcache=yes");c.addEventListener("loadstart",function(e){if(0===e.url.indexOf(a)){c.removeEventListener("exit",function(e){}),c.close();for(var n=e.url.split("#")[1],t=n.split("&"),r=[],i=0;i<t.length;i++)r[t[i].split("=")[0]]=t[i].split("=")[1];void 0!==r.access_token&&null!==r.access_token?o.resolve({access_token:r.access_token,expires_in:r.expires_in}):o.reject("Problem authenticating")}}),c.addEventListener("exit",function(e){o.reject("The sign in flow was canceled")})}else o.reject("Could not find InAppBrowser plugin")}else o.reject("Cannot authenticate via a web browser");return o.promise}}}]),angular.module("ngCordovaOauth",["oauth.providers","oauth.utils"]),angular.module("oauth.utils",[]).factory("$cordovaOauthUtility",["$q",function(e){return{isInAppBrowserInstalled:function(e){var n=["cordova-plugin-inappbrowser","org.apache.cordova.inappbrowser"];return n.some(function(n){return e.hasOwnProperty(n)})},createSignature:function(e,n,t,r,o,i){if("undefined"!=typeof jsSHA){for(var a=angular.copy(t),c=Object.keys(r),s=0;s<c.length;s++)a[c[s]]=encodeURIComponent(r[c[s]]);var u=e+"&"+encodeURIComponent(n)+"&",l=Object.keys(a).sort();for(s=0;s<l.length;s++)u+=s==l.length-1?encodeURIComponent(l[s]+"="+a[l[s]]):encodeURIComponent(l[s]+"="+a[l[s]]+"&");var d=new jsSHA(u,"TEXT"),f="";i&&(f=encodeURIComponent(i)),t.oauth_signature=encodeURIComponent(d.getHMAC(encodeURIComponent(o)+"&"+f,"TEXT","SHA-1","B64"));var p=Object.keys(t),v="OAuth ";for(s=0;s<p.length;s++)v+=s==p.length-1?p[s]+'="'+t[p[s]]+'"':p[s]+'="'+t[p[s]]+'",';return{signature_base_string:u,authorization_header:v,signature:t.oauth_signature}}return"Missing jsSHA JavaScript library"},createNonce:function(e){for(var n="",t="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",r=0;e>r;r++)n+=t.charAt(Math.floor(Math.random()*t.length));return n},generateUrlParameters:function(e){var n=Object.keys(e);n.sort();for(var t="",r="",o=0;o<n.length;o++)t+=r+n[o]+"="+e[n[o]],r="&";return t},parseResponseParameters:function(e){if(e.split){for(var n=e.split("&"),t={},r=0;r<n.length;r++)t[n[r].split("=")[0]]=n[r].split("=")[1];return t}return{}},generateOauthParametersInstance:function(e){var n=new jsSHA(Math.round((new Date).getTime()/1e3),"TEXT"),t={oauth_consumer_key:e,oauth_nonce:n.getHash("SHA-1","HEX"),oauth_signature_method:"HMAC-SHA1",oauth_timestamp:Math.round((new Date).getTime()/1e3),oauth_version:"1.0"};return t}}}])}();
/**
 * ==================  angular-ios9-uiwebview.patch.js v1.1.1 ==================
 *
 * This patch works around iOS9 UIWebView regression that causes infinite digest
 * errors in Angular.
 *
 * The patch can be applied to Angular 1.2.0 – 1.4.5. Newer versions of Angular
 * have the workaround baked in.
 *
 * To apply this patch load/bundle this file with your application and add a
 * dependency on the "ngIOS9UIWebViewPatch" module to your main app module.
 *
 * For example:
 *
 * ```
 * angular.module('myApp', ['ngRoute'])`
 * ```
 *
 * becomes
 *
 * ```
 * angular.module('myApp', ['ngRoute', 'ngIOS9UIWebViewPatch'])
 * ```
 *
 *
 * More info:
 * - https://openradar.appspot.com/22186109
 * - https://github.com/angular/angular.js/issues/12241
 * - https://github.com/driftyco/ionic/issues/4082
 *
 *
 * @license AngularJS
 * (c) 2010-2015 Google, Inc. http://angularjs.org
 * License: MIT
 */

angular.module('ngIOS9UIWebViewPatch', ['ng']).config(['$provide', function($provide) {
    'use strict';

    $provide.decorator('$browser', ['$delegate', '$window', function($delegate, $window) {

        if (isIOS9UIWebView($window.navigator.userAgent)) {
            return applyIOS9Shim($delegate);
        }

        return $delegate;

        function isIOS9UIWebView(userAgent) {
            return /(iPhone|iPad|iPod).* OS 9_\d/.test(userAgent) && !/Version\/9\./.test(userAgent);
        }

        function applyIOS9Shim(browser) {
            var pendingLocationUrl = null;
            var originalUrlFn= browser.url;

            browser.url = function() {
                if (arguments.length) {
                    pendingLocationUrl = arguments[0];
                    return originalUrlFn.apply(browser, arguments);
                }

                return pendingLocationUrl || originalUrlFn.apply(browser, arguments);
            };

            window.addEventListener('popstate', clearPendingLocationUrl, false);
            window.addEventListener('hashchange', clearPendingLocationUrl, false);

            function clearPendingLocationUrl() {
                pendingLocationUrl = null;
            }

            return browser;
        }
    }]);
}]);
(function () {
    String.prototype.slugify = function (replace) {

        if (!replace) replace = '-';
        var str = this.toString().replace(/[\s\.]+/g, replace).toLowerCase().latinize().replace(new RegExp('[^a-z0-9' + replace + ']', 'g'), replace).replace(new RegExp(replace + '+', 'g'), replace);
        if (str.charAt(str.length - 1) == replace) str = str.substring(0, str.length - 1);
        return str;

    }


    var Latinise = {};
    Latinise.latin_map = {
        "Á": "A",
        "Ă": "A",
        "Ắ": "A",
        "Ặ": "A",
        "Ằ": "A",
        "Ẳ": "A",
        "Ẵ": "A",
        "Ǎ": "A",
        "Â": "A",
        "Ấ": "A",
        "Ậ": "A",
        "Ầ": "A",
        "Ẩ": "A",
        "Ẫ": "A",
        "Ä": "A",
        "Ǟ": "A",
        "Ȧ": "A",
        "Ǡ": "A",
        "Ạ": "A",
        "Ȁ": "A",
        "À": "A",
        "Ả": "A",
        "Ȃ": "A",
        "Ā": "A",
        "Ą": "A",
        "Å": "A",
        "Ǻ": "A",
        "Ḁ": "A",
        "Ⱥ": "A",
        "Ã": "A",
        "Ꜳ": "AA",
        "Æ": "AE",
        "Ǽ": "AE",
        "Ǣ": "AE",
        "Ꜵ": "AO",
        "Ꜷ": "AU",
        "Ꜹ": "AV",
        "Ꜻ": "AV",
        "Ꜽ": "AY",
        "Ḃ": "B",
        "Ḅ": "B",
        "Ɓ": "B",
        "Ḇ": "B",
        "Ƀ": "B",
        "Ƃ": "B",
        "Ć": "C",
        "Č": "C",
        "Ç": "C",
        "Ḉ": "C",
        "Ĉ": "C",
        "Ċ": "C",
        "Ƈ": "C",
        "Ȼ": "C",
        "Ď": "D",
        "Ḑ": "D",
        "Ḓ": "D",
        "Ḋ": "D",
        "Ḍ": "D",
        "Ɗ": "D",
        "Ḏ": "D",
        "ǲ": "D",
        "ǅ": "D",
        "Đ": "D",
        "Ƌ": "D",
        "Ǳ": "DZ",
        "Ǆ": "DZ",
        "É": "E",
        "Ĕ": "E",
        "Ě": "E",
        "Ȩ": "E",
        "Ḝ": "E",
        "Ê": "E",
        "Ế": "E",
        "Ệ": "E",
        "Ề": "E",
        "Ể": "E",
        "Ễ": "E",
        "Ḙ": "E",
        "Ë": "E",
        "Ė": "E",
        "Ẹ": "E",
        "Ȅ": "E",
        "È": "E",
        "Ẻ": "E",
        "Ȇ": "E",
        "Ē": "E",
        "Ḗ": "E",
        "Ḕ": "E",
        "Ę": "E",
        "Ɇ": "E",
        "Ẽ": "E",
        "Ḛ": "E",
        "Ꝫ": "ET",
        "Ḟ": "F",
        "Ƒ": "F",
        "Ǵ": "G",
        "Ğ": "G",
        "Ǧ": "G",
        "Ģ": "G",
        "Ĝ": "G",
        "Ġ": "G",
        "Ɠ": "G",
        "Ḡ": "G",
        "Ǥ": "G",
        "Ḫ": "H",
        "Ȟ": "H",
        "Ḩ": "H",
        "Ĥ": "H",
        "Ⱨ": "H",
        "Ḧ": "H",
        "Ḣ": "H",
        "Ḥ": "H",
        "Ħ": "H",
        "Í": "I",
        "Ĭ": "I",
        "Ǐ": "I",
        "Î": "I",
        "Ï": "I",
        "Ḯ": "I",
        "İ": "I",
        "Ị": "I",
        "Ȉ": "I",
        "Ì": "I",
        "Ỉ": "I",
        "Ȋ": "I",
        "Ī": "I",
        "Į": "I",
        "Ɨ": "I",
        "Ĩ": "I",
        "Ḭ": "I",
        "Ꝺ": "D",
        "Ꝼ": "F",
        "Ᵹ": "G",
        "Ꞃ": "R",
        "Ꞅ": "S",
        "Ꞇ": "T",
        "Ꝭ": "IS",
        "Ĵ": "J",
        "Ɉ": "J",
        "Ḱ": "K",
        "Ǩ": "K",
        "Ķ": "K",
        "Ⱪ": "K",
        "Ꝃ": "K",
        "Ḳ": "K",
        "Ƙ": "K",
        "Ḵ": "K",
        "Ꝁ": "K",
        "Ꝅ": "K",
        "Ĺ": "L",
        "Ƚ": "L",
        "Ľ": "L",
        "Ļ": "L",
        "Ḽ": "L",
        "Ḷ": "L",
        "Ḹ": "L",
        "Ⱡ": "L",
        "Ꝉ": "L",
        "Ḻ": "L",
        "Ŀ": "L",
        "Ɫ": "L",
        "ǈ": "L",
        "Ł": "L",
        "Ǉ": "LJ",
        "Ḿ": "M",
        "Ṁ": "M",
        "Ṃ": "M",
        "Ɱ": "M",
        "Ń": "N",
        "Ň": "N",
        "Ņ": "N",
        "Ṋ": "N",
        "Ṅ": "N",
        "Ṇ": "N",
        "Ǹ": "N",
        "Ɲ": "N",
        "Ṉ": "N",
        "Ƞ": "N",
        "ǋ": "N",
        "Ñ": "N",
        "Ǌ": "NJ",
        "Ó": "O",
        "Ŏ": "O",
        "Ǒ": "O",
        "Ô": "O",
        "Ố": "O",
        "Ộ": "O",
        "Ồ": "O",
        "Ổ": "O",
        "Ỗ": "O",
        "Ö": "O",
        "Ȫ": "O",
        "Ȯ": "O",
        "Ȱ": "O",
        "Ọ": "O",
        "Ő": "O",
        "Ȍ": "O",
        "Ò": "O",
        "Ỏ": "O",
        "Ơ": "O",
        "Ớ": "O",
        "Ợ": "O",
        "Ờ": "O",
        "Ở": "O",
        "Ỡ": "O",
        "Ȏ": "O",
        "Ꝋ": "O",
        "Ꝍ": "O",
        "Ō": "O",
        "Ṓ": "O",
        "Ṑ": "O",
        "Ɵ": "O",
        "Ǫ": "O",
        "Ǭ": "O",
        "Ø": "O",
        "Ǿ": "O",
        "Õ": "O",
        "Ṍ": "O",
        "Ṏ": "O",
        "Ȭ": "O",
        "Ƣ": "OI",
        "Ꝏ": "OO",
        "Ɛ": "E",
        "Ɔ": "O",
        "Ȣ": "OU",
        "Ṕ": "P",
        "Ṗ": "P",
        "Ꝓ": "P",
        "Ƥ": "P",
        "Ꝕ": "P",
        "Ᵽ": "P",
        "Ꝑ": "P",
        "Ꝙ": "Q",
        "Ꝗ": "Q",
        "Ŕ": "R",
        "Ř": "R",
        "Ŗ": "R",
        "Ṙ": "R",
        "Ṛ": "R",
        "Ṝ": "R",
        "Ȑ": "R",
        "Ȓ": "R",
        "Ṟ": "R",
        "Ɍ": "R",
        "Ɽ": "R",
        "Ꜿ": "C",
        "Ǝ": "E",
        "Ś": "S",
        "Ṥ": "S",
        "Š": "S",
        "Ṧ": "S",
        "Ş": "S",
        "Ŝ": "S",
        "Ș": "S",
        "Ṡ": "S",
        "Ṣ": "S",
        "Ṩ": "S",
        "Ť": "T",
        "Ţ": "T",
        "Ṱ": "T",
        "Ț": "T",
        "Ⱦ": "T",
        "Ṫ": "T",
        "Ṭ": "T",
        "Ƭ": "T",
        "Ṯ": "T",
        "Ʈ": "T",
        "Ŧ": "T",
        "Ɐ": "A",
        "Ꞁ": "L",
        "Ɯ": "M",
        "Ʌ": "V",
        "Ꜩ": "TZ",
        "Ú": "U",
        "Ŭ": "U",
        "Ǔ": "U",
        "Û": "U",
        "Ṷ": "U",
        "Ü": "U",
        "Ǘ": "U",
        "Ǚ": "U",
        "Ǜ": "U",
        "Ǖ": "U",
        "Ṳ": "U",
        "Ụ": "U",
        "Ű": "U",
        "Ȕ": "U",
        "Ù": "U",
        "Ủ": "U",
        "Ư": "U",
        "Ứ": "U",
        "Ự": "U",
        "Ừ": "U",
        "Ử": "U",
        "Ữ": "U",
        "Ȗ": "U",
        "Ū": "U",
        "Ṻ": "U",
        "Ų": "U",
        "Ů": "U",
        "Ũ": "U",
        "Ṹ": "U",
        "Ṵ": "U",
        "Ꝟ": "V",
        "Ṿ": "V",
        "Ʋ": "V",
        "Ṽ": "V",
        "Ꝡ": "VY",
        "Ẃ": "W",
        "Ŵ": "W",
        "Ẅ": "W",
        "Ẇ": "W",
        "Ẉ": "W",
        "Ẁ": "W",
        "Ⱳ": "W",
        "Ẍ": "X",
        "Ẋ": "X",
        "Ý": "Y",
        "Ŷ": "Y",
        "Ÿ": "Y",
        "Ẏ": "Y",
        "Ỵ": "Y",
        "Ỳ": "Y",
        "Ƴ": "Y",
        "Ỷ": "Y",
        "Ỿ": "Y",
        "Ȳ": "Y",
        "Ɏ": "Y",
        "Ỹ": "Y",
        "Ź": "Z",
        "Ž": "Z",
        "Ẑ": "Z",
        "Ⱬ": "Z",
        "Ż": "Z",
        "Ẓ": "Z",
        "Ȥ": "Z",
        "Ẕ": "Z",
        "Ƶ": "Z",
        "Ĳ": "IJ",
        "Œ": "OE",
        "ᴀ": "A",
        "ᴁ": "AE",
        "ʙ": "B",
        "ᴃ": "B",
        "ᴄ": "C",
        "ᴅ": "D",
        "ᴇ": "E",
        "ꜰ": "F",
        "ɢ": "G",
        "ʛ": "G",
        "ʜ": "H",
        "ɪ": "I",
        "ʁ": "R",
        "ᴊ": "J",
        "ᴋ": "K",
        "ʟ": "L",
        "ᴌ": "L",
        "ᴍ": "M",
        "ɴ": "N",
        "ᴏ": "O",
        "ɶ": "OE",
        "ᴐ": "O",
        "ᴕ": "OU",
        "ᴘ": "P",
        "ʀ": "R",
        "ᴎ": "N",
        "ᴙ": "R",
        "ꜱ": "S",
        "ᴛ": "T",
        "ⱻ": "E",
        "ᴚ": "R",
        "ᴜ": "U",
        "ᴠ": "V",
        "ᴡ": "W",
        "ʏ": "Y",
        "ᴢ": "Z",
        "á": "a",
        "ă": "a",
        "ắ": "a",
        "ặ": "a",
        "ằ": "a",
        "ẳ": "a",
        "ẵ": "a",
        "ǎ": "a",
        "â": "a",
        "ấ": "a",
        "ậ": "a",
        "ầ": "a",
        "ẩ": "a",
        "ẫ": "a",
        "ä": "a",
        "ǟ": "a",
        "ȧ": "a",
        "ǡ": "a",
        "ạ": "a",
        "ȁ": "a",
        "à": "a",
        "ả": "a",
        "ȃ": "a",
        "ā": "a",
        "ą": "a",
        "ᶏ": "a",
        "ẚ": "a",
        "å": "a",
        "ǻ": "a",
        "ḁ": "a",
        "ⱥ": "a",
        "ã": "a",
        "ꜳ": "aa",
        "æ": "ae",
        "ǽ": "ae",
        "ǣ": "ae",
        "ꜵ": "ao",
        "ꜷ": "au",
        "ꜹ": "av",
        "ꜻ": "av",
        "ꜽ": "ay",
        "ḃ": "b",
        "ḅ": "b",
        "ɓ": "b",
        "ḇ": "b",
        "ᵬ": "b",
        "ᶀ": "b",
        "ƀ": "b",
        "ƃ": "b",
        "ɵ": "o",
        "ć": "c",
        "č": "c",
        "ç": "c",
        "ḉ": "c",
        "ĉ": "c",
        "ɕ": "c",
        "ċ": "c",
        "ƈ": "c",
        "ȼ": "c",
        "ď": "d",
        "ḑ": "d",
        "ḓ": "d",
        "ȡ": "d",
        "ḋ": "d",
        "ḍ": "d",
        "ɗ": "d",
        "ᶑ": "d",
        "ḏ": "d",
        "ᵭ": "d",
        "ᶁ": "d",
        "đ": "d",
        "ɖ": "d",
        "ƌ": "d",
        "ı": "i",
        "ȷ": "j",
        "ɟ": "j",
        "ʄ": "j",
        "ǳ": "dz",
        "ǆ": "dz",
        "é": "e",
        "ĕ": "e",
        "ě": "e",
        "ȩ": "e",
        "ḝ": "e",
        "ê": "e",
        "ế": "e",
        "ệ": "e",
        "ề": "e",
        "ể": "e",
        "ễ": "e",
        "ḙ": "e",
        "ë": "e",
        "ė": "e",
        "ẹ": "e",
        "ȅ": "e",
        "è": "e",
        "ẻ": "e",
        "ȇ": "e",
        "ē": "e",
        "ḗ": "e",
        "ḕ": "e",
        "ⱸ": "e",
        "ę": "e",
        "ᶒ": "e",
        "ɇ": "e",
        "ẽ": "e",
        "ḛ": "e",
        "ꝫ": "et",
        "ḟ": "f",
        "ƒ": "f",
        "ᵮ": "f",
        "ᶂ": "f",
        "ǵ": "g",
        "ğ": "g",
        "ǧ": "g",
        "ģ": "g",
        "ĝ": "g",
        "ġ": "g",
        "ɠ": "g",
        "ḡ": "g",
        "ᶃ": "g",
        "ǥ": "g",
        "ḫ": "h",
        "ȟ": "h",
        "ḩ": "h",
        "ĥ": "h",
        "ⱨ": "h",
        "ḧ": "h",
        "ḣ": "h",
        "ḥ": "h",
        "ɦ": "h",
        "ẖ": "h",
        "ħ": "h",
        "ƕ": "hv",
        "í": "i",
        "ĭ": "i",
        "ǐ": "i",
        "î": "i",
        "ï": "i",
        "ḯ": "i",
        "ị": "i",
        "ȉ": "i",
        "ì": "i",
        "ỉ": "i",
        "ȋ": "i",
        "ī": "i",
        "į": "i",
        "ᶖ": "i",
        "ɨ": "i",
        "ĩ": "i",
        "ḭ": "i",
        "ꝺ": "d",
        "ꝼ": "f",
        "ᵹ": "g",
        "ꞃ": "r",
        "ꞅ": "s",
        "ꞇ": "t",
        "ꝭ": "is",
        "ǰ": "j",
        "ĵ": "j",
        "ʝ": "j",
        "ɉ": "j",
        "ḱ": "k",
        "ǩ": "k",
        "ķ": "k",
        "ⱪ": "k",
        "ꝃ": "k",
        "ḳ": "k",
        "ƙ": "k",
        "ḵ": "k",
        "ᶄ": "k",
        "ꝁ": "k",
        "ꝅ": "k",
        "ĺ": "l",
        "ƚ": "l",
        "ɬ": "l",
        "ľ": "l",
        "ļ": "l",
        "ḽ": "l",
        "ȴ": "l",
        "ḷ": "l",
        "ḹ": "l",
        "ⱡ": "l",
        "ꝉ": "l",
        "ḻ": "l",
        "ŀ": "l",
        "ɫ": "l",
        "ᶅ": "l",
        "ɭ": "l",
        "ł": "l",
        "ǉ": "lj",
        "ſ": "s",
        "ẜ": "s",
        "ẛ": "s",
        "ẝ": "s",
        "ḿ": "m",
        "ṁ": "m",
        "ṃ": "m",
        "ɱ": "m",
        "ᵯ": "m",
        "ᶆ": "m",
        "ń": "n",
        "ň": "n",
        "ņ": "n",
        "ṋ": "n",
        "ȵ": "n",
        "ṅ": "n",
        "ṇ": "n",
        "ǹ": "n",
        "ɲ": "n",
        "ṉ": "n",
        "ƞ": "n",
        "ᵰ": "n",
        "ᶇ": "n",
        "ɳ": "n",
        "ñ": "n",
        "ǌ": "nj",
        "ó": "o",
        "ŏ": "o",
        "ǒ": "o",
        "ô": "o",
        "ố": "o",
        "ộ": "o",
        "ồ": "o",
        "ổ": "o",
        "ỗ": "o",
        "ö": "o",
        "ȫ": "o",
        "ȯ": "o",
        "ȱ": "o",
        "ọ": "o",
        "ő": "o",
        "ȍ": "o",
        "ò": "o",
        "ỏ": "o",
        "ơ": "o",
        "ớ": "o",
        "ợ": "o",
        "ờ": "o",
        "ở": "o",
        "ỡ": "o",
        "ȏ": "o",
        "ꝋ": "o",
        "ꝍ": "o",
        "ⱺ": "o",
        "ō": "o",
        "ṓ": "o",
        "ṑ": "o",
        "ǫ": "o",
        "ǭ": "o",
        "ø": "o",
        "ǿ": "o",
        "õ": "o",
        "ṍ": "o",
        "ṏ": "o",
        "ȭ": "o",
        "ƣ": "oi",
        "ꝏ": "oo",
        "ɛ": "e",
        "ᶓ": "e",
        "ɔ": "o",
        "ᶗ": "o",
        "ȣ": "ou",
        "ṕ": "p",
        "ṗ": "p",
        "ꝓ": "p",
        "ƥ": "p",
        "ᵱ": "p",
        "ᶈ": "p",
        "ꝕ": "p",
        "ᵽ": "p",
        "ꝑ": "p",
        "ꝙ": "q",
        "ʠ": "q",
        "ɋ": "q",
        "ꝗ": "q",
        "ŕ": "r",
        "ř": "r",
        "ŗ": "r",
        "ṙ": "r",
        "ṛ": "r",
        "ṝ": "r",
        "ȑ": "r",
        "ɾ": "r",
        "ᵳ": "r",
        "ȓ": "r",
        "ṟ": "r",
        "ɼ": "r",
        "ᵲ": "r",
        "ᶉ": "r",
        "ɍ": "r",
        "ɽ": "r",
        "ↄ": "c",
        "ꜿ": "c",
        "ɘ": "e",
        "ɿ": "r",
        "ś": "s",
        "ṥ": "s",
        "š": "s",
        "ṧ": "s",
        "ş": "s",
        "ŝ": "s",
        "ș": "s",
        "ṡ": "s",
        "ṣ": "s",
        "ṩ": "s",
        "ʂ": "s",
        "ᵴ": "s",
        "ᶊ": "s",
        "ȿ": "s",
        "ɡ": "g",
        "ᴑ": "o",
        "ᴓ": "o",
        "ᴝ": "u",
        "ť": "t",
        "ţ": "t",
        "ṱ": "t",
        "ț": "t",
        "ȶ": "t",
        "ẗ": "t",
        "ⱦ": "t",
        "ṫ": "t",
        "ṭ": "t",
        "ƭ": "t",
        "ṯ": "t",
        "ᵵ": "t",
        "ƫ": "t",
        "ʈ": "t",
        "ŧ": "t",
        "ᵺ": "th",
        "ɐ": "a",
        "ᴂ": "ae",
        "ǝ": "e",
        "ᵷ": "g",
        "ɥ": "h",
        "ʮ": "h",
        "ʯ": "h",
        "ᴉ": "i",
        "ʞ": "k",
        "ꞁ": "l",
        "ɯ": "m",
        "ɰ": "m",
        "ᴔ": "oe",
        "ɹ": "r",
        "ɻ": "r",
        "ɺ": "r",
        "ⱹ": "r",
        "ʇ": "t",
        "ʌ": "v",
        "ʍ": "w",
        "ʎ": "y",
        "ꜩ": "tz",
        "ú": "u",
        "ŭ": "u",
        "ǔ": "u",
        "û": "u",
        "ṷ": "u",
        "ü": "u",
        "ǘ": "u",
        "ǚ": "u",
        "ǜ": "u",
        "ǖ": "u",
        "ṳ": "u",
        "ụ": "u",
        "ű": "u",
        "ȕ": "u",
        "ù": "u",
        "ủ": "u",
        "ư": "u",
        "ứ": "u",
        "ự": "u",
        "ừ": "u",
        "ử": "u",
        "ữ": "u",
        "ȗ": "u",
        "ū": "u",
        "ṻ": "u",
        "ų": "u",
        "ᶙ": "u",
        "ů": "u",
        "ũ": "u",
        "ṹ": "u",
        "ṵ": "u",
        "ᵫ": "ue",
        "ꝸ": "um",
        "ⱴ": "v",
        "ꝟ": "v",
        "ṿ": "v",
        "ʋ": "v",
        "ᶌ": "v",
        "ⱱ": "v",
        "ṽ": "v",
        "ꝡ": "vy",
        "ẃ": "w",
        "ŵ": "w",
        "ẅ": "w",
        "ẇ": "w",
        "ẉ": "w",
        "ẁ": "w",
        "ⱳ": "w",
        "ẘ": "w",
        "ẍ": "x",
        "ẋ": "x",
        "ᶍ": "x",
        "ý": "y",
        "ŷ": "y",
        "ÿ": "y",
        "ẏ": "y",
        "ỵ": "y",
        "ỳ": "y",
        "ƴ": "y",
        "ỷ": "y",
        "ỿ": "y",
        "ȳ": "y",
        "ẙ": "y",
        "ɏ": "y",
        "ỹ": "y",
        "ź": "z",
        "ž": "z",
        "ẑ": "z",
        "ʑ": "z",
        "ⱬ": "z",
        "ż": "z",
        "ẓ": "z",
        "ȥ": "z",
        "ẕ": "z",
        "ᵶ": "z",
        "ᶎ": "z",
        "ʐ": "z",
        "ƶ": "z",
        "ɀ": "z",
        "ﬀ": "ff",
        "ﬃ": "ffi",
        "ﬄ": "ffl",
        "ﬁ": "fi",
        "ﬂ": "fl",
        "ĳ": "ij",
        "œ": "oe",
        "ﬆ": "st",
        "ₐ": "a",
        "ₑ": "e",
        "ᵢ": "i",
        "ⱼ": "j",
        "ₒ": "o",
        "ᵣ": "r",
        "ᵤ": "u",
        "ᵥ": "v",
        "ₓ": "x"
    };

    String.prototype.latinise = function () {
        return this.replace(/[^A-Za-z0-9\[\] ]/g, function (a) {
            return Latinise.latin_map[a] || a
        })
    };
    String.prototype.latinize = String.prototype.latinise;
    String.prototype.isLatin = function () {
        return this == this.latinise()
    }

})();
app.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $ionicNativeTransitionsProvider) {
    // $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.scrolling.jsScrolling(false);
    $ionicConfigProvider.views.maxCache(3);

    $ionicNativeTransitionsProvider.setDefaultOptions({
        duration: 400, // in milliseconds (ms), default 400,
        slowdownfactor: 4, // overlap views (higher number is more) or no overlap (1), default 4
        iosdelay: -1, // ms to wait for the iOS webview to update before animation kicks in, default -1
        androiddelay: -1, // same as above but for Android, default -1
        winphonedelay: -1, // same as above but for Windows Phone, default -1,
        fixedPixelsTop: 0, // the number of pixels of your fixed header, default 0 (iOS and Android)
        fixedPixelsBottom: 0, // the number of pixels of your fixed footer (f.i. a tab bar), default 0 (iOS and Android)
        triggerTransitionEvent: '$ionicView.afterEnter', // internal ionic-native-transitions option
        backInOppositeDirection: false // Takes over default back transition and state back transition to use the opposite direction transition to go back
    });
    // @todo: REMOVER EM PRODUÇÃO
    // desabilita o cache global
    // $ionicConfigProvider.views.maxCache(0);


    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html'
        })
        .state('home', {
            url: '/home',
            templateUrl: 'templates/tab-home.html'
        })
        .state('delivery', {
            url: '/delivery',
            templateUrl: 'templates/tab-delivery.html',
            controller: 'deliveryController',
            nativeTransitionsAndroid: {
                "type": "flip",
                "direction": "right"
            },
        })
        .state('festas', {
            url: '/festas',
            templateUrl: 'templates/tab-festas.html',
            controller: 'partiesController',
            nativeTransitionsAndroid: {
                "type": "flip",
                "direction": "right"
            },
        })
        .state('zelador', {
            url: '/zelador',
            templateUrl: 'templates/tab-zelador.html'
        })
        .state('relatorios', {
            url: '/relatorios',
            templateUrl: 'templates/tab-relatorios.html'
        })
        .state('change', {
            url: '/change',
            templateUrl: 'templates/change-pass.html'
        })
        .state('choosePhone', {
            url: '/choosePhone',
            templateUrl: 'templates/choose-phone.html'
        });

    // if none of the above states are matched, use this as the fallback
    // $urlRouterProvider.when("", "/login");


    //$urlRouterProvider.when("/", "/login");
    //$urlRouterProvider.otherwise('/login');
});


app.controller("deliveryController", function ($scope, $ionicPopup, $rootScope, $http) {
    $scope.showCodes = false;

    // DELIVERY FORM
    $scope.deliveries = [];
    $scope.requestDelivery = function () {
        var user = JSON.parse(localStorage.getItem('user'));
        var device = device || 'device';
        var platform = platform || 'platform';
        if (platform.indexOf("iPhone") == 0) {
            platform = 'iPhone'
        } else {
            platform = 'Android'
        }

        $scope.deliveryForm = {
            user: user.data.USER_ID,
            dataIni: moment().format('YYYY/MM/DD'),
            horaIni: moment().format('HH:mm'),
            qtde: 1,
            celular: user.data.USER_CEL,
            tipo: 'D',
            plataforma: platform,
            device: device
        }
        $scope.savingDelivery = true;

        $http({
            url: domain + '/access',
            method: 'POST',
            data: $scope.deliveryForm
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                // senha solicitada

                var alertPopup = $ionicPopup.alert({
                    title: " ",
                    template: data.message,
                    cssClass: 'popStyle'
                });
                alertPopup.then(function (res) {
                    $scope.showCodes = true;

                    var delivery = {code: data.code, codeTime: moment().format('DD/MM/YYYY HH:mm')};
                    $scope.deliveries.push(delivery);

                    var itemsFromLocalStorage = [];

                    if (JSON.parse(localStorage.getItem('deliveryCode')) == null) {
                        localStorage.setItem('deliveryCode', JSON.stringify(itemsFromLocalStorage));
                        // console.log('criou localStorage');
                    }

                    var newCodes = [];
                    newCodes = JSON.parse(localStorage.getItem('deliveryCode'));
                    newCodes.push(delivery);

                    // console.log(newCodes);
                    localStorage.setItem('deliveryCode', JSON.stringify(newCodes));
                });
                $scope.savingDelivery = false;

            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingDelivery = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingDelivery = false;
        });
    };

    // LOAD CODES FROM LOCALSTORAGE
    if (JSON.parse(localStorage.getItem('deliveryCode')) !== null) {
        $scope.showCodes = true;
        $scope.deliveries = JSON.parse(localStorage.getItem('deliveryCode'));

        var i;
        for (i = $scope.deliveries.length - 1; i >= 0; i -= 1) {
            if (moment($scope.deliveries[i].codeTime, 'DD/MM/YYYY HH:mm').add(8, 'h').unix() < moment().unix()) {
                $scope.deliveries.splice(i, 1);

                console.log('perdeu o prazo ' + i);
                localStorage.setItem('deliveryCode', JSON.stringify($scope.deliveries));
            }
        }

    } else {
        $scope.showCodes = false;
    }

});

/*! jQuery v3.0.0 | (c) jQuery Foundation | jquery.org/license */
!function(a,b){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){"use strict";var c=[],d=a.document,e=Object.getPrototypeOf,f=c.slice,g=c.concat,h=c.push,i=c.indexOf,j={},k=j.toString,l=j.hasOwnProperty,m=l.toString,n=m.call(Object),o={};function p(a,b){b=b||d;var c=b.createElement("script");c.text=a,b.head.appendChild(c).parentNode.removeChild(c)}var q="3.0.0",r=function(a,b){return new r.fn.init(a,b)},s=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,t=/^-ms-/,u=/-([a-z])/g,v=function(a,b){return b.toUpperCase()};r.fn=r.prototype={jquery:q,constructor:r,length:0,toArray:function(){return f.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:f.call(this)},pushStack:function(a){var b=r.merge(this.constructor(),a);return b.prevObject=this,b},each:function(a){return r.each(this,a)},map:function(a){return this.pushStack(r.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(f.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor()},push:h,sort:c.sort,splice:c.splice},r.extend=r.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||r.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(a=arguments[h]))for(b in a)c=g[b],d=a[b],g!==d&&(j&&d&&(r.isPlainObject(d)||(e=r.isArray(d)))?(e?(e=!1,f=c&&r.isArray(c)?c:[]):f=c&&r.isPlainObject(c)?c:{},g[b]=r.extend(j,f,d)):void 0!==d&&(g[b]=d));return g},r.extend({expando:"jQuery"+(q+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===r.type(a)},isArray:Array.isArray,isWindow:function(a){return null!=a&&a===a.window},isNumeric:function(a){var b=r.type(a);return("number"===b||"string"===b)&&!isNaN(a-parseFloat(a))},isPlainObject:function(a){var b,c;return a&&"[object Object]"===k.call(a)?(b=e(a))?(c=l.call(b,"constructor")&&b.constructor,"function"==typeof c&&m.call(c)===n):!0:!1},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?j[k.call(a)]||"object":typeof a},globalEval:function(a){p(a)},camelCase:function(a){return a.replace(t,"ms-").replace(u,v)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b){var c,d=0;if(w(a)){for(c=a.length;c>d;d++)if(b.call(a[d],d,a[d])===!1)break}else for(d in a)if(b.call(a[d],d,a[d])===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(s,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(w(Object(a))?r.merge(c,"string"==typeof a?[a]:a):h.call(c,a)),c},inArray:function(a,b,c){return null==b?-1:i.call(b,a,c)},merge:function(a,b){for(var c=+b.length,d=0,e=a.length;c>d;d++)a[e++]=b[d];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,e,f=0,h=[];if(w(a))for(d=a.length;d>f;f++)e=b(a[f],f,c),null!=e&&h.push(e);else for(f in a)e=b(a[f],f,c),null!=e&&h.push(e);return g.apply([],h)},guid:1,proxy:function(a,b){var c,d,e;return"string"==typeof b&&(c=a[b],b=a,a=c),r.isFunction(a)?(d=f.call(arguments,2),e=function(){return a.apply(b||this,d.concat(f.call(arguments)))},e.guid=a.guid=a.guid||r.guid++,e):void 0},now:Date.now,support:o}),"function"==typeof Symbol&&(r.fn[Symbol.iterator]=c[Symbol.iterator]),r.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){j["[object "+b+"]"]=b.toLowerCase()});function w(a){var b=!!a&&"length"in a&&a.length,c=r.type(a);return"function"===c||r.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var x=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ha(),z=ha(),A=ha(),B=function(a,b){return a===b&&(l=!0),0},C={}.hasOwnProperty,D=[],E=D.pop,F=D.push,G=D.push,H=D.slice,I=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},J="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",K="[\\x20\\t\\r\\n\\f]",L="(?:\\\\.|[\\w-]|[^\x00-\\xa0])+",M="\\["+K+"*("+L+")(?:"+K+"*([*^$|!~]?=)"+K+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+L+"))|)"+K+"*\\]",N=":("+L+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+M+")*)|.*)\\)|)",O=new RegExp(K+"+","g"),P=new RegExp("^"+K+"+|((?:^|[^\\\\])(?:\\\\.)*)"+K+"+$","g"),Q=new RegExp("^"+K+"*,"+K+"*"),R=new RegExp("^"+K+"*([>+~]|"+K+")"+K+"*"),S=new RegExp("="+K+"*([^\\]'\"]*?)"+K+"*\\]","g"),T=new RegExp(N),U=new RegExp("^"+L+"$"),V={ID:new RegExp("^#("+L+")"),CLASS:new RegExp("^\\.("+L+")"),TAG:new RegExp("^("+L+"|[*])"),ATTR:new RegExp("^"+M),PSEUDO:new RegExp("^"+N),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+K+"*(even|odd|(([+-]|)(\\d*)n|)"+K+"*(?:([+-]|)"+K+"*(\\d+)|))"+K+"*\\)|)","i"),bool:new RegExp("^(?:"+J+")$","i"),needsContext:new RegExp("^"+K+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+K+"*((?:-\\d)?\\d*)"+K+"*\\)|)(?=[^-]|$)","i")},W=/^(?:input|select|textarea|button)$/i,X=/^h\d$/i,Y=/^[^{]+\{\s*\[native \w/,Z=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,$=/[+~]/,_=new RegExp("\\\\([\\da-f]{1,6}"+K+"?|("+K+")|.)","ig"),aa=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},ba=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g,ca=function(a,b){return b?"\x00"===a?"\ufffd":a.slice(0,-1)+"\\"+a.charCodeAt(a.length-1).toString(16)+" ":"\\"+a},da=function(){m()},ea=ta(function(a){return a.disabled===!0},{dir:"parentNode",next:"legend"});try{G.apply(D=H.call(v.childNodes),v.childNodes),D[v.childNodes.length].nodeType}catch(fa){G={apply:D.length?function(a,b){F.apply(a,H.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function ga(a,b,d,e){var f,h,j,k,l,o,r,s=b&&b.ownerDocument,w=b?b.nodeType:9;if(d=d||[],"string"!=typeof a||!a||1!==w&&9!==w&&11!==w)return d;if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==w&&(l=Z.exec(a)))if(f=l[1]){if(9===w){if(!(j=b.getElementById(f)))return d;if(j.id===f)return d.push(j),d}else if(s&&(j=s.getElementById(f))&&t(b,j)&&j.id===f)return d.push(j),d}else{if(l[2])return G.apply(d,b.getElementsByTagName(a)),d;if((f=l[3])&&c.getElementsByClassName&&b.getElementsByClassName)return G.apply(d,b.getElementsByClassName(f)),d}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==w)s=b,r=a;else if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(ba,ca):b.setAttribute("id",k=u),o=g(a),h=o.length;while(h--)o[h]="#"+k+" "+sa(o[h]);r=o.join(","),s=$.test(a)&&qa(b.parentNode)||b}if(r)try{return G.apply(d,s.querySelectorAll(r)),d}catch(x){}finally{k===u&&b.removeAttribute("id")}}}return i(a.replace(P,"$1"),b,d,e)}function ha(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ia(a){return a[u]=!0,a}function ja(a){var b=n.createElement("fieldset");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ka(a,b){var c=a.split("|"),e=c.length;while(e--)d.attrHandle[c[e]]=b}function la(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&a.sourceIndex-b.sourceIndex;if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function na(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function oa(a){return function(b){return"label"in b&&b.disabled===a||"form"in b&&b.disabled===a||"form"in b&&b.disabled===!1&&(b.isDisabled===a||b.isDisabled!==!a&&("label"in b||!ea(b))!==a)}}function pa(a){return ia(function(b){return b=+b,ia(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function qa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=ga.support={},f=ga.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=ga.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),v!==n&&(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ja(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ja(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Y.test(n.getElementsByClassName),c.getById=ja(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c?[c]:[]}},d.filter.ID=function(a){var b=a.replace(_,aa);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(_,aa);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=Y.test(n.querySelectorAll))&&(ja(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+K+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+K+"*(?:value|"+J+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ja(function(a){a.innerHTML="<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var b=n.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+K+"*[*^$|!~]?="),2!==a.querySelectorAll(":enabled").length&&q.push(":enabled",":disabled"),o.appendChild(a).disabled=!0,2!==a.querySelectorAll(":disabled").length&&q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=Y.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ja(function(a){c.disconnectedMatch=s.call(a,"*"),s.call(a,"[s!='']:x"),r.push("!=",N)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Y.test(o.compareDocumentPosition),t=b||Y.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?I(k,a)-I(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];if(!e||!f)return a===n?-1:b===n?1:e?-1:f?1:k?I(k,a)-I(k,b):0;if(e===f)return la(a,b);c=a;while(c=c.parentNode)g.unshift(c);c=b;while(c=c.parentNode)h.unshift(c);while(g[d]===h[d])d++;return d?la(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0},n):n},ga.matches=function(a,b){return ga(a,null,null,b)},ga.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(S,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return ga(b,n,null,[a]).length>0},ga.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},ga.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&C.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},ga.escape=function(a){return(a+"").replace(ba,ca)},ga.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},ga.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=ga.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=ga.selectors={cacheLength:50,createPseudo:ia,match:V,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(_,aa),a[3]=(a[3]||a[4]||a[5]||"").replace(_,aa),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||ga.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&ga.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return V.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&T.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(_,aa).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+K+")"+a+"("+K+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=ga.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(O," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;if(q){if(f){while(p){m=b;while(m=m[p])if(h?m.nodeName.toLowerCase()===r:1===m.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];break}}else if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1)while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b))break;return t-=e,t===d||t%d===0&&t/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||ga.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ia(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=I(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ia(function(a){var b=[],c=[],d=h(a.replace(P,"$1"));return d[u]?ia(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ia(function(a){return function(b){return ga(a,b).length>0}}),contains:ia(function(a){return a=a.replace(_,aa),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ia(function(a){return U.test(a||"")||ga.error("unsupported lang: "+a),a=a.replace(_,aa).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:oa(!1),disabled:oa(!0),checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return X.test(a.nodeName)},input:function(a){return W.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:pa(function(){return[0]}),last:pa(function(a,b){return[b-1]}),eq:pa(function(a,b,c){return[0>c?c+b:c]}),even:pa(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:pa(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:pa(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:pa(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=ma(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=na(b);function ra(){}ra.prototype=d.filters=d.pseudos,d.setFilters=new ra,g=ga.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){c&&!(e=Q.exec(h))||(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=R.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(P," ")}),h=h.slice(c.length));for(g in d.filter)!(e=V[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?ga.error(a):z(a,i).slice(0)};function sa(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function ta(a,b,c){var d=b.dir,e=b.next,f=e||d,g=c&&"parentNode"===f,h=x++;return b.first?function(b,c,e){while(b=b[d])if(1===b.nodeType||g)return a(b,c,e)}:function(b,c,i){var j,k,l,m=[w,h];if(i){while(b=b[d])if((1===b.nodeType||g)&&a(b,c,i))return!0}else while(b=b[d])if(1===b.nodeType||g)if(l=b[u]||(b[u]={}),k=l[b.uniqueID]||(l[b.uniqueID]={}),e&&e===b.nodeName.toLowerCase())b=b[d]||b;else{if((j=k[f])&&j[0]===w&&j[1]===h)return m[2]=j[2];if(k[f]=m,m[2]=a(b,c,i))return!0}}}function ua(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function va(a,b,c){for(var d=0,e=b.length;e>d;d++)ga(a,b[d],c);return c}function wa(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(c&&!c(f,d,e)||(g.push(f),j&&b.push(h)));return g}function xa(a,b,c,d,e,f){return d&&!d[u]&&(d=xa(d)),e&&!e[u]&&(e=xa(e,f)),ia(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||va(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:wa(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=wa(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?I(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=wa(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):G.apply(g,r)})}function ya(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ta(function(a){return a===b},h,!0),l=ta(function(a){return I(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[ta(ua(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return xa(i>1&&ua(m),i>1&&sa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(P,"$1"),c,e>i&&ya(a.slice(i,e)),f>e&&ya(a=a.slice(e)),f>e&&sa(a))}m.push(c)}return ua(m)}function za(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||.1,z=x.length;for(k&&(j=g===n||g||k);s!==z&&null!=(l=x[s]);s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);while(q=a[o++])if(q(l,g||n,h)){i.push(l);break}k&&(w=y)}c&&((l=!q&&l)&&r--,f&&t.push(l))}if(r+=s,c&&s!==r){o=0;while(q=b[o++])q(t,u,g,h);if(f){if(r>0)while(s--)t[s]||u[s]||(u[s]=E.call(i));u=wa(u)}G.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&ga.uniqueSort(i)}return k&&(w=y,j=v),t};return c?ia(f):f}return h=ga.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=ya(b[c]),f[u]?d.push(f):e.push(f);f=A(a,za(e,d)),f.selector=a}return f},i=ga.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(_,aa),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=V.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(_,aa),$.test(j[0].type)&&qa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&sa(j),!a)return G.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,!b||$.test(a)&&qa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ja(function(a){return 1&a.compareDocumentPosition(n.createElement("fieldset"))}),ja(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ka("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ja(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ka("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ja(function(a){return null==a.getAttribute("disabled")})||ka(J,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),ga}(a);r.find=x,r.expr=x.selectors,r.expr[":"]=r.expr.pseudos,r.uniqueSort=r.unique=x.uniqueSort,r.text=x.getText,r.isXMLDoc=x.isXML,r.contains=x.contains,r.escapeSelector=x.escape;var y=function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&r(a).is(c))break;d.push(a)}return d},z=function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c},A=r.expr.match.needsContext,B=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,C=/^.[^:#\[\.,]*$/;function D(a,b,c){if(r.isFunction(b))return r.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return r.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(C.test(b))return r.filter(b,a,c);b=r.filter(b,a)}return r.grep(a,function(a){return i.call(b,a)>-1!==c&&1===a.nodeType})}r.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?r.find.matchesSelector(d,a)?[d]:[]:r.find.matches(a,r.grep(b,function(a){return 1===a.nodeType}))},r.fn.extend({find:function(a){var b,c,d=this.length,e=this;if("string"!=typeof a)return this.pushStack(r(a).filter(function(){for(b=0;d>b;b++)if(r.contains(e[b],this))return!0}));for(c=this.pushStack([]),b=0;d>b;b++)r.find(a,e[b],c);return d>1?r.uniqueSort(c):c},filter:function(a){return this.pushStack(D(this,a||[],!1))},not:function(a){return this.pushStack(D(this,a||[],!0))},is:function(a){return!!D(this,"string"==typeof a&&A.test(a)?r(a):a||[],!1).length}});var E,F=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,G=r.fn.init=function(a,b,c){var e,f;if(!a)return this;if(c=c||E,"string"==typeof a){if(e="<"===a[0]&&">"===a[a.length-1]&&a.length>=3?[null,a,null]:F.exec(a),!e||!e[1]&&b)return!b||b.jquery?(b||c).find(a):this.constructor(b).find(a);if(e[1]){if(b=b instanceof r?b[0]:b,r.merge(this,r.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),B.test(e[1])&&r.isPlainObject(b))for(e in b)r.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e]);return this}return f=d.getElementById(e[2]),f&&(this[0]=f,this.length=1),this}return a.nodeType?(this[0]=a,this.length=1,this):r.isFunction(a)?void 0!==c.ready?c.ready(a):a(r):r.makeArray(a,this)};G.prototype=r.fn,E=r(d);var H=/^(?:parents|prev(?:Until|All))/,I={children:!0,contents:!0,next:!0,prev:!0};r.fn.extend({has:function(a){var b=r(a,this),c=b.length;return this.filter(function(){for(var a=0;c>a;a++)if(r.contains(this,b[a]))return!0})},closest:function(a,b){var c,d=0,e=this.length,f=[],g="string"!=typeof a&&r(a);if(!A.test(a))for(;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&r.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?r.uniqueSort(f):f)},index:function(a){return a?"string"==typeof a?i.call(r(a),this[0]):i.call(this,a.jquery?a[0]:a):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(r.uniqueSort(r.merge(this.get(),r(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function J(a,b){while((a=a[b])&&1!==a.nodeType);return a}r.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return y(a,"parentNode")},parentsUntil:function(a,b,c){return y(a,"parentNode",c)},next:function(a){return J(a,"nextSibling")},prev:function(a){return J(a,"previousSibling")},nextAll:function(a){return y(a,"nextSibling")},prevAll:function(a){return y(a,"previousSibling")},nextUntil:function(a,b,c){return y(a,"nextSibling",c)},prevUntil:function(a,b,c){return y(a,"previousSibling",c)},siblings:function(a){return z((a.parentNode||{}).firstChild,a)},children:function(a){return z(a.firstChild)},contents:function(a){return a.contentDocument||r.merge([],a.childNodes)}},function(a,b){r.fn[a]=function(c,d){var e=r.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=r.filter(d,e)),this.length>1&&(I[a]||r.uniqueSort(e),H.test(a)&&e.reverse()),this.pushStack(e)}});var K=/\S+/g;function L(a){var b={};return r.each(a.match(K)||[],function(a,c){b[c]=!0}),b}r.Callbacks=function(a){a="string"==typeof a?L(a):r.extend({},a);var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;g.length;h=-1){c=g.shift();while(++h<f.length)f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){r.each(b,function(b,c){r.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==r.type(c)&&d(c)})}(arguments),c&&!b&&i()),this},remove:function(){return r.each(arguments,function(a,b){var c;while((c=r.inArray(b,f,c))>-1)f.splice(c,1),h>=c&&h--}),this},has:function(a){return a?r.inArray(a,f)>-1:f.length>0},empty:function(){return f&&(f=[]),this},disable:function(){return e=g=[],f=c="",this},disabled:function(){return!f},lock:function(){return e=g=[],c||b||(f=c=""),this},locked:function(){return!!e},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this},fire:function(){return j.fireWith(this,arguments),this},fired:function(){return!!d}};return j};function M(a){return a}function N(a){throw a}function O(a,b,c){var d;try{a&&r.isFunction(d=a.promise)?d.call(a).done(b).fail(c):a&&r.isFunction(d=a.then)?d.call(a,b,c):b.call(void 0,a)}catch(a){c.call(void 0,a)}}r.extend({Deferred:function(b){var c=[["notify","progress",r.Callbacks("memory"),r.Callbacks("memory"),2],["resolve","done",r.Callbacks("once memory"),r.Callbacks("once memory"),0,"resolved"],["reject","fail",r.Callbacks("once memory"),r.Callbacks("once memory"),1,"rejected"]],d="pending",e={state:function(){return d},always:function(){return f.done(arguments).fail(arguments),this},"catch":function(a){return e.then(null,a)},pipe:function(){var a=arguments;return r.Deferred(function(b){r.each(c,function(c,d){var e=r.isFunction(a[d[4]])&&a[d[4]];f[d[1]](function(){var a=e&&e.apply(this,arguments);a&&r.isFunction(a.promise)?a.promise().progress(b.notify).done(b.resolve).fail(b.reject):b[d[0]+"With"](this,e?[a]:arguments)})}),a=null}).promise()},then:function(b,d,e){var f=0;function g(b,c,d,e){return function(){var h=this,i=arguments,j=function(){var a,j;if(!(f>b)){if(a=d.apply(h,i),a===c.promise())throw new TypeError("Thenable self-resolution");j=a&&("object"==typeof a||"function"==typeof a)&&a.then,r.isFunction(j)?e?j.call(a,g(f,c,M,e),g(f,c,N,e)):(f++,j.call(a,g(f,c,M,e),g(f,c,N,e),g(f,c,M,c.notifyWith))):(d!==M&&(h=void 0,i=[a]),(e||c.resolveWith)(h,i))}},k=e?j:function(){try{j()}catch(a){r.Deferred.exceptionHook&&r.Deferred.exceptionHook(a,k.stackTrace),b+1>=f&&(d!==N&&(h=void 0,i=[a]),c.rejectWith(h,i))}};b?k():(r.Deferred.getStackHook&&(k.stackTrace=r.Deferred.getStackHook()),a.setTimeout(k))}}return r.Deferred(function(a){c[0][3].add(g(0,a,r.isFunction(e)?e:M,a.notifyWith)),c[1][3].add(g(0,a,r.isFunction(b)?b:M)),c[2][3].add(g(0,a,r.isFunction(d)?d:N))}).promise()},promise:function(a){return null!=a?r.extend(a,e):e}},f={};return r.each(c,function(a,b){var g=b[2],h=b[5];e[b[1]]=g.add,h&&g.add(function(){d=h},c[3-a][2].disable,c[0][2].lock),g.add(b[3].fire),f[b[0]]=function(){return f[b[0]+"With"](this===f?void 0:this,arguments),this},f[b[0]+"With"]=g.fireWith}),e.promise(f),b&&b.call(f,f),f},when:function(a){var b=arguments.length,c=b,d=Array(c),e=f.call(arguments),g=r.Deferred(),h=function(a){return function(c){d[a]=this,e[a]=arguments.length>1?f.call(arguments):c,--b||g.resolveWith(d,e)}};if(1>=b&&(O(a,g.done(h(c)).resolve,g.reject),"pending"===g.state()||r.isFunction(e[c]&&e[c].then)))return g.then();while(c--)O(e[c],h(c),g.reject);return g.promise()}});var P=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;r.Deferred.exceptionHook=function(b,c){a.console&&a.console.warn&&b&&P.test(b.name)&&a.console.warn("jQuery.Deferred exception: "+b.message,b.stack,c)};var Q=r.Deferred();r.fn.ready=function(a){return Q.then(a),this},r.extend({isReady:!1,readyWait:1,holdReady:function(a){a?r.readyWait++:r.ready(!0)},ready:function(a){(a===!0?--r.readyWait:r.isReady)||(r.isReady=!0,a!==!0&&--r.readyWait>0||Q.resolveWith(d,[r]))}}),r.ready.then=Q.then;function R(){d.removeEventListener("DOMContentLoaded",R),a.removeEventListener("load",R),r.ready()}"complete"===d.readyState||"loading"!==d.readyState&&!d.documentElement.doScroll?a.setTimeout(r.ready):(d.addEventListener("DOMContentLoaded",R),a.addEventListener("load",R));var S=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===r.type(c)){e=!0;for(h in c)S(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,r.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){
        return j.call(r(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},T=function(a){return 1===a.nodeType||9===a.nodeType||!+a.nodeType};function U(){this.expando=r.expando+U.uid++}U.uid=1,U.prototype={cache:function(a){var b=a[this.expando];return b||(b={},T(a)&&(a.nodeType?a[this.expando]=b:Object.defineProperty(a,this.expando,{value:b,configurable:!0}))),b},set:function(a,b,c){var d,e=this.cache(a);if("string"==typeof b)e[r.camelCase(b)]=c;else for(d in b)e[r.camelCase(d)]=b[d];return e},get:function(a,b){return void 0===b?this.cache(a):a[this.expando]&&a[this.expando][r.camelCase(b)]},access:function(a,b,c){return void 0===b||b&&"string"==typeof b&&void 0===c?this.get(a,b):(this.set(a,b,c),void 0!==c?c:b)},remove:function(a,b){var c,d=a[this.expando];if(void 0!==d){if(void 0!==b){r.isArray(b)?b=b.map(r.camelCase):(b=r.camelCase(b),b=b in d?[b]:b.match(K)||[]),c=b.length;while(c--)delete d[b[c]]}(void 0===b||r.isEmptyObject(d))&&(a.nodeType?a[this.expando]=void 0:delete a[this.expando])}},hasData:function(a){var b=a[this.expando];return void 0!==b&&!r.isEmptyObject(b)}};var V=new U,W=new U,X=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,Y=/[A-Z]/g;function Z(a,b,c){var d;if(void 0===c&&1===a.nodeType)if(d="data-"+b.replace(Y,"-$&").toLowerCase(),c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:X.test(c)?JSON.parse(c):c}catch(e){}W.set(a,b,c)}else c=void 0;return c}r.extend({hasData:function(a){return W.hasData(a)||V.hasData(a)},data:function(a,b,c){return W.access(a,b,c)},removeData:function(a,b){W.remove(a,b)},_data:function(a,b,c){return V.access(a,b,c)},_removeData:function(a,b){V.remove(a,b)}}),r.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=W.get(f),1===f.nodeType&&!V.get(f,"hasDataAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=r.camelCase(d.slice(5)),Z(f,d,e[d])));V.set(f,"hasDataAttrs",!0)}return e}return"object"==typeof a?this.each(function(){W.set(this,a)}):S(this,function(b){var c;if(f&&void 0===b){if(c=W.get(f,a),void 0!==c)return c;if(c=Z(f,a),void 0!==c)return c}else this.each(function(){W.set(this,a,b)})},null,b,arguments.length>1,null,!0)},removeData:function(a){return this.each(function(){W.remove(this,a)})}}),r.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=V.get(a,b),c&&(!d||r.isArray(c)?d=V.access(a,b,r.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=r.queue(a,b),d=c.length,e=c.shift(),f=r._queueHooks(a,b),g=function(){r.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return V.get(a,c)||V.access(a,c,{empty:r.Callbacks("once memory").add(function(){V.remove(a,[b+"queue",c])})})}}),r.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?r.queue(this[0],a):void 0===b?this:this.each(function(){var c=r.queue(this,a,b);r._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&r.dequeue(this,a)})},dequeue:function(a){return this.each(function(){r.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=r.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=V.get(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var $=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,_=new RegExp("^(?:([+-])=|)("+$+")([a-z%]*)$","i"),aa=["Top","Right","Bottom","Left"],ba=function(a,b){return a=b||a,"none"===a.style.display||""===a.style.display&&r.contains(a.ownerDocument,a)&&"none"===r.css(a,"display")},ca=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e};function da(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()}:function(){return r.css(a,b,"")},i=h(),j=c&&c[3]||(r.cssNumber[b]?"":"px"),k=(r.cssNumber[b]||"px"!==j&&+i)&&_.exec(r.css(a,b));if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;do f=f||".5",k/=f,r.style(a,b,k+j);while(f!==(f=h()/i)&&1!==f&&--g)}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e}var ea={};function fa(a){var b,c=a.ownerDocument,d=a.nodeName,e=ea[d];return e?e:(b=c.body.appendChild(c.createElement(d)),e=r.css(b,"display"),b.parentNode.removeChild(b),"none"===e&&(e="block"),ea[d]=e,e)}function ga(a,b){for(var c,d,e=[],f=0,g=a.length;g>f;f++)d=a[f],d.style&&(c=d.style.display,b?("none"===c&&(e[f]=V.get(d,"display")||null,e[f]||(d.style.display="")),""===d.style.display&&ba(d)&&(e[f]=fa(d))):"none"!==c&&(e[f]="none",V.set(d,"display",c)));for(f=0;g>f;f++)null!=e[f]&&(a[f].style.display=e[f]);return a}r.fn.extend({show:function(){return ga(this,!0)},hide:function(){return ga(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){ba(this)?r(this).show():r(this).hide()})}});var ha=/^(?:checkbox|radio)$/i,ia=/<([a-z][^\/\0>\x20\t\r\n\f]+)/i,ja=/^$|\/(?:java|ecma)script/i,ka={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};ka.optgroup=ka.option,ka.tbody=ka.tfoot=ka.colgroup=ka.caption=ka.thead,ka.th=ka.td;function la(a,b){var c="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):[];return void 0===b||b&&r.nodeName(a,b)?r.merge([a],c):c}function ma(a,b){for(var c=0,d=a.length;d>c;c++)V.set(a[c],"globalEval",!b||V.get(b[c],"globalEval"))}var na=/<|&#?\w+;/;function oa(a,b,c,d,e){for(var f,g,h,i,j,k,l=b.createDocumentFragment(),m=[],n=0,o=a.length;o>n;n++)if(f=a[n],f||0===f)if("object"===r.type(f))r.merge(m,f.nodeType?[f]:f);else if(na.test(f)){g=g||l.appendChild(b.createElement("div")),h=(ia.exec(f)||["",""])[1].toLowerCase(),i=ka[h]||ka._default,g.innerHTML=i[1]+r.htmlPrefilter(f)+i[2],k=i[0];while(k--)g=g.lastChild;r.merge(m,g.childNodes),g=l.firstChild,g.textContent=""}else m.push(b.createTextNode(f));l.textContent="",n=0;while(f=m[n++])if(d&&r.inArray(f,d)>-1)e&&e.push(f);else if(j=r.contains(f.ownerDocument,f),g=la(l.appendChild(f),"script"),j&&ma(g),c){k=0;while(f=g[k++])ja.test(f.type||"")&&c.push(f)}return l}!function(){var a=d.createDocumentFragment(),b=a.appendChild(d.createElement("div")),c=d.createElement("input");c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),b.appendChild(c),o.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,b.innerHTML="<textarea>x</textarea>",o.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue}();var pa=d.documentElement,qa=/^key/,ra=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,sa=/^([^.]*)(?:\.(.+)|)/;function ta(){return!0}function ua(){return!1}function va(){try{return d.activeElement}catch(a){}}function wa(a,b,c,d,e,f){var g,h;if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);for(h in b)wa(a,h,c,d,b[h],f);return a}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1)e=ua;else if(!e)return a;return 1===f&&(g=e,e=function(a){return r().off(a),g.apply(this,arguments)},e.guid=g.guid||(g.guid=r.guid++)),a.each(function(){r.event.add(this,b,e,d,c)})}r.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,n,o,p,q=V.get(a);if(q){c.handler&&(f=c,c=f.handler,e=f.selector),e&&r.find.matchesSelector(pa,e),c.guid||(c.guid=r.guid++),(i=q.events)||(i=q.events={}),(g=q.handle)||(g=q.handle=function(b){return"undefined"!=typeof r&&r.event.triggered!==b.type?r.event.dispatch.apply(a,arguments):void 0}),b=(b||"").match(K)||[""],j=b.length;while(j--)h=sa.exec(b[j])||[],n=p=h[1],o=(h[2]||"").split(".").sort(),n&&(l=r.event.special[n]||{},n=(e?l.delegateType:l.bindType)||n,l=r.event.special[n]||{},k=r.extend({type:n,origType:p,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&r.expr.match.needsContext.test(e),namespace:o.join(".")},f),(m=i[n])||(m=i[n]=[],m.delegateCount=0,l.setup&&l.setup.call(a,d,o,g)!==!1||a.addEventListener&&a.addEventListener(n,g)),l.add&&(l.add.call(a,k),k.handler.guid||(k.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,k):m.push(k),r.event.global[n]=!0)}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,n,o,p,q=V.hasData(a)&&V.get(a);if(q&&(i=q.events)){b=(b||"").match(K)||[""],j=b.length;while(j--)if(h=sa.exec(b[j])||[],n=p=h[1],o=(h[2]||"").split(".").sort(),n){l=r.event.special[n]||{},n=(d?l.delegateType:l.bindType)||n,m=i[n]||[],h=h[2]&&new RegExp("(^|\\.)"+o.join("\\.(?:.*\\.|)")+"(\\.|$)"),g=f=m.length;while(f--)k=m[f],!e&&p!==k.origType||c&&c.guid!==k.guid||h&&!h.test(k.namespace)||d&&d!==k.selector&&("**"!==d||!k.selector)||(m.splice(f,1),k.selector&&m.delegateCount--,l.remove&&l.remove.call(a,k));g&&!m.length&&(l.teardown&&l.teardown.call(a,o,q.handle)!==!1||r.removeEvent(a,n,q.handle),delete i[n])}else for(n in i)r.event.remove(a,n+b[j],c,d,!0);r.isEmptyObject(i)&&V.remove(a,"handle events")}},dispatch:function(a){var b=r.event.fix(a),c,d,e,f,g,h,i=new Array(arguments.length),j=(V.get(this,"events")||{})[b.type]||[],k=r.event.special[b.type]||{};for(i[0]=b,c=1;c<arguments.length;c++)i[c]=arguments[c];if(b.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,b)!==!1){h=r.event.handlers.call(this,b,j),c=0;while((f=h[c++])&&!b.isPropagationStopped()){b.currentTarget=f.elem,d=0;while((g=f.handlers[d++])&&!b.isImmediatePropagationStopped())b.rnamespace&&!b.rnamespace.test(g.namespace)||(b.handleObj=g,b.data=g.data,e=((r.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==e&&(b.result=e)===!1&&(b.preventDefault(),b.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,b),b.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1))for(;i!==this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?r(e,this).index(i)>-1:r.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},addProp:function(a,b){Object.defineProperty(r.Event.prototype,a,{enumerable:!0,configurable:!0,get:r.isFunction(b)?function(){return this.originalEvent?b(this.originalEvent):void 0}:function(){return this.originalEvent?this.originalEvent[a]:void 0},set:function(b){Object.defineProperty(this,a,{enumerable:!0,configurable:!0,writable:!0,value:b})}})},fix:function(a){return a[r.expando]?a:new r.Event(a)},special:{load:{noBubble:!0},focus:{trigger:function(){return this!==va()&&this.focus?(this.focus(),!1):void 0},delegateType:"focusin"},blur:{trigger:function(){return this===va()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return"checkbox"===this.type&&this.click&&r.nodeName(this,"input")?(this.click(),!1):void 0},_default:function(a){return r.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}}},r.removeEvent=function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)},r.Event=function(a,b){return this instanceof r.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?ta:ua,this.target=a.target&&3===a.target.nodeType?a.target.parentNode:a.target,this.currentTarget=a.currentTarget,this.relatedTarget=a.relatedTarget):this.type=a,b&&r.extend(this,b),this.timeStamp=a&&a.timeStamp||r.now(),void(this[r.expando]=!0)):new r.Event(a,b)},r.Event.prototype={constructor:r.Event,isDefaultPrevented:ua,isPropagationStopped:ua,isImmediatePropagationStopped:ua,isSimulated:!1,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=ta,a&&!this.isSimulated&&a.preventDefault()},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=ta,a&&!this.isSimulated&&a.stopPropagation()},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=ta,a&&!this.isSimulated&&a.stopImmediatePropagation(),this.stopPropagation()}},r.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:function(a){var b=a.button;return null==a.which&&qa.test(a.type)?null!=a.charCode?a.charCode:a.keyCode:!a.which&&void 0!==b&&ra.test(a.type)?1&b?1:2&b?3:4&b?2:0:a.which}},r.event.addProp),r.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){r.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return e&&(e===d||r.contains(d,e))||(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),r.fn.extend({on:function(a,b,c,d){return wa(this,a,b,c,d)},one:function(a,b,c,d){return wa(this,a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,r(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return b!==!1&&"function"!=typeof b||(c=b,b=void 0),c===!1&&(c=ua),this.each(function(){r.event.remove(this,a,c,b)})}});var xa=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,ya=/<script|<style|<link/i,za=/checked\s*(?:[^=]|=\s*.checked.)/i,Aa=/^true\/(.*)/,Ba=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function Ca(a,b){return r.nodeName(a,"table")&&r.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a:a}function Da(a){return a.type=(null!==a.getAttribute("type"))+"/"+a.type,a}function Ea(a){var b=Aa.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function Fa(a,b){var c,d,e,f,g,h,i,j;if(1===b.nodeType){if(V.hasData(a)&&(f=V.access(a),g=V.set(b,f),j=f.events)){delete g.handle,g.events={};for(e in j)for(c=0,d=j[e].length;d>c;c++)r.event.add(b,e,j[e][c])}W.hasData(a)&&(h=W.access(a),i=r.extend({},h),W.set(b,i))}}function Ga(a,b){var c=b.nodeName.toLowerCase();"input"===c&&ha.test(a.type)?b.checked=a.checked:"input"!==c&&"textarea"!==c||(b.defaultValue=a.defaultValue)}function Ha(a,b,c,d){b=g.apply([],b);var e,f,h,i,j,k,l=0,m=a.length,n=m-1,q=b[0],s=r.isFunction(q);if(s||m>1&&"string"==typeof q&&!o.checkClone&&za.test(q))return a.each(function(e){var f=a.eq(e);s&&(b[0]=q.call(this,e,f.html())),Ha(f,b,c,d)});if(m&&(e=oa(b,a[0].ownerDocument,!1,a,d),f=e.firstChild,1===e.childNodes.length&&(e=f),f||d)){for(h=r.map(la(e,"script"),Da),i=h.length;m>l;l++)j=e,l!==n&&(j=r.clone(j,!0,!0),i&&r.merge(h,la(j,"script"))),c.call(a[l],j,l);if(i)for(k=h[h.length-1].ownerDocument,r.map(h,Ea),l=0;i>l;l++)j=h[l],ja.test(j.type||"")&&!V.access(j,"globalEval")&&r.contains(k,j)&&(j.src?r._evalUrl&&r._evalUrl(j.src):p(j.textContent.replace(Ba,""),k))}return a}function Ia(a,b,c){for(var d,e=b?r.filter(b,a):a,f=0;null!=(d=e[f]);f++)c||1!==d.nodeType||r.cleanData(la(d)),d.parentNode&&(c&&r.contains(d.ownerDocument,d)&&ma(la(d,"script")),d.parentNode.removeChild(d));return a}r.extend({htmlPrefilter:function(a){return a.replace(xa,"<$1></$2>")},clone:function(a,b,c){var d,e,f,g,h=a.cloneNode(!0),i=r.contains(a.ownerDocument,a);if(!(o.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||r.isXMLDoc(a)))for(g=la(h),f=la(a),d=0,e=f.length;e>d;d++)Ga(f[d],g[d]);if(b)if(c)for(f=f||la(a),g=g||la(h),d=0,e=f.length;e>d;d++)Fa(f[d],g[d]);else Fa(a,h);return g=la(h,"script"),g.length>0&&ma(g,!i&&la(a,"script")),h},cleanData:function(a){for(var b,c,d,e=r.event.special,f=0;void 0!==(c=a[f]);f++)if(T(c)){if(b=c[V.expando]){if(b.events)for(d in b.events)e[d]?r.event.remove(c,d):r.removeEvent(c,d,b.handle);c[V.expando]=void 0}c[W.expando]&&(c[W.expando]=void 0)}}}),r.fn.extend({detach:function(a){return Ia(this,a,!0)},remove:function(a){return Ia(this,a)},text:function(a){return S(this,function(a){return void 0===a?r.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=a)})},null,a,arguments.length)},append:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.appendChild(a)}})},prepend:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},empty:function(){for(var a,b=0;null!=(a=this[b]);b++)1===a.nodeType&&(r.cleanData(la(a,!1)),a.textContent="");return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return r.clone(this,a,b)})},html:function(a){return S(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a&&1===b.nodeType)return b.innerHTML;if("string"==typeof a&&!ya.test(a)&&!ka[(ia.exec(a)||["",""])[1].toLowerCase()]){a=r.htmlPrefilter(a);try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(r.cleanData(la(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=[];return Ha(this,arguments,function(b){var c=this.parentNode;r.inArray(this,a)<0&&(r.cleanData(la(this)),c&&c.replaceChild(b,this))},a)}}),r.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){r.fn[a]=function(a){for(var c,d=[],e=r(a),f=e.length-1,g=0;f>=g;g++)c=g===f?this:this.clone(!0),r(e[g])[b](c),h.apply(d,c.get());return this.pushStack(d)}});var Ja=/^margin/,Ka=new RegExp("^("+$+")(?!px)[a-z%]+$","i"),La=function(b){var c=b.ownerDocument.defaultView;return c&&c.opener||(c=a),c.getComputedStyle(b)};!function(){function b(){if(i){i.style.cssText="box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",i.innerHTML="",pa.appendChild(h);var b=a.getComputedStyle(i);c="1%"!==b.top,g="2px"===b.marginLeft,e="4px"===b.width,i.style.marginRight="50%",f="4px"===b.marginRight,pa.removeChild(h),i=null}}var c,e,f,g,h=d.createElement("div"),i=d.createElement("div");i.style&&(i.style.backgroundClip="content-box",i.cloneNode(!0).style.backgroundClip="",o.clearCloneStyle="content-box"===i.style.backgroundClip,h.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",h.appendChild(i),r.extend(o,{pixelPosition:function(){return b(),c},boxSizingReliable:function(){return b(),e},pixelMarginRight:function(){return b(),f},reliableMarginLeft:function(){return b(),g}}))}();function Ma(a,b,c){var d,e,f,g,h=a.style;return c=c||La(a),c&&(g=c.getPropertyValue(b)||c[b],""!==g||r.contains(a.ownerDocument,a)||(g=r.style(a,b)),!o.pixelMarginRight()&&Ka.test(g)&&Ja.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0!==g?g+"":g}function Na(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}var Oa=/^(none|table(?!-c[ea]).+)/,Pa={position:"absolute",visibility:"hidden",display:"block"},Qa={letterSpacing:"0",fontWeight:"400"},Ra=["Webkit","Moz","ms"],Sa=d.createElement("div").style;function Ta(a){if(a in Sa)return a;var b=a[0].toUpperCase()+a.slice(1),c=Ra.length;while(c--)if(a=Ra[c]+b,a in Sa)return a}function Ua(a,b,c){var d=_.exec(b);return d?Math.max(0,d[2]-(c||0))+(d[3]||"px"):b}function Va(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=r.css(a,c+aa[f],!0,e)),d?("content"===c&&(g-=r.css(a,"padding"+aa[f],!0,e)),"margin"!==c&&(g-=r.css(a,"border"+aa[f]+"Width",!0,e))):(g+=r.css(a,"padding"+aa[f],!0,e),"padding"!==c&&(g+=r.css(a,"border"+aa[f]+"Width",!0,e)));return g}function Wa(a,b,c){var d,e=!0,f=La(a),g="border-box"===r.css(a,"boxSizing",!1,f);if(a.getClientRects().length&&(d=a.getBoundingClientRect()[b]),0>=d||null==d){if(d=Ma(a,b,f),(0>d||null==d)&&(d=a.style[b]),Ka.test(d))return d;e=g&&(o.boxSizingReliable()||d===a.style[b]),d=parseFloat(d)||0}return d+Va(a,b,c||(g?"border":"content"),e,f)+"px"}r.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Ma(a,"opacity");return""===c?"1":c}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":"cssFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=r.camelCase(b),i=a.style;return b=r.cssProps[h]||(r.cssProps[h]=Ta(h)||h),g=r.cssHooks[b]||r.cssHooks[h],void 0===c?g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b]:(f=typeof c,"string"===f&&(e=_.exec(c))&&e[1]&&(c=da(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(r.cssNumber[h]?"":"px")),o.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),g&&"set"in g&&void 0===(c=g.set(a,c,d))||(i[b]=c)),void 0)}},css:function(a,b,c,d){var e,f,g,h=r.camelCase(b);return b=r.cssProps[h]||(r.cssProps[h]=Ta(h)||h),g=r.cssHooks[b]||r.cssHooks[h],g&&"get"in g&&(e=g.get(a,!0,c)),void 0===e&&(e=Ma(a,b,d)),"normal"===e&&b in Qa&&(e=Qa[b]),""===c||c?(f=parseFloat(e),c===!0||isFinite(f)?f||0:e):e}}),r.each(["height","width"],function(a,b){r.cssHooks[b]={get:function(a,c,d){return c?!Oa.test(r.css(a,"display"))||a.getClientRects().length&&a.getBoundingClientRect().width?Wa(a,b,d):ca(a,Pa,function(){return Wa(a,b,d)}):void 0},set:function(a,c,d){var e,f=d&&La(a),g=d&&Va(a,b,d,"border-box"===r.css(a,"boxSizing",!1,f),f);return g&&(e=_.exec(c))&&"px"!==(e[3]||"px")&&(a.style[b]=c,c=r.css(a,b)),Ua(a,c,g)}}}),r.cssHooks.marginLeft=Na(o.reliableMarginLeft,function(a,b){return b?(parseFloat(Ma(a,"marginLeft"))||a.getBoundingClientRect().left-ca(a,{marginLeft:0},function(){return a.getBoundingClientRect().left}))+"px":void 0}),r.each({margin:"",padding:"",border:"Width"},function(a,b){r.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+aa[d]+b]=f[d]||f[d-2]||f[0];return e}},Ja.test(a)||(r.cssHooks[a+b].set=Ua)}),r.fn.extend({css:function(a,b){return S(this,function(a,b,c){var d,e,f={},g=0;if(r.isArray(b)){for(d=La(a),e=b.length;e>g;g++)f[b[g]]=r.css(a,b[g],!1,d);return f}return void 0!==c?r.style(a,b,c):r.css(a,b)},a,b,arguments.length>1)}});function Xa(a,b,c,d,e){return new Xa.prototype.init(a,b,c,d,e)}r.Tween=Xa,Xa.prototype={constructor:Xa,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||r.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(r.cssNumber[c]?"":"px")},cur:function(){var a=Xa.propHooks[this.prop];return a&&a.get?a.get(this):Xa.propHooks._default.get(this)},run:function(a){var b,c=Xa.propHooks[this.prop];return this.options.duration?this.pos=b=r.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):Xa.propHooks._default.set(this),this}},Xa.prototype.init.prototype=Xa.prototype,Xa.propHooks={_default:{get:function(a){var b;return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=r.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)},set:function(a){r.fx.step[a.prop]?r.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[r.cssProps[a.prop]]&&!r.cssHooks[a.prop]?a.elem[a.prop]=a.now:r.style(a.elem,a.prop,a.now+a.unit)}}},Xa.propHooks.scrollTop=Xa.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},r.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},_default:"swing"},r.fx=Xa.prototype.init,r.fx.step={};var Ya,Za,$a=/^(?:toggle|show|hide)$/,_a=/queueHooks$/;function ab(){Za&&(a.requestAnimationFrame(ab),r.fx.tick())}function bb(){return a.setTimeout(function(){Ya=void 0}),Ya=r.now()}function cb(a,b){var c,d=0,e={height:a};for(b=b?1:0;4>d;d+=2-b)c=aa[d],e["margin"+c]=e["padding"+c]=a;return b&&(e.opacity=e.width=a),e}function db(a,b,c){for(var d,e=(gb.tweeners[b]||[]).concat(gb.tweeners["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function eb(a,b,c){var d,e,f,g,h,i,j,k,l="width"in b||"height"in b,m=this,n={},o=a.style,p=a.nodeType&&ba(a),q=V.get(a,"fxshow");c.queue||(g=r._queueHooks(a,"fx"),null==g.unqueued&&(g.unqueued=0,h=g.empty.fire,g.empty.fire=function(){g.unqueued||h()}),g.unqueued++,m.always(function(){m.always(function(){g.unqueued--,r.queue(a,"fx").length||g.empty.fire()})}));for(d in b)if(e=b[d],$a.test(e)){if(delete b[d],f=f||"toggle"===e,e===(p?"hide":"show")){if("show"!==e||!q||void 0===q[d])continue;p=!0}n[d]=q&&q[d]||r.style(a,d)}if(i=!r.isEmptyObject(b),i||!r.isEmptyObject(n)){l&&1===a.nodeType&&(c.overflow=[o.overflow,o.overflowX,o.overflowY],j=q&&q.display,null==j&&(j=V.get(a,"display")),k=r.css(a,"display"),"none"===k&&(j?k=j:(ga([a],!0),j=a.style.display||j,k=r.css(a,"display"),ga([a]))),("inline"===k||"inline-block"===k&&null!=j)&&"none"===r.css(a,"float")&&(i||(m.done(function(){o.display=j}),null==j&&(k=o.display,j="none"===k?"":k)),o.display="inline-block")),c.overflow&&(o.overflow="hidden",m.always(function(){o.overflow=c.overflow[0],o.overflowX=c.overflow[1],o.overflowY=c.overflow[2]})),i=!1;for(d in n)i||(q?"hidden"in q&&(p=q.hidden):q=V.access(a,"fxshow",{display:j}),f&&(q.hidden=!p),p&&ga([a],!0),m.done(function(){p||ga([a]),V.remove(a,"fxshow");for(d in n)r.style(a,d,n[d])})),i=db(p?q[d]:0,d,m),d in q||(q[d]=i.start,p&&(i.end=i.start,i.start=0))}}function fb(a,b){var c,d,e,f,g;for(c in a)if(d=r.camelCase(c),e=b[d],f=a[c],r.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=r.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function gb(a,b,c){var d,e,f=0,g=gb.prefilters.length,h=r.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=Ya||bb(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:r.extend({},b),opts:r.extend(!0,{specialEasing:{},easing:r.easing._default},c),originalProperties:b,originalOptions:c,startTime:Ya||bb(),duration:c.duration,tweens:[],createTween:function(b,c){var d=r.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this}}),k=j.props;for(fb(k,j.opts.specialEasing);g>f;f++)if(d=gb.prefilters[f].call(j,a,k,j.opts))return r.isFunction(d.stop)&&(r._queueHooks(j.elem,j.opts.queue).stop=r.proxy(d.stop,d)),d;return r.map(k,db,j),r.isFunction(j.opts.start)&&j.opts.start.call(a,j),r.fx.timer(r.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}r.Animation=r.extend(gb,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);return da(c.elem,a,_.exec(b),c),c}]},tweener:function(a,b){r.isFunction(a)?(b=a,a=["*"]):a=a.match(K);for(var c,d=0,e=a.length;e>d;d++)c=a[d],gb.tweeners[c]=gb.tweeners[c]||[],gb.tweeners[c].unshift(b)},prefilters:[eb],prefilter:function(a,b){b?gb.prefilters.unshift(a):gb.prefilters.push(a)}}),r.speed=function(a,b,c){var e=a&&"object"==typeof a?r.extend({},a):{complete:c||!c&&b||r.isFunction(a)&&a,duration:a,easing:c&&b||b&&!r.isFunction(b)&&b};return r.fx.off||d.hidden?e.duration=0:e.duration="number"==typeof e.duration?e.duration:e.duration in r.fx.speeds?r.fx.speeds[e.duration]:r.fx.speeds._default,null!=e.queue&&e.queue!==!0||(e.queue="fx"),e.old=e.complete,e.complete=function(){r.isFunction(e.old)&&e.old.call(this),e.queue&&r.dequeue(this,e.queue)},e},r.fn.extend({fadeTo:function(a,b,c,d){return this.filter(ba).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=r.isEmptyObject(a),f=r.speed(b,c,d),g=function(){var b=gb(this,r.extend({},a),f);(e||V.get(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=r.timers,g=V.get(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&_a.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));!b&&c||r.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=V.get(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=r.timers,g=d?d.length:0;for(c.finish=!0,r.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),r.each(["toggle","show","hide"],function(a,b){var c=r.fn[b];r.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(cb(b,!0),a,d,e)}}),r.each({slideDown:cb("show"),slideUp:cb("hide"),slideToggle:cb("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){r.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),r.timers=[],r.fx.tick=function(){var a,b=0,c=r.timers;for(Ya=r.now();b<c.length;b++)a=c[b],a()||c[b]!==a||c.splice(b--,1);c.length||r.fx.stop(),Ya=void 0},r.fx.timer=function(a){r.timers.push(a),a()?r.fx.start():r.timers.pop()},r.fx.interval=13,r.fx.start=function(){Za||(Za=a.requestAnimationFrame?a.requestAnimationFrame(ab):a.setInterval(r.fx.tick,r.fx.interval))},r.fx.stop=function(){a.cancelAnimationFrame?a.cancelAnimationFrame(Za):a.clearInterval(Za),Za=null},r.fx.speeds={slow:600,fast:200,_default:400},r.fn.delay=function(b,c){return b=r.fx?r.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);d.stop=function(){a.clearTimeout(e)}})},function(){var a=d.createElement("input"),b=d.createElement("select"),c=b.appendChild(d.createElement("option"));a.type="checkbox",o.checkOn=""!==a.value,o.optSelected=c.selected,a=d.createElement("input"),a.value="t",a.type="radio",o.radioValue="t"===a.value}();var hb,ib=r.expr.attrHandle;r.fn.extend({attr:function(a,b){return S(this,r.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){r.removeAttr(this,a)})}}),r.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return"undefined"==typeof a.getAttribute?r.prop(a,b,c):(1===f&&r.isXMLDoc(a)||(e=r.attrHooks[b.toLowerCase()]||(r.expr.match.bool.test(b)?hb:void 0)),void 0!==c?null===c?void r.removeAttr(a,b):e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get"in e&&null!==(d=e.get(a,b))?d:(d=r.find.attr(a,b),null==d?void 0:d))},attrHooks:{type:{set:function(a,b){if(!o.radioValue&&"radio"===b&&r.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}},removeAttr:function(a,b){var c,d=0,e=b&&b.match(K);if(e&&1===a.nodeType)while(c=e[d++])a.removeAttribute(c);
}}),hb={set:function(a,b,c){return b===!1?r.removeAttr(a,c):a.setAttribute(c,c),c}},r.each(r.expr.match.bool.source.match(/\w+/g),function(a,b){var c=ib[b]||r.find.attr;ib[b]=function(a,b,d){var e,f,g=b.toLowerCase();return d||(f=ib[g],ib[g]=e,e=null!=c(a,b,d)?g:null,ib[g]=f),e}});var jb=/^(?:input|select|textarea|button)$/i,kb=/^(?:a|area)$/i;r.fn.extend({prop:function(a,b){return S(this,r.prop,a,b,arguments.length>1)},removeProp:function(a){return this.each(function(){delete this[r.propFix[a]||a]})}}),r.extend({prop:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return 1===f&&r.isXMLDoc(a)||(b=r.propFix[b]||b,e=r.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=r.find.attr(a,"tabindex");return b?parseInt(b,10):jb.test(a.nodeName)||kb.test(a.nodeName)&&a.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),o.optSelected||(r.propHooks.selected={get:function(a){var b=a.parentNode;return b&&b.parentNode&&b.parentNode.selectedIndex,null},set:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex)}}),r.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){r.propFix[this.toLowerCase()]=this});var lb=/[\t\r\n\f]/g;function mb(a){return a.getAttribute&&a.getAttribute("class")||""}r.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;if(r.isFunction(a))return this.each(function(b){r(this).addClass(a.call(this,b,mb(this)))});if("string"==typeof a&&a){b=a.match(K)||[];while(c=this[i++])if(e=mb(c),d=1===c.nodeType&&(" "+e+" ").replace(lb," ")){g=0;while(f=b[g++])d.indexOf(" "+f+" ")<0&&(d+=f+" ");h=r.trim(d),e!==h&&c.setAttribute("class",h)}}return this},removeClass:function(a){var b,c,d,e,f,g,h,i=0;if(r.isFunction(a))return this.each(function(b){r(this).removeClass(a.call(this,b,mb(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof a&&a){b=a.match(K)||[];while(c=this[i++])if(e=mb(c),d=1===c.nodeType&&(" "+e+" ").replace(lb," ")){g=0;while(f=b[g++])while(d.indexOf(" "+f+" ")>-1)d=d.replace(" "+f+" "," ");h=r.trim(d),e!==h&&c.setAttribute("class",h)}}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):r.isFunction(a)?this.each(function(c){r(this).toggleClass(a.call(this,c,mb(this),b),b)}):this.each(function(){var b,d,e,f;if("string"===c){d=0,e=r(this),f=a.match(K)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else void 0!==a&&"boolean"!==c||(b=mb(this),b&&V.set(this,"__className__",b),this.setAttribute&&this.setAttribute("class",b||a===!1?"":V.get(this,"__className__")||""))})},hasClass:function(a){var b,c,d=0;b=" "+a+" ";while(c=this[d++])if(1===c.nodeType&&(" "+mb(c)+" ").replace(lb," ").indexOf(b)>-1)return!0;return!1}});var nb=/\r/g,ob=/[\x20\t\r\n\f]+/g;r.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=r.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,r(this).val()):a,null==e?e="":"number"==typeof e?e+="":r.isArray(e)&&(e=r.map(e,function(a){return null==a?"":a+""})),b=r.valHooks[this.type]||r.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=r.valHooks[e.type]||r.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(nb,""):null==c?"":c)}}}),r.extend({valHooks:{option:{get:function(a){var b=r.find.attr(a,"value");return null!=b?b:r.trim(r.text(a)).replace(ob," ")}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],(c.selected||i===e)&&!c.disabled&&(!c.parentNode.disabled||!r.nodeName(c.parentNode,"optgroup"))){if(b=r(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=r.makeArray(b),g=e.length;while(g--)d=e[g],(d.selected=r.inArray(r.valHooks.option.get(d),f)>-1)&&(c=!0);return c||(a.selectedIndex=-1),f}}}}),r.each(["radio","checkbox"],function(){r.valHooks[this]={set:function(a,b){return r.isArray(b)?a.checked=r.inArray(r(a).val(),b)>-1:void 0}},o.checkOn||(r.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var pb=/^(?:focusinfocus|focusoutblur)$/;r.extend(r.event,{trigger:function(b,c,e,f){var g,h,i,j,k,m,n,o=[e||d],p=l.call(b,"type")?b.type:b,q=l.call(b,"namespace")?b.namespace.split("."):[];if(h=i=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!pb.test(p+r.event.triggered)&&(p.indexOf(".")>-1&&(q=p.split("."),p=q.shift(),q.sort()),k=p.indexOf(":")<0&&"on"+p,b=b[r.expando]?b:new r.Event(p,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=q.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+q.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:r.makeArray(c,[b]),n=r.event.special[p]||{},f||!n.trigger||n.trigger.apply(e,c)!==!1)){if(!f&&!n.noBubble&&!r.isWindow(e)){for(j=n.delegateType||p,pb.test(j+p)||(h=h.parentNode);h;h=h.parentNode)o.push(h),i=h;i===(e.ownerDocument||d)&&o.push(i.defaultView||i.parentWindow||a)}g=0;while((h=o[g++])&&!b.isPropagationStopped())b.type=g>1?j:n.bindType||p,m=(V.get(h,"events")||{})[b.type]&&V.get(h,"handle"),m&&m.apply(h,c),m=k&&h[k],m&&m.apply&&T(h)&&(b.result=m.apply(h,c),b.result===!1&&b.preventDefault());return b.type=p,f||b.isDefaultPrevented()||n._default&&n._default.apply(o.pop(),c)!==!1||!T(e)||k&&r.isFunction(e[p])&&!r.isWindow(e)&&(i=e[k],i&&(e[k]=null),r.event.triggered=p,e[p](),r.event.triggered=void 0,i&&(e[k]=i)),b.result}},simulate:function(a,b,c){var d=r.extend(new r.Event,c,{type:a,isSimulated:!0});r.event.trigger(d,null,b)}}),r.fn.extend({trigger:function(a,b){return this.each(function(){r.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?r.event.trigger(a,b,c,!0):void 0}}),r.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(a,b){r.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),r.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}}),o.focusin="onfocusin"in a,o.focusin||r.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){r.event.simulate(b,a.target,r.event.fix(a))};r.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=V.access(d,b);e||d.addEventListener(a,c,!0),V.access(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=V.access(d,b)-1;e?V.access(d,b,e):(d.removeEventListener(a,c,!0),V.remove(d,b))}}});var qb=a.location,rb=r.now(),sb=/\?/;r.parseXML=function(b){var c;if(!b||"string"!=typeof b)return null;try{c=(new a.DOMParser).parseFromString(b,"text/xml")}catch(d){c=void 0}return c&&!c.getElementsByTagName("parsererror").length||r.error("Invalid XML: "+b),c};var tb=/\[\]$/,ub=/\r?\n/g,vb=/^(?:submit|button|image|reset|file)$/i,wb=/^(?:input|select|textarea|keygen)/i;function xb(a,b,c,d){var e;if(r.isArray(b))r.each(b,function(b,e){c||tb.test(a)?d(a,e):xb(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)});else if(c||"object"!==r.type(b))d(a,b);else for(e in b)xb(a+"["+e+"]",b[e],c,d)}r.param=function(a,b){var c,d=[],e=function(a,b){var c=r.isFunction(b)?b():b;d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(null==c?"":c)};if(r.isArray(a)||a.jquery&&!r.isPlainObject(a))r.each(a,function(){e(this.name,this.value)});else for(c in a)xb(c,a[c],b,e);return d.join("&")},r.fn.extend({serialize:function(){return r.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=r.prop(this,"elements");return a?r.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!r(this).is(":disabled")&&wb.test(this.nodeName)&&!vb.test(a)&&(this.checked||!ha.test(a))}).map(function(a,b){var c=r(this).val();return null==c?null:r.isArray(c)?r.map(c,function(a){return{name:b.name,value:a.replace(ub,"\r\n")}}):{name:b.name,value:c.replace(ub,"\r\n")}}).get()}});var yb=/%20/g,zb=/#.*$/,Ab=/([?&])_=[^&]*/,Bb=/^(.*?):[ \t]*([^\r\n]*)$/gm,Cb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Db=/^(?:GET|HEAD)$/,Eb=/^\/\//,Fb={},Gb={},Hb="*/".concat("*"),Ib=d.createElement("a");Ib.href=qb.href;function Jb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(K)||[];if(r.isFunction(c))while(d=f[e++])"+"===d[0]?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Kb(a,b,c,d){var e={},f=a===Gb;function g(h){var i;return e[h]=!0,r.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Lb(a,b){var c,d,e=r.ajaxSettings.flatOptions||{};for(c in b)void 0!==b[c]&&((e[c]?a:d||(d={}))[c]=b[c]);return d&&r.extend(!0,a,d),a}function Mb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===d&&(d=a.mimeType||b.getResponseHeader("Content-Type"));if(d)for(e in h)if(h[e]&&h[e].test(d)){i.unshift(e);break}if(i[0]in c)f=i[0];else{for(e in c){if(!i[0]||a.converters[e+" "+i[0]]){f=e;break}g||(g=e)}f=f||g}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Nb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}r.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:qb.href,type:"GET",isLocal:Cb.test(qb.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Hb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":r.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Lb(Lb(a,r.ajaxSettings),b):Lb(r.ajaxSettings,a)},ajaxPrefilter:Jb(Fb),ajaxTransport:Jb(Gb),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};var e,f,g,h,i,j,k,l,m,n,o=r.ajaxSetup({},c),p=o.context||o,q=o.context&&(p.nodeType||p.jquery)?r(p):r.event,s=r.Deferred(),t=r.Callbacks("once memory"),u=o.statusCode||{},v={},w={},x="canceled",y={readyState:0,getResponseHeader:function(a){var b;if(k){if(!h){h={};while(b=Bb.exec(g))h[b[1].toLowerCase()]=b[2]}b=h[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return k?g:null},setRequestHeader:function(a,b){return null==k&&(a=w[a.toLowerCase()]=w[a.toLowerCase()]||a,v[a]=b),this},overrideMimeType:function(a){return null==k&&(o.mimeType=a),this},statusCode:function(a){var b;if(a)if(k)y.always(a[y.status]);else for(b in a)u[b]=[u[b],a[b]];return this},abort:function(a){var b=a||x;return e&&e.abort(b),A(0,b),this}};if(s.promise(y),o.url=((b||o.url||qb.href)+"").replace(Eb,qb.protocol+"//"),o.type=c.method||c.type||o.method||o.type,o.dataTypes=(o.dataType||"*").toLowerCase().match(K)||[""],null==o.crossDomain){j=d.createElement("a");try{j.href=o.url,j.href=j.href,o.crossDomain=Ib.protocol+"//"+Ib.host!=j.protocol+"//"+j.host}catch(z){o.crossDomain=!0}}if(o.data&&o.processData&&"string"!=typeof o.data&&(o.data=r.param(o.data,o.traditional)),Kb(Fb,o,c,y),k)return y;l=r.event&&o.global,l&&0===r.active++&&r.event.trigger("ajaxStart"),o.type=o.type.toUpperCase(),o.hasContent=!Db.test(o.type),f=o.url.replace(zb,""),o.hasContent?o.data&&o.processData&&0===(o.contentType||"").indexOf("application/x-www-form-urlencoded")&&(o.data=o.data.replace(yb,"+")):(n=o.url.slice(f.length),o.data&&(f+=(sb.test(f)?"&":"?")+o.data,delete o.data),o.cache===!1&&(f=f.replace(Ab,""),n=(sb.test(f)?"&":"?")+"_="+rb++ +n),o.url=f+n),o.ifModified&&(r.lastModified[f]&&y.setRequestHeader("If-Modified-Since",r.lastModified[f]),r.etag[f]&&y.setRequestHeader("If-None-Match",r.etag[f])),(o.data&&o.hasContent&&o.contentType!==!1||c.contentType)&&y.setRequestHeader("Content-Type",o.contentType),y.setRequestHeader("Accept",o.dataTypes[0]&&o.accepts[o.dataTypes[0]]?o.accepts[o.dataTypes[0]]+("*"!==o.dataTypes[0]?", "+Hb+"; q=0.01":""):o.accepts["*"]);for(m in o.headers)y.setRequestHeader(m,o.headers[m]);if(o.beforeSend&&(o.beforeSend.call(p,y,o)===!1||k))return y.abort();if(x="abort",t.add(o.complete),y.done(o.success),y.fail(o.error),e=Kb(Gb,o,c,y)){if(y.readyState=1,l&&q.trigger("ajaxSend",[y,o]),k)return y;o.async&&o.timeout>0&&(i=a.setTimeout(function(){y.abort("timeout")},o.timeout));try{k=!1,e.send(v,A)}catch(z){if(k)throw z;A(-1,z)}}else A(-1,"No Transport");function A(b,c,d,h){var j,m,n,v,w,x=c;k||(k=!0,i&&a.clearTimeout(i),e=void 0,g=h||"",y.readyState=b>0?4:0,j=b>=200&&300>b||304===b,d&&(v=Mb(o,y,d)),v=Nb(o,v,y,j),j?(o.ifModified&&(w=y.getResponseHeader("Last-Modified"),w&&(r.lastModified[f]=w),w=y.getResponseHeader("etag"),w&&(r.etag[f]=w)),204===b||"HEAD"===o.type?x="nocontent":304===b?x="notmodified":(x=v.state,m=v.data,n=v.error,j=!n)):(n=x,!b&&x||(x="error",0>b&&(b=0))),y.status=b,y.statusText=(c||x)+"",j?s.resolveWith(p,[m,x,y]):s.rejectWith(p,[y,x,n]),y.statusCode(u),u=void 0,l&&q.trigger(j?"ajaxSuccess":"ajaxError",[y,o,j?m:n]),t.fireWith(p,[y,x]),l&&(q.trigger("ajaxComplete",[y,o]),--r.active||r.event.trigger("ajaxStop")))}return y},getJSON:function(a,b,c){return r.get(a,b,c,"json")},getScript:function(a,b){return r.get(a,void 0,b,"script")}}),r.each(["get","post"],function(a,b){r[b]=function(a,c,d,e){return r.isFunction(c)&&(e=e||d,d=c,c=void 0),r.ajax(r.extend({url:a,type:b,dataType:e,data:c,success:d},r.isPlainObject(a)&&a))}}),r._evalUrl=function(a){return r.ajax({url:a,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})},r.fn.extend({wrapAll:function(a){var b;return this[0]&&(r.isFunction(a)&&(a=a.call(this[0])),b=r(a,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstElementChild)a=a.firstElementChild;return a}).append(this)),this},wrapInner:function(a){return r.isFunction(a)?this.each(function(b){r(this).wrapInner(a.call(this,b))}):this.each(function(){var b=r(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=r.isFunction(a);return this.each(function(c){r(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(a){return this.parent(a).not("body").each(function(){r(this).replaceWith(this.childNodes)}),this}}),r.expr.pseudos.hidden=function(a){return!r.expr.pseudos.visible(a)},r.expr.pseudos.visible=function(a){return!!(a.offsetWidth||a.offsetHeight||a.getClientRects().length)},r.ajaxSettings.xhr=function(){try{return new a.XMLHttpRequest}catch(b){}};var Ob={0:200,1223:204},Pb=r.ajaxSettings.xhr();o.cors=!!Pb&&"withCredentials"in Pb,o.ajax=Pb=!!Pb,r.ajaxTransport(function(b){var c,d;return o.cors||Pb&&!b.crossDomain?{send:function(e,f){var g,h=b.xhr();if(h.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields)for(g in b.xhrFields)h[g]=b.xhrFields[g];b.mimeType&&h.overrideMimeType&&h.overrideMimeType(b.mimeType),b.crossDomain||e["X-Requested-With"]||(e["X-Requested-With"]="XMLHttpRequest");for(g in e)h.setRequestHeader(g,e[g]);c=function(a){return function(){c&&(c=d=h.onload=h.onerror=h.onabort=h.onreadystatechange=null,"abort"===a?h.abort():"error"===a?"number"!=typeof h.status?f(0,"error"):f(h.status,h.statusText):f(Ob[h.status]||h.status,h.statusText,"text"!==(h.responseType||"text")||"string"!=typeof h.responseText?{binary:h.response}:{text:h.responseText},h.getAllResponseHeaders()))}},h.onload=c(),d=h.onerror=c("error"),void 0!==h.onabort?h.onabort=d:h.onreadystatechange=function(){4===h.readyState&&a.setTimeout(function(){c&&d()})},c=c("abort");try{h.send(b.hasContent&&b.data||null)}catch(i){if(c)throw i}},abort:function(){c&&c()}}:void 0}),r.ajaxPrefilter(function(a){a.crossDomain&&(a.contents.script=!1)}),r.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return r.globalEval(a),a}}}),r.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET")}),r.ajaxTransport("script",function(a){if(a.crossDomain){var b,c;return{send:function(e,f){b=r("<script>").prop({charset:a.scriptCharset,src:a.url}).on("load error",c=function(a){b.remove(),c=null,a&&f("error"===a.type?404:200,a.type)}),d.head.appendChild(b[0])},abort:function(){c&&c()}}}});var Qb=[],Rb=/(=)\?(?=&|$)|\?\?/;r.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=Qb.pop()||r.expando+"_"+rb++;return this[a]=!0,a}}),r.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(Rb.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&Rb.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=r.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(Rb,"$1"+e):b.jsonp!==!1&&(b.url+=(sb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||r.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){void 0===f?r(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,Qb.push(e)),g&&r.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),o.createHTMLDocument=function(){var a=d.implementation.createHTMLDocument("").body;return a.innerHTML="<form></form><form></form>",2===a.childNodes.length}(),r.parseHTML=function(a,b,c){if("string"!=typeof a)return[];"boolean"==typeof b&&(c=b,b=!1);var e,f,g;return b||(o.createHTMLDocument?(b=d.implementation.createHTMLDocument(""),e=b.createElement("base"),e.href=d.location.href,b.head.appendChild(e)):b=d),f=B.exec(a),g=!c&&[],f?[b.createElement(f[1])]:(f=oa([a],b,g),g&&g.length&&r(g).remove(),r.merge([],f.childNodes))},r.fn.load=function(a,b,c){var d,e,f,g=this,h=a.indexOf(" ");return h>-1&&(d=r.trim(a.slice(h)),a=a.slice(0,h)),r.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&r.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?r("<div>").append(r.parseHTML(a)).find(d):a)}).always(c&&function(a,b){g.each(function(){c.apply(this,f||[a.responseText,b,a])})}),this},r.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){r.fn[b]=function(a){return this.on(b,a)}}),r.expr.pseudos.animated=function(a){return r.grep(r.timers,function(b){return a===b.elem}).length};function Sb(a){return r.isWindow(a)?a:9===a.nodeType&&a.defaultView}r.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=r.css(a,"position"),l=r(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=r.css(a,"top"),i=r.css(a,"left"),j=("absolute"===k||"fixed"===k)&&(f+i).indexOf("auto")>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),r.isFunction(b)&&(b=b.call(a,c,r.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},r.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){r.offset.setOffset(this,a,b)});var b,c,d,e,f=this[0];if(f)return f.getClientRects().length?(d=f.getBoundingClientRect(),d.width||d.height?(e=f.ownerDocument,c=Sb(e),b=e.documentElement,{top:d.top+c.pageYOffset-b.clientTop,left:d.left+c.pageXOffset-b.clientLeft}):d):{top:0,left:0}},position:function(){if(this[0]){var a,b,c=this[0],d={top:0,left:0};return"fixed"===r.css(c,"position")?b=c.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),r.nodeName(a[0],"html")||(d=a.offset()),d={top:d.top+r.css(a[0],"borderTopWidth",!0),left:d.left+r.css(a[0],"borderLeftWidth",!0)}),{top:b.top-d.top-r.css(c,"marginTop",!0),left:b.left-d.left-r.css(c,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;while(a&&"static"===r.css(a,"position"))a=a.offsetParent;return a||pa})}}),r.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c="pageYOffset"===b;r.fn[a]=function(d){return S(this,function(a,d,e){var f=Sb(a);return void 0===e?f?f[b]:a[d]:void(f?f.scrollTo(c?f.pageXOffset:e,c?e:f.pageYOffset):a[d]=e)},a,d,arguments.length)}}),r.each(["top","left"],function(a,b){r.cssHooks[b]=Na(o.pixelPosition,function(a,c){return c?(c=Ma(a,b),Ka.test(c)?r(a).position()[b]+"px":c):void 0})}),r.each({Height:"height",Width:"width"},function(a,b){r.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){r.fn[d]=function(e,f){var g=arguments.length&&(c||"boolean"!=typeof e),h=c||(e===!0||f===!0?"margin":"border");return S(this,function(b,c,e){var f;return r.isWindow(b)?0===d.indexOf("outer")?b["inner"+a]:b.document.documentElement["client"+a]:9===b.nodeType?(f=b.documentElement,Math.max(b.body["scroll"+a],f["scroll"+a],b.body["offset"+a],f["offset"+a],f["client"+a])):void 0===e?r.css(b,c,h):r.style(b,c,e,h)},b,g?e:void 0,g)}})}),r.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}}),r.parseJSON=JSON.parse,"function"==typeof define&&define.amd&&define("jquery",[],function(){return r});var Tb=a.jQuery,Ub=a.$;return r.noConflict=function(b){return a.$===r&&(a.$=Ub),b&&a.jQuery===r&&(a.jQuery=Tb),r},b||(a.jQuery=a.$=r),r});
app.controller('MainController', function ($scope,
                                           $rootScope,
                                           $http,
                                           $timeout,
                                           $ionicLoading,
                                           $ionicPopup,
                                           $ionicLoading,
                                           $ionicTabsDelegate,
                                           $state,
                                           $ionicModal,
                                           $ionicSideMenuDelegate,
                                           $ionicHistory,
                                           $sce,
                                           $filter,
                                           $cordovaContacts,
                                           $ionicSlideBoxDelegate,
                                           $log,
                                           $window,
                                           CallService,
                                           PanicService,
                                           UserService) {

    window.http = $http;
    window.CallService = CallService;
    window.PanicService = PanicService;
    window.UserService = UserService;

    /**
     var req = {
 method: 'POST',
 url: 'https://rest.nexmo.com/call/json',
 headers: {
   'Content-Type': 'application/x-www-form-urlencoded'
 },
 data: {
        "api_secret": "f17108ba7fbb1227",
        "to": "5511960540130",
        "api_key": "0f398013",
        "answer_url": "http://vihu.monstro.io/ura.xml",
        "from": "551149331221",
  }
}

     http(req).then(function(){console.log(arguments)}, function(){console.error(arguments)});
     */
    // $log.debug("$window.device", $window.device);
    // $log.debug("$window.device as Boolean", $window.device ? true : false);
    // $log.debug("ionic.Platform.device()", ionic.Platform.device());
    // $log.debug("ionic.Platform.device() === {}", ionic.Platform.device() === {});


    window._scopeMain = $scope;

    $scope.accessOptions = [
        {name: '1', qtd: '1'},
        {name: '2', qtd: '2'},
        {name: '3', qtd: '3'},
        {name: '4', qtd: '4'},
        {name: '5', qtd: '5'},
    ];

    // $scope.onlyExiting = false;
    // $scope.onlyExiting = { checked: true };

    //
    $scope.$watch($scope.onlyExiting, function () {
        console.log(arguments);
    })

    $scope.acessos = {qtd: $scope.accessOptions[0].qtd};


    var showChoosePhonePopup = function (contact) {

        $scope.data = {};

        $scope.data.phones = $scope.contatoSelecionado.numeros;

        var myPopup = $ionicPopup.show({
            templateUrl: 'templates/choose-phone.html',
            title: 'Selecione o número de telefone',
            subTitle: 'Este contato tem mais de um número associado a ele',
            scope: $scope,
            buttons: [
                {text: 'Cancelar'},
                {
                    text: '<b>Escolher</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        if (!$scope.data.phoneSelected) {
                            $scope.invalidPhone = true;
                            console.error('if, $scope.data =>', $scope.data)
                            e.preventDefault();
                        } else {
                            $scope.invalidPhone = false;
                            console.error('else, $scope.data =>', $scope.data)
                            return $rootScope.accessVisitorsSelected.phone = $scope.data.phone;
                        }
                    }
                }
            ]
        });


        myPopup.then(function () {
            // $scope.data.phone.replace(/[^\d]/g,'');
        });
    }

    $scope.selecionarContato = function () {
        $cordovaContacts.pickContact().then(function (contato) {
            console.log(contato);

            if (typeof contato.phoneNumbers === "undefined" || contato.phoneNumbers == null || contato.phoneNumbers == "null" || !Array.isArray(contato.phoneNumbers)) {
                $ionicPopup.alert({
                    title: 'Contato Inválido',
                    template: 'Não há nenhum número de celular associado a esse contato. Por favor, selecione outro.'
                });
                return;
            }

            $scope.contatoSelecionado = contato;
            $rootScope.accessVisitorsSelected.name = contato.name.formatted;

            if (contato.phoneNumbers.length > 1) {

                $scope.contatoSelecionado.numeros = [];

                angular.forEach(contato.phoneNumbers, function (v, k) {
                    $scope.contatoSelecionado.numeros.push(contato.phoneNumbers[k].value);

                    console.log(contato.phoneNumbers[k].value);
                })

                showChoosePhonePopup(contato);

                return;
            } else {

                $scope.contatoSelecionado = contato;
                $rootScope.accessVisitorsSelected.name = contato.name.formatted;

                console.error("O BAGULHO FICOU LOUCO!!!!!", contato)
                if (typeof $scope.data == "undefined") $scope.data = {};
                $scope.data.phoneSelected = $rootScope.accessVisitorsSelected.phone = $scope.contatoSelecionado.phoneNumbers[0].value

                console.log($scope.data.phoneSelected, $rootScope.accessVisitorsSelected.phone, $scope.contatoSelecionado.phoneNumbers[0].value);

            }

            // console.log($scope.contatoSelecionado.photos);

            if ($scope.contatoSelecionado.photos !== null) {
                if ($scope.contatoSelecionado.photos instanceof Array) {
                    $scope.contatoSelecionado.foto = $scope.contatoSelecionado.photos[0].value;
                }
            } else {
                $scope.contatoSelecionado.foto = 'img/user.jpg';
            }
        });
    }

    window.scope = $scope;

    $scope.logged = false;
    $scope.password;
    $scope.activeMenu = 0;
    $scope.setInvalidAmount = false;
    $scope.invalidPhone;

    $scope.validateAmount = function (qtd) {
        if (qtd < 1 || qtd > 5) {
            $scope.setInvalidAmount = true;
            $scope.setInvalidQtd = true;
        } else {
            $scope.setInvalidAmount = false;
            $scope.setInvalidQtd = false;
        }
    }

// CHECK LOGIN
    $scope.checkLogin = function () {
        var user = JSON.parse(localStorage.getItem('user'));

        //var user = {
        //    status: "SUCCESS",
        //    data: {
        //        CELULAR_ZEL: "11947929861",
        //        CONDOMINIO: "Mundial Ecológica",
        //        EMAIL_ZEL: "eduardo@mundialecologica.com.br",
        //        ID_CONDOMINIO: "2",
        //        ID_TIPO_USUARIO: "1",
        //        NOME_ZEL: "Eduardo",
        //        TORRE_APTO: "Bloco A - 323",
        //        USER_CEL: "11982616336",
        //        USER_EMAIL: "alessandro@monstroestudio.com.br",
        //        USER_ID: "16",
        //        USER_NAME: "Alessandro Luz"
        //    }
        //};

        //localStorage.setItem('user', JSON.stringify(user));

        //document.location.href = '#/festas';

        if (user == null) {
            $scope.logged = false;
            console.log('deslogado');
            document.location.href = '#/login';
        } else {
            $scope.logged = true;
            document.location.href = '#/home';
            console.log('logado');
            console.log(user);
        }
    }
// LOGOUT
    $scope.sair = function () {
        localStorage.removeItem('user');
        $scope.checkLogin();
    }

// MENU
    $scope.toggleLeftSideMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
    };
    $scope.callChangePass = function () {
        $state.go('change');
    }
    $scope.callTab = function (index) {
        $ionicTabsDelegate.select(index);
    }

// MODAL FORGOT PASS
    $ionicModal.fromTemplateUrl('templates/forgot-pass.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modalForgot) {
        $scope.modalForgot = modalForgot;
    });
    $scope.openModalForgot = function () {
        $scope.modalForgot.show();
    };
    $scope.closeModalForgot = function () {
        $scope.modalForgot.hide();
    };

// MODAL CHANGE PASS
    $ionicModal.fromTemplateUrl('templates/change-pass.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modalChange) {
        $scope.modalChange = modalChange;
    });
    $scope.openModalChange = function () {
        $scope.modalChange.show();
    };
    $scope.closeModalChange = function () {
        $scope.modalChange.hide();
    };

// MODAL SETTINGS
    $ionicModal.fromTemplateUrl('templates/settings.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modalSettings) {
        $scope.modalSettings = modalSettings;
    });
    $scope.openModalSettings = function () {
        $scope.modalSettings.show();
        $scope.loadSettings();
    };
    $scope.closeModalSettings = function () {
        $scope.modalSettings.hide();
    };


// DATE PICKER
    $scope.datepickerObject = {};
    $scope.datepickerObject.inputDate = new Date();

    $scope.datepickerStart = {};
    $scope.datepickerStart.inputDate = new Date();

    $scope.datepickerEnd = {};
    $scope.datepickerEnd.inputDate = new Date();

    $scope.datepickerParty = {};
    $scope.datepickerParty.inputDate = new Date();

    var monthList = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
    var weekDaysList = ["D", "S", "T", "Q", "Q", "S", "S"];

    $scope.datepickerRelEnd = {
        titleLabel: 'Data Final',
        todayLabel: 'Hoje',
        closeLabel: 'Fechar',
        setLabel: 'Ok',
        setButtonType: 'button-assertive',
        todayButtonType: 'button-default',
        closeButtonType: 'button-default',
        inputDate: $scope.datepickerEnd.inputDate,
        mondayFirst: true,
        weekDaysList: weekDaysList,
        monthList: monthList,
        templateType: 'popup',
        showTodayButton: 'true',
        modalHeaderColor: 'bar-positive',
        modalFooterColor: 'bar-positive',
        from: new Date(2015, 10, 1),
        callback: function (val) {  //Mandatory
            datePickerFinal(val);
        },
        dateFormat: 'dd-MM-yyyy',
        closeOnSelect: false,
    };
    $scope.datepickerRelStart = {
        titleLabel: 'Data Inicial',
        todayLabel: 'Hoje',
        closeLabel: 'Fechar',
        setLabel: 'Ok',
        setButtonType: 'button-assertive',
        todayButtonType: 'button-default',
        closeButtonType: 'button-default',
        inputDate: $scope.datepickerStart.inputDate,
        mondayFirst: true,
        weekDaysList: weekDaysList,
        monthList: monthList,
        templateType: 'popup',
        showTodayButton: 'true',
        modalHeaderColor: 'bar-positive',
        modalFooterColor: 'bar-positive',
        from: new Date(2015, 10, 1),
        callback: function (val) {  //Mandatory
            datePickerInitial(val);
        },
        dateFormat: 'dd-MM-yyyy',
        closeOnSelect: false,
    };
    $scope.datepickerObjectPopup = {
        titleLabel: 'Data de Acesso',
        todayLabel: 'Hoje',
        closeLabel: 'Fechar',
        setLabel: 'Ok',
        setButtonType: 'button-assertive',
        todayButtonType: 'button-default',
        closeButtonType: 'button-default',
        inputDate: $scope.datepickerObject.inputDate,
        mondayFirst: true,
        weekDaysList: weekDaysList,
        monthList: monthList,
        templateType: 'popup',
        showTodayButton: 'true',
        modalHeaderColor: 'bar-positive',
        modalFooterColor: 'bar-positive',
        from: new Date(2015, 10, 1),
        callback: function (val) {  //Mandatory
            datePickerCallbackPopup(val);
        },
        dateFormat: 'dd-MM-yyyy',
        closeOnSelect: false,
    };
    $scope.datepickerFesta = {
        titleLabel: 'Data da Festa',
        todayLabel: 'Hoje',
        closeLabel: 'Fechar',
        setLabel: 'Ok',
        setButtonType: 'button-assertive',
        todayButtonType: 'button-default',
        closeButtonType: 'button-default',
        inputDate: $scope.datepickerParty.inputDate,
        mondayFirst: true,
        weekDaysList: weekDaysList,
        monthList: monthList,
        templateType: 'popup',
        showTodayButton: 'true',
        modalHeaderColor: 'bar-positive',
        modalFooterColor: 'bar-positive',
        from: new Date(2015, 10, 1),
        callback: function (val) {  //Mandatory
            datePickerParty(val);
        },
        dateFormat: 'dd-MM-yyyy',
        closeOnSelect: false,
    };

    var datePickerParty = function (val) {
        if (typeof(val) === 'undefined') {
            console.log('No date selected');
        } else {
            $scope.datepickerParty.inputDate = val;
        }
    };
    var datePickerCallbackPopup = function (val) {
        if (typeof(val) === 'undefined') {
            console.log('No date selected');
        } else {
            $scope.datepickerObject.inputDate = val;
        }
    };
    var datePickerInitial = function (val) {
        if (typeof(val) === 'undefined') {
            console.log('No date selected');
        } else {
            $scope.datepickerStart.inputDate = val;
        }
    };
    var datePickerFinal = function (val) {
        if (typeof(val) === 'undefined') {
            console.log('No date selected');
        } else {
            $scope.datepickerEnd.inputDate = val;
        }
    };

    $scope.showLoading = function () {
        $ionicLoading.show({
            template: '<span class="loading-puff modal-anim"><img src="img/puff.svg" alt=""></span>'
        });
    };
    $scope.hideLoading = function () {
        $ionicLoading.hide();
    };


    $scope.mainPanics = [];
    $scope._cards = {
        phone: "",
        "panictypes": [{
            label: "Chamar Supervisor",
            val: false
        }, {
            label: "Assalto em andamento",
            val: false
        }, {
            label: "Problema de saúde",
            val: false
        }]
    };

    angular.forEach($scope._cards, function (valor, index) {
        angular.forEach(valor, function (v, i) {
            $scope.mainPanics.push(v.label);
        });
    });

    $scope.cards = [{
        phone: "",
        "panictypes": [{
            label: "Chamar Supervisor",
            val: false
        }, {
            label: "Assalto em andamento",
            val: false
        }, {
            label: "Problema de saúde",
            val: false
        }]
    }];

    $scope.noPanicType = true;
    $scope.invalidForm = true;
    $scope.addCard = function () {
        $scope.cards.push(angular.copy($scope._cards));
        $scope.noPanicType = true;
    }

    $scope.savePanic = function (phoneValid, formInvalid) {
        console.log(formInvalid);

        if (formInvalid == false) {
            $scope.invalidForm = false;
        } else {
            $scope.invalidForm = true;
        }

        if (typeof phoneValid !== 'undefined') {

            //
            //$timeout(function () {
            //    // console.log(phoneValid);
            $scope.sendSettings();
            //}, 2000);
        }
    }


    $scope.panicOpt = function () {
        angular.forEach($scope.cards, function (key, value) {

            $scope.sendSettings();

            var trues = $filter("filter")(key.panictypes, {val: true});
            console.log(trues.length);

            if (trues.length >= 1) {
                $scope.noPanicType = false;
            } else {
                $scope.noPanicType = true;
            }
        });
        // $scope.sendSettings();
    }

    $rootScope.panicWarning = $scope.panicWarning = function () {
        var alertPopup = $ionicPopup.alert({
            title: 'Você precisa adicionar números para <b>' + missingLabel + '</b> no botão de pânico',
            cssClass: 'popStyle'
        });
        alertPopup.then(function (res) {
            $scope.openModalSettings();
        });
    }

    var savedPanics = [];
    if (JSON.parse(localStorage.getItem('panic')) == null) {
        localStorage.setItem('panic', JSON.stringify(savedPanics));
    }

    var trueAmount = [];
    var trueLabels = [];
    var onlyLabels = [];

    $scope.panicPopup;
    $scope.totaltrues;
    $scope.eachTrueLabel;
    $scope.cards = JSON.parse(localStorage.getItem('panic'));

    angular.forEach($scope.cards, function (value, key) {
        var eachTrue = $filter("filter")(value.panictypes, {val: true});

        trueLabels.push(eachTrue);
        trueAmount.push(eachTrue.length);

        $scope.totaltrues = trueAmount.reduce(function (a, b) {
            return a + b
        });
    });

    angular.forEach(trueLabels, function (v, k) {
        console.warn('---------------', v);
        angular.forEach(v, function (vl, ky) {
            onlyLabels.push(vl.label);
        });
    });

    var missingPanicArr = _.difference($scope.mainPanics, onlyLabels);
    var missingLabel = _.toString(missingPanicArr);

    missingLabel.replace(',', ', ');

    console.log(missingLabel);

    if ($scope.totaltrues < 3) {
        $scope.panicWarning();
    }


    /**
     * Define um listener para mudanças nos cards serem salvas imediatamente no localStorage
     */
    $scope.$watch(function (scope) {
        return scope.cards;
    }, function (a) {
        console.warn('Assistindo a mudanças no $scope.cards', a)
        //localStorage.setItem('panic', JSON.stringify($scope.cards));
        $scope.sendSettings();
    }, true);


    $scope.sendSettings = function () {
        if ($scope.invalidForm == false && $scope.noPanicType == false) {
            console.log($scope.cards);

            localStorage.setItem('panic', JSON.stringify($scope.cards));
        }

        // $http({
        //     url: domain + '/user/addPhones',
        //     method: 'POST',
        //     data: {
        //         cards: $scope.cards,
        //     }
        // }).then(function (data) {
        //     var data = data.data;

        //
        //     // console.log(data);
        // }, function (response) {
        //     $ionicPopup.alert({
        //         title: 'Ocorreu um erro, tente novamente mais tarde.',
        //         cssClass: 'popStyle'
        //     });
        // })
    }


    window.$filter = $filter;

    $scope.loadSettings = function () {
        $scope.cards = $filter('cards')(JSON.parse(localStorage.getItem('panic')));
        $scope.invalidForm = false;
        $scope.noPanicType = false;

        angular.forEach($scope.cards, function (value, key) {
            console.log(value);
        })
    }


    $scope.removeCard = function (card) {

        if (card.phone == "") return;
        if (card.phone.length < 10) return;
        var filtered = $filter('filter')(card.panictypes, {val: true});

        if (filtered.length == 0) {
            return;
        }


        console.log('remove card', card)

        $scope.cards = JSON.parse(localStorage.getItem('panic'));
        angular.forEach($scope.cards, function (i, v) {
            console.log(i.phone + i.panictypes == card.phone + card.panictypes);
            if (i.phone + i.panictypes == card.phone + card.panictypes) {
                $scope.cards.splice(v, 1);
            }
        });
        localStorage.setItem('panic', JSON.stringify($scope.cards));
    }

    $scope.panicPress = function () {

        var panic = JSON.parse(localStorage.getItem('panic'));

        /**

         @todo
         **/


        if (panic.length == 0) {
            $rootScope.panicWarning();
            return;
        }


        $rootScope.panicPopup = $ionicPopup.show({
            templateUrl: 'templates/choose-panic.html',
            scope: $scope,
            cssClass: 'panicPopStyle'
        });
    }

// LOGIN FORM
    $scope.processForm = function () {
        var username = this.loginForm.user;
        $scope.password = this.loginForm.password;

        $scope.isSaving = true;

        $http({
            url: domain + '/login',
            method: 'POST',
            data: {
                login: username,
                pwd: $scope.password
            }
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                localStorage.setItem('user', JSON.stringify(data));
                $scope.logged = true;
                $scope.checkLogin();
                $scope.isSaving = false;
            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.isSaving = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.isSaving = false;
        })
    }

// CONCIERGE FORM - ZELADORIA
    $scope.requestConcierge = function () {
        var user = JSON.parse(localStorage.getItem('user'));

        $scope.savingConcierge = true;

        $http({
            url: domain + '/concierge',
            method: 'POST',
            data: {
                userObj: user,
                uID: user.data.USER_ID,
                uName: user.data.USER_NAME,
                uEmail: user.data.USER_EMAIL,
                uMobile: user.data.USER_CEL,
                cName: user.data.NOME_ZEL,
                cEmail: user.data.EMAIL_ZEL,
                cMobile: user.data.CELULAR_ZEL,
                condominium: user.data.ID_CONDOMINIO
            }
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {

                // senha solicitada
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingConcierge = false;
            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingConcierge = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingConcierge = false;
        })
    };

// ACESSOS FORM
    $scope.allowForm = function (isValid) {
        var thisForm = this;

        console.groupCollapsed('$scope.allowForm ==> this');
        console.log('$scope.allowForm ==> this', this)
        console.warn(' this.acessosForm', this.acessosForm)
        console.warn(' this.onlyExiting', this.onlyExiting)
        console.groupEnd();


        var user = JSON.parse(localStorage.getItem('user'));

        var allowDateVal = $scope.datepickerObject.inputDate
        var horarioVal = this.acessos.hora;

        var allowDate = moment(allowDateVal).format('YYYY/MM/DD');
        var horario = moment(horarioVal).format('HH:mm');
        var phone = $scope.data.phoneSelected;
        var qtdade = this.acessos.qtd;

        console.log(phone);

        var platform = platform || 'platform';
        var device = device || 'device';

        $scope.savingAcess = true;


        if (platform.indexOf("iPhone") == 0) {
            platform = 'iPhone';
        } else {
            platform = 'Android';
        }

        $scope.reset = function () {

            thisForm.acessos.hora = "";
            thisForm.acessos.phone = "";

            thisForm.onlyExiting = false;


            $scope.accessOptions = [
                {name: '1', qtd: '1'},
                {name: '2', qtd: '2'},
                {name: '3', qtd: '3'},
                {name: '4', qtd: '4'},
                {name: '5', qtd: '5'},
            ];

            $scope.acessos = {qtd: $scope.accessOptions[0].qtd};

            $scope.contatoSelecionado = '';
            $rootScope.contatoSelecionado = '';

        }

        $scope.allowAcessosForm = {
            userObj: user,
            user: user.data.USER_ID,
            dataIni: allowDate,
            horaIni: horario,
            qtde: qtdade,
            celular: $filter('telPost')($scope.data.phoneSelected),
            tipo: 'A',
            plataforma: platform,
            device: device,
            name: $rootScope.accessVisitorsSelected.name,
            onlyExiting: this.onlyExiting
        }


        $http({
            url: domain + '/access',
            method: 'POST',
            data: $scope.allowAcessosForm
        }).then(function (data) {

            console.warn('HTTP enviando esses dados:', $scope.allowAcessosForm)


            var data = data.data;

            if (data.status == 'SUCCESS') {
                // acesso liberado
                var alertPopup = $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                alertPopup.then(function (res) {
                    $scope.reset();
                });
                $scope.savingAcess = false;

            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingAcess = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingAcess = false;
        })
    };

// RELATORIOS FORM
    $scope.relatorioForm = {};
    $scope.gerarRelatorio = function () {
        var user = JSON.parse(localStorage.getItem('user'));

        $scope.relatorioForm = {
            user: user.data.USER_ID,
            nome: user.data.USER_NAME,
            email: user.data.USER_EMAIL,
            celular: user.data.USER_CEL,
            dataIni: moment($scope.datepickerStart.inputDate).format('YYYY/MM/DD'),
            dataFim: moment($scope.datepickerEnd.inputDate).format('YYYY/MM/DD')
        };

        $scope.savingReport = true;

        $http({
            url: domain + '/reports',
            method: 'POST',
            data: $scope.relatorioForm
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                // sucesso
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingReport = false;
            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingReport = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingReport = false;
        })
    }

// CHANGE PASS FORM
    $scope.changeForm = {};
    $scope.changePassForm = function () {
        var user = JSON.parse(localStorage.getItem('user'));

        $scope.changeForm = {
            login: user.data.USER_CEL,
            pwd: this.changeForm.pass,
            newPwd: this.changeForm.passNew
        };
        $scope.savingPass = true;

        $http.post(
            domain + '/changepwd',
            $scope.changeForm
        ).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                var alertPopup = $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                alertPopup.then(function (res) {
                    $scope.closeModalChange();
                });
                $scope.savingPass = false;

                document.getElementById('currentPass').value = '';
                document.getElementById('newPass').value = '';
                document.getElementById('confirmPass').value = '';

            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingPass = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingPass = false;
        });
    }

// RECOVER PASS FORM
    $scope.forgotForm = {};

    $scope.recoverForm = function () {

        $scope.forgotForm = {
            email: this.forgotForm.passEmail
        };
        $scope.sendingPass = true;

        $http({
            url: domain + '/forgot',
            method: 'POST',
            data: $scope.forgotForm
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                var alertPopup = $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                alertPopup.then(function (res) {
                    $scope.closeModalForgot();
                });
                $scope.sendingPass = false;

                document.getElementById('emailForgot').value = '';

            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.sendingPass = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.sendingPass = false;
        });
    }
})

app.controller("partiesController", function ($scope, $ionicPopup, $http, $rootScope, $localstorage, $filter) {

    window.scopeParties = $scope;

    $rootScope.$watch('visitorsSelected', function(visitors){
        $scope.visitorsSelected = visitors;
    });


    window.$local = $localstorage.getObject;

    //console.log('after', $scope)

    // navigator.contacts.pickContact(function(contact){
    //     console.log('The following contact has been selected:' + JSON.stringify(contact));
    // },function(err){
    //     console.log('Error: ' + err);
    // });

    $scope.partyForm = {};
    $scope.inputs = [];
    $scope.addBlocker = false;
    $scope.addMoreBlocker = false;
    $scope.invalidField = false;
    $scope.setInvalidQtd = false;
    $scope.emptyContact = true;

    if($scope.visitorsSelected.length < 1){
        $scope.emptyContact = true;
    } else {
        $scope.emptyContact = false;
    }

    $scope.addRow = function (form) {
        if (form.amount.$modelValue < 1 || form.amount.$modelValue > 5) {
            $scope.invalidField = true;
        } else {
            $scope.invalidField = false;
        }

        if (form.celular.$valid && $scope.addBlocker == false) {
            $scope.inputs.push({});
            $scope.addBlocker = true;
        }
    }

    $scope.addMoreRows = function (form, quant) {
        var phone = form.cellphone.$modelValue;
        var qtd = form.qtdade.$modelValue;
        $scope.addedFirst = false;

        if (phone !== '' && qtd !== null && $scope.addMoreBlocker == false) {
            $scope.inputs.push({});
            $scope.addMoreBlocker = true;
            $scope.addedFirst = true;
        }

        if (form.cellphone.$dirty && form.qtdade.$dirty && $scope.addedFirst == false) {
            $scope.inputs.push({});
        }

        angular.forEach($scope.inputs, function (v, k) {
            if (v.phone == undefined && v.qtd < 1 || v.phone == undefined && v.qtd > 5) {
                $scope.addMoreBlocker = true;
            }
        });

        if (quant < 1 || quant > 5) {
            $scope.setInvalidQtd = true;
        } else {
            $scope.setInvalidQtd = false;
        }

    }

    var $_parentScope = $scope;

    $scope.partyList = function (isValid, form) {
        var thisForm = this;

        //console.log($_parentScope.visitorsSelected)
        var user = JSON.parse(localStorage.getItem('user'));
        var convidadosPost = '';

        $scope.convidados = [];


        if ($scope.visitorsSelected.length < 1) {
            $ionicPopup.alert({
                title: 'Por favor, selecione, pelo menos, um contato.',
                cssClass: 'popStyle'
            });

            return false
        }


        //$scope.convidados = [{
        //phone: this.partyForm.phone,
        //qtd: this.partyForm.qtd
        //}];

        //convidadosPost = this.partyForm.phone + "," + this.partyForm.qtd + ";";

        //console.error('convidadosPost', convidadosPost)

        angular.forEach($scope.visitorsSelected, function (value, key) {
            $scope.moreGuests = {
                phone: value.phoneSelected,
                qtd: value.tickets
            };

            if (value.phoneSelected !== null && typeof value.phoneSelected !== "undefined" && typeof value.tickets !== "undefined") {
                $scope.convidados.push($scope.moreGuests);

                console.log('==========================')
                clog(value)

                convidadosPost += $filter('telPost')(value.phoneSelected) + "," + value.tickets + "," + value.name + ";";
            }
        });

        console.warn('SCOPE.CONVIDADOSS', $scope.convidados)
        console.warn('convidadosPost', convidadosPost)

        //
        //angular.forEach($scope.inputs, function (value, key) {
        //    $scope.moreGuests = {
        //        phone: value.phone,
        //        qtd: value.qtd
        //    };
        //
        //    if (value.phone !== undefined || value.qtd !== undefined) {
        //        $scope.convidados.push($scope.moreGuests);
        //        convidadosPost += value.phone + "," + value.qtd + ";";
        //    }
        //});

        $scope.reset = function () {
            thisForm.partyForm.hora = "";
            thisForm.partyForm.phone = "";
            thisForm.partyForm.qtd = "";
            thisForm.partyForm.horario.$touched = false;
            $scope.visitorsSelected = [];
            $rootScope.visitorsSelected = [];
            $scope.addBlocker = false;
            $scope.addMoreBlocker = false;
        }

        var platform = platform || 'plataforma';
        var device = device || 'device';
        if (platform.indexOf("iPhone") == 0) {
            platform = 'iPhone'
        } else {
            platform = 'Android'
        }

        $scope.festaForm = {
            userObj: user,
            user: user.data.USER_ID,
            dataIni: moment($scope.datepickerParty.inputDate).format('YYYY/MM/DD'),
            horaIni: moment(this.partyForm.hora).format('HH:mm'),
            celular: convidadosPost,
            tipo: 'F',
            plataforma: platform,
            device: device
        };

        console.log($scope.festaForm);

        $rootScope.savingParty = true;

        $http({
            url: domain + '/access',
            method: 'POST',
            data: $scope.festaForm
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                // sucesso
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.reset();

                $rootScope.visitorsSelected = [];

                $rootScope.savingParty = false;
            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $rootScope.savingParty = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });

            $rootScope.savingParty = false;
            $scope.inputs = [];
            $scope.visitorsSelected = [];
            $scope.addBlocker = false;
            $scope.addMoreBlocker = false;
        });
    }


});


var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};
app.directive("compareTo", compareTo);

angular.module('components', [])
    .directive('visitantes', function ($filter, $cordovaContacts, $ionicScrollDelegate, $ionicPopup, $rootScope, $localstorage) {
        return {
            restrict: 'E',
            replace: true,
            link: function ($scope, $element) {

                window._scopeVisitantes = $scope;

                $scope.accessOptions = [
                    {name: 1, qtd: 1},
                    {name: 2, qtd: 2},
                    {name: 3, qtd: 3},
                    {name: 4, qtd: 4},
                    {name: 5, qtd: 5}
                ];

                $scope.acessos = {
                    qtd: $scope.accessOptions[0].qtd
                };

                $rootScope.visitorsSelected = $rootScope.visitorsSelected || [];

                // $rootScope.visitorsSelected.push({
                //    'id': '',
                //    'tickets': 1,
                //    'name': 'Rodolfo',
                //    'phoneSelected': '11960540130',
                //    'picture': 'https://i.stack.imgur.com/IEKbS.png?s=128&g=1&g&s=32'
                // });

                var showChoosePhonePopup = function (contact) {
                    $scope.contatoSelecionado = contact;
                    $scope.contatoSelecionado.numeros = [];

                    angular.forEach(contact.phoneNumbers, function (v, k) {
                        $scope.contatoSelecionado.numeros.push(contact.phoneNumbers[k].value);
                    });

                    $scope.data = {};

                    $scope.data.phones = $scope.contatoSelecionado.numeros;

                    var myPopup = $ionicPopup.show({
                        templateUrl: 'templates/choose-phone.html',
                        title: 'Selecione o número de telefone',
                        subTitle: 'Este contato tem mais de um número associado a ele',
                        scope: $scope,
                        buttons: [
                            {text: 'Cancelar'},
                            {
                                text: '<b>Escolher</b>',
                                type: 'button-positive',
                                onTap: function (e) {
                                    if (!$scope.data.phoneSelected) {
                                        clog(' IF -----------------------')
                                        clog($scope.data.phoneSelected)
                                        e.preventDefault();
                                    } else {
                                        clog(' ELSE -----------------------')
                                        contact.phoneSelected = $scope.data.phoneSelected;
                                        $scope.addContact(contact)
                                    }
                                }
                            }
                        ]
                    });
                }

                $scope.scrollToMe = function () {

                }

                $scope.addContact = function (contact) {
                    console.error('rootscope é isso daqui', $rootScope)

                    console.log('OLHA SO O QUE EU RECEBI', contact);

                    $rootScope.visitorsSelected.push({
                        'id': contact.id,
                        'name': contact.name.formatted,
                        'phoneSelected': contact.phoneSelected,
                        'tickets': contact.tickets,
                        'picture': function (contact) {
                            console.log(contact.photos)
                            console.log(JSON.stringify(contact.photos))
                            if (contact.photos !== null) {
                                if (contact.photos instanceof Array) {
                                    return contact.photos[0].value;
                                }
                                return 'img/user.jpg'
                            } else {
                                return 'img/user.jpg'
                            }
                        }(contact)
                    });


                    $ionicScrollDelegate.scrollBottom();


                    clog($scope.visitorsSelected)
                }

                $scope.remove = function (contactIndex, contact) {
                    var confirm = $ionicPopup.confirm({
                        title: 'Remover contato',
                        template: 'Tem certeza de que deseja remover <b>' + contact.name + '</b> da lista?',
                        cancelText: 'Cancelar',
                        okText: 'Sim, remover'
                    }).then(function (res) {
                        if (res) {
                            $rootScope.visitorsSelected.splice(contactIndex, 1)
                        } else {

                        }
                    });

                    return;
                }

                $scope.selectContact = function () {
                    if (!isMobile) {

                        //$rootScope.visitorsSelected.push({
                        //    'id': '',
                        //    'tickets': 3,
                        //    'name': 'Larissa Tobias',
                        //    'phoneSelected': '+5511988887765',
                        //    'picture': 'https://i.stack.imgur.com/89dSf.png?s=128&g=1&g&s=32'
                        //});
                        //
                        //$ionicScrollDelegate.scrollBottom();
                        //return;
                    }


                    $cordovaContacts.pickContact().then(function (contact) {
                        clog(contact);

                        if (!Array.isArray(contact.phoneNumbers)) {
                            $ionicPopup.alert({
                                title: 'Contato Inválido',
                                template: 'Não há nenhum número de celular associado a esse contato. Por favor, selecione outro.'
                            });
                            return;
                        }


                        if ($('.party-visitors[data-visitor-id="' + contact.id + '"]').length > 0) {
                            $ionicPopup.alert({
                                title: 'Contato já selecionado!',
                                template: 'Selecione outro'
                            });
                            return false;
                        }


                        if (contact.phoneNumbers.length > 1) {
                            contact.phoneSelected = showChoosePhonePopup(contact);
                        } else {
                            contact.phoneSelected = contact.phoneNumbers[0].value;
                            $scope.addContact(contact)
                        }

                        $ionicScrollDelegate.scrollBottom();

                    });

                }
                window.sc = $scope;
                window.rs = $rootScope;

            },
            templateUrl: 'templates/tab-festas-visitantes.html'
        }


    })


app.factory('$localstorage', ['$window', function ($window) {
    return {
        set: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage[key] || '{}');
        },
        destroy: function (key) {
            if (typeof ($window.localStorage[key]) != 'undefined') {
                $window.localStorage.removeItem(key);
            }
        }
    }
}]);

window.phoneFormat = function (tel) {

    var oldTel = tel;


    if (!tel) {
        return '';
    }

    var value;

    if (tel.length > 9 && tel.slice(0, 2) == "55") {
        value = tel.replace('55', '')
    } else {
       value = tel.replace('+55', '')
    }

    if(tel.slice(0,1) == "0"){
        value = tel.replace('0', '')
    }

    var ddd = JSON.parse(localStorage.getItem('user')).data.USER_CEL.slice(0, 2)

    value = value.toString().trim().replace(/\D/g, '');
    value = value.replace(/^\+/g, '')
        .replace(' ', '')
        .replace(' ', '')
        .replace('-', '')
        .replace('-', '');

    if (value.match(/[^0-9]/)) {
        return tel.replace(/\s+/, '').replace(' ', '').replace('+55', '');
    }

    if (value.length > 11) {
        value = value.substr(value.length - 11);
    }


    var country, city, number;

    switch (value.length) {
        case 8:
        case 9:
            country = 55;
            city = ddd;
            number = value;
            break;

        case 10:
        case 11:
            country = 55;
            city = value.slice(0, 2);
            number = value.slice(2);
            break;

        case 13:
            country = value.slice(0, 2);
            city = value.slice(2, 4);
            number = value.slice(4);
            break;

        default:
            return tel;
    }

    if (country == 1) {
        country = "";
    }

    var telOutput = {
        ddd: city.toString(),
        phone: number.toString()
    };

    console.log('%c TELEFONE FORMATADO! \n Antigo: ' + oldTel + ' \n Novo: ' + JSON.stringify(telOutput, null, 4), 'background: #222; color: #bada55');

    return telOutput;
}


app.filter('tel', function ($localstorage) {
    return function (tel) {

        var number = phoneFormat(tel);
        var position = Math.round(number.phone.length/2);
        var phone = number.phone.slice(0, position) + '-' + number.phone.slice(position);

        return ("(" + number.ddd + ") " + phone).trim();

    };
});


app.filter('telPost', function () {
    return function (tel) {
        if (!tel) {
            return '';
        }
        var number = phoneFormat(tel);
        return number.ddd + number.phone;

    };
});


app.filter('cards', function () {
    return function (cards) {
        var output = [];

        angular.forEach(cards, function(card){
            var filtered = $filter('filter')(card.panictypes, {val : true});

            console.log(filtered);

            if(filtered.length != 0) {
                if (card.phone != "") output.push(card)
            }
        });

        window.localStorage.setItem('panic', JSON.stringify(output));

        return output;
    };
});

app.factory('CallService', CallService);

CallService.$inject = ['$httpParamSerializerJQLike', 'UserService'];

function CallService($httpParamSerializerJQLike, UserService) {
    return {

        /**
         * @returns Object
         */
        user: function () {
            return UserService.get().data;
        }(),

        /**
         * @returns Object
         */
        panic: function () {
            return UserService.panic()
        }(),

        /**
         *
         * @param type
         * @param dataToUrl
         * @param phone
         * @returns Promise
         */
        request: function (type, dataToUrl, phone) {
            console.info('CallService.request() called with params:', JSON.stringify(arguments, null, 2));

            // var answerUrl = 'http://vihu.monstro.io/api/say/panic?' + dataToUrl;
            var answerUrl = 'http://vihu.monstro.io/calls/say?' + dataToUrl;
            // var answerUrl = 'http://vihu.monstro.io/ura.xml';

            console.warn(answerUrl);

            return this.requested = jQuery.ajax({
                url: "https://rest.nexmo.com/call/json",
                type: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                contentType: "application/x-www-form-urlencoded",
                data: {
                    "api_secret": "f17108ba7fbb1227",
                    "to": phone,
                    "api_key": "0f398013",
                    "answer_url": answerUrl,
                    "from": "551149331221",
                    'machine_detection': true,
                    'machine_timeout': '400ms',
                    'status_method': 'POST',
                    'status_url': 'http://vihu.monstro.io/calls/callback',
                    'error_method': 'POST',
                    'error_url': 'http://vihu.monstro.io/calls/callback'
                },
            })
        },

        /**
         * Hora da piadinha
         *
         * @param params
         * @returns {*}
         */
        call: function (params) {
            console.info('CallService.call() called with params:', params);

            if (params.action == "panic") {
                return this.callPanic(params);
            }
        },

        /**
         * Leandrão reclamão
         *
         * @param params
         */
        callPanic: function (params) {
            var $this = this;

            params['message'] = 'x';
            params['name'] = this.user.USER_NAME;
            params['apartamento'] = this.user.TORRE_APTO;
            params['condominio'] = this.user.CONDOMINIO;
            params['type'] = params['reason']; // @todo mover para reason


            console.log('================================================================', params);

            _.each(params.phones, function (phone, key) {
                phone = '55' + phone.toString();

                var request = $this.request('panic', $httpParamSerializerJQLike(params), phone);

                console.warn('Phone ' + key + ' called', '\nPhone = ', phone, '\nRequest = ', request)

            })
        },
    }
}
app.service('PanicService', PanicService);

PanicService.$inject = ['$localstorage', 'CallService'];

function PanicService($localstorage, CallService) {
    return {
        phones: null,

        /**
         *
         * @returns {Array}
         */
        get: function () {
            var panic = $localstorage.getObject('panic'),
                phones = [];

            _.each(panic, function (obj) {
                phones.push({
                    'phone': obj.phone,
                    'actions': _.map(_.filter(_.each(obj.panictypes, function (type) {
                        type.label = type.label.slugify();
                        return type;
                    }), {val: true}), 'label')
                });
            });

            return this.phones = phones;
        },

        /**
         *
         * @param type
         * @returns {Array}
         */
        getPhonesByAction: function (type) {
            var type = type.slugify(),
                panic = this.get();

            var filter = _.filter(panic, function (val) {
                return val.actions.indexOf(type) !== -1;
            });

            if (Object.prototype.toString.apply(filter) === '[object Array]') {
                var phones = [];
                filter.forEach(function (value, key) {
                    phones.push(value.phone);
                })
                return phones;
            }

            return filter;
        },

        /**
         *
         * @param panicType
         * @returns {*}
         */
        call: function (panicType) {
            console.info('PanicService.call() called');

            return CallService.call({
                action: 'panic',
                reason: panicType.slugify(),
                phones: this.getPhonesByAction(panicType)
            });
        }
    }
}
app.service('UserService', function ($localstorage) {
    return {
        get: function () {
            return $localstorage.getObject('user');
        },
        panic: function () {
            return $localstorage.getObject('panic');
        }
    }
})