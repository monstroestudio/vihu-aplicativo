function clog(param) {
    console.warn(JSON.stringify(param, null, 2));
}

var app = angular.module('vihu',
    ['ionic',
        'ngCordova',
        'ionic-datepicker',
        'ngIOS9UIWebViewPatch',
        'ui.utils.masks',
        '720kb.tooltips',
        'components',
        'ionic-native-transitions'
    ]);



var geolocationSuccess = function (position) {
    console.log('Latitude: ' + position.coords.latitude + '\n' +
        'Longitude: ' + position.coords.longitude + '\n' +
        'Altitude: ' + position.coords.altitude + '\n' +
        'Accuracy: ' + position.coords.accuracy + '\n' +
        'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
        'Heading: ' + position.coords.heading + '\n' +
        'Speed: ' + position.coords.speed + '\n' +
        'Timestamp: ' + position.timestamp + '\n');
};

var geolocationError = function (error) {
    console.log('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
};

var device;
//var isMobile = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|Windows Phone)/);
var isMobile = navigator.userAgent.match(/(iPhone)/); //truque pra funcionar no Chrome

document.addEventListener('deviceready', function onDeviceReady() {
    device = device.uuid;
    // console.log(navigator.contacts.find);
}, true);

app.run(function ($ionicPlatform, $rootScope, $http, $ionicPopup, PanicService) {
    $ionicPlatform.ready(function () {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });

    $rootScope.generalPanicTypes = ["Chamar supervisor", "Assalto em andamento", "Problema de saúde"];
    $rootScope.visitorsSelected = [];
    $rootScope.accessVisitorsSelected = {};
    $rootScope.panicPress = false;

    window.rs = $rootScope;

    $rootScope.Panic = function (type) {
        $rootScope.panicPopup.close();

        console.log(type)

        // PanicService.call(type);


        // return;
        // $scope.$scope.panicPopup.close();

        // this.disabled = true;

        var user = JSON.parse(localStorage.getItem('user')).data;
        var panic = JSON.parse(localStorage.getItem('panic'));


        var amountPhones = [];

        angular.forEach(panic, function (_panic) {
            console.warn('Dentro do forEach, $rootScope.generalPanicTypes[type] = ', $rootScope.generalPanicTypes[type - 1])
            angular.forEach($filter('filter')(_panic.panictypes, {
                label: $rootScope.generalPanicTypes[type - 1],
                val: true
            }), function (panic) {
                amountPhones.push(panic)
            });
        });

        console.warn('amountPhones', amountPhones, 'length=>', amountPhones.length)


        if (panic.length == 0) {
            $rootScope.panicWarning();
            return;
        }


        if (amountPhones.length == 0) {
            $rootScope.panicWarning();
            return;
        }

        var phones = [];
        _.each(panic, function (obj) {
            phones.push({
                'phone': obj.phone,
                'actions': _.map(_.filter(obj.panictypes, {val: true}), 'label')
            });
        });


        var data = {
            userObj: user,
            user: user,
            panic: phones,
            action: 'panic'
        }

        $http({
            url: domain + '/panic/' + type,
            method: 'POST',
            data: data
        }).then(function (data) {

            console.log('Vim do webservice após pressionamento do botão de pânico', data);
            //if (typeof data === "object") {
            //    console.warn('Data received:', data.data);
            //    $ionicPopup.alert({
            //        title: 'O botão de pânico foi acionado. <br> Fique calmo(a)! Em breve, alguém entrará em contato com você.',
            //        cssClass: 'popStyle'
            //    });
            //}
        }, function (response) {
            //$ionicPopup.alert({
            //    title: 'Ocorreu um erro.',
            //    cssClass: 'popStyle'
            //});
        })

        console.log('BOTÃO DE PANICO CLICADO >' + type, user);

        return true;
    }
});

// var domain = 'http://vihu.api.monstroestudio.com.br/api';
// var domain = 'http://vihu.monstro.io/api';
var domain = 'http://api.vihuwebservice.info/api';
// var domain = 'http://vihuwebservices.app/api';
