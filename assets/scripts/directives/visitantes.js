angular.module('components', [])
    .directive('visitantes', function ($filter, $cordovaContacts, $ionicScrollDelegate, $ionicPopup, $rootScope, $localstorage) {
        return {
            restrict: 'E',
            replace: true,
            link: function ($scope, $element) {

                window._scopeVisitantes = $scope;

                $scope.accessOptions = [
                    {name: 1, qtd: 1},
                    {name: 2, qtd: 2},
                    {name: 3, qtd: 3},
                    {name: 4, qtd: 4},
                    {name: 5, qtd: 5}
                ];

                $scope.acessos = {
                    qtd: $scope.accessOptions[0].qtd
                };

                $rootScope.visitorsSelected = $rootScope.visitorsSelected || [];

                // $rootScope.visitorsSelected.push({
                //    'id': '',
                //    'tickets': 1,
                //    'name': 'Rodolfo',
                //    'phoneSelected': '11960540130',
                //    'picture': 'https://i.stack.imgur.com/IEKbS.png?s=128&g=1&g&s=32'
                // });

                var showChoosePhonePopup = function (contact) {
                    $scope.contatoSelecionado = contact;
                    $scope.contatoSelecionado.numeros = [];

                    angular.forEach(contact.phoneNumbers, function (v, k) {
                        $scope.contatoSelecionado.numeros.push(contact.phoneNumbers[k].value);
                    });

                    $scope.data = {};

                    $scope.data.phones = $scope.contatoSelecionado.numeros;

                    var myPopup = $ionicPopup.show({
                        templateUrl: 'templates/choose-phone.html',
                        title: 'Selecione o número de telefone',
                        subTitle: 'Este contato tem mais de um número associado a ele',
                        scope: $scope,
                        buttons: [
                            {text: 'Cancelar'},
                            {
                                text: '<b>Escolher</b>',
                                type: 'button-positive',
                                onTap: function (e) {
                                    if (!$scope.data.phoneSelected) {
                                        clog(' IF -----------------------')
                                        clog($scope.data.phoneSelected)
                                        e.preventDefault();
                                    } else {
                                        clog(' ELSE -----------------------')
                                        contact.phoneSelected = $scope.data.phoneSelected;
                                        $scope.addContact(contact)
                                    }
                                }
                            }
                        ]
                    });
                }

                $scope.scrollToMe = function () {

                }

                $scope.addContact = function (contact) {
                    console.error('rootscope é isso daqui', $rootScope)

                    console.log('OLHA SO O QUE EU RECEBI', contact);

                    $rootScope.visitorsSelected.push({
                        'id': contact.id,
                        'name': contact.name.formatted,
                        'phoneSelected': contact.phoneSelected,
                        'tickets': contact.tickets,
                        'picture': function (contact) {
                            console.log(contact.photos)
                            console.log(JSON.stringify(contact.photos))
                            if (contact.photos !== null) {
                                if (contact.photos instanceof Array) {
                                    return contact.photos[0].value;
                                }
                                return 'img/user.jpg'
                            } else {
                                return 'img/user.jpg'
                            }
                        }(contact)
                    });


                    $ionicScrollDelegate.scrollBottom();


                    clog($scope.visitorsSelected)
                }

                $scope.remove = function (contactIndex, contact) {
                    var confirm = $ionicPopup.confirm({
                        title: 'Remover contato',
                        template: 'Tem certeza de que deseja remover <b>' + contact.name + '</b> da lista?',
                        cancelText: 'Cancelar',
                        okText: 'Sim, remover'
                    }).then(function (res) {
                        if (res) {
                            $rootScope.visitorsSelected.splice(contactIndex, 1)
                        } else {

                        }
                    });

                    return;
                }

                $scope.selectContact = function () {
                    if (!isMobile) {

                        //$rootScope.visitorsSelected.push({
                        //    'id': '',
                        //    'tickets': 3,
                        //    'name': 'Larissa Tobias',
                        //    'phoneSelected': '+5511988887765',
                        //    'picture': 'https://i.stack.imgur.com/89dSf.png?s=128&g=1&g&s=32'
                        //});
                        //
                        //$ionicScrollDelegate.scrollBottom();
                        //return;
                    }


                    $cordovaContacts.pickContact().then(function (contact) {
                        clog(contact);

                        if (!Array.isArray(contact.phoneNumbers)) {
                            $ionicPopup.alert({
                                title: 'Contato Inválido',
                                template: 'Não há nenhum número de celular associado a esse contato. Por favor, selecione outro.'
                            });
                            return;
                        }


                        if ($('.party-visitors[data-visitor-id="' + contact.id + '"]').length > 0) {
                            $ionicPopup.alert({
                                title: 'Contato já selecionado!',
                                template: 'Selecione outro'
                            });
                            return false;
                        }


                        if (contact.phoneNumbers.length > 1) {
                            contact.phoneSelected = showChoosePhonePopup(contact);
                        } else {
                            contact.phoneSelected = contact.phoneNumbers[0].value;
                            $scope.addContact(contact)
                        }

                        $ionicScrollDelegate.scrollBottom();

                    });

                }
                window.sc = $scope;
                window.rs = $rootScope;

            },
            templateUrl: 'templates/tab-festas-visitantes.html'
        }


    })
