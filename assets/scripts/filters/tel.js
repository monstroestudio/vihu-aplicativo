window.phoneFormat = function (tel) {

    var oldTel = tel;


    if (!tel) {
        return '';
    }

    var value;

    if (tel.length > 9 && tel.slice(0, 2) == "55") {
        value = tel.replace('55', '')
    } else {
       value = tel.replace('+55', '')
    }

    if(tel.slice(0,1) == "0"){
        value = tel.replace('0', '')
    }

    var ddd = JSON.parse(localStorage.getItem('user')).data.USER_CEL.slice(0, 2)

    value = value.toString().trim().replace(/\D/g, '');
    value = value.replace(/^\+/g, '')
        .replace(' ', '')
        .replace(' ', '')
        .replace('-', '')
        .replace('-', '');

    if (value.match(/[^0-9]/)) {
        return tel.replace(/\s+/, '').replace(' ', '').replace('+55', '');
    }

    if (value.length > 11) {
        value = value.substr(value.length - 11);
    }


    var country, city, number;

    switch (value.length) {
        case 8:
        case 9:
            country = 55;
            city = ddd;
            number = value;
            break;

        case 10:
        case 11:
            country = 55;
            city = value.slice(0, 2);
            number = value.slice(2);
            break;

        case 13:
            country = value.slice(0, 2);
            city = value.slice(2, 4);
            number = value.slice(4);
            break;

        default:
            return tel;
    }

    if (country == 1) {
        country = "";
    }

    var telOutput = {
        ddd: city.toString(),
        phone: number.toString()
    };

    console.log('%c TELEFONE FORMATADO! \n Antigo: ' + oldTel + ' \n Novo: ' + JSON.stringify(telOutput, null, 4), 'background: #222; color: #bada55');

    return telOutput;
}


app.filter('tel', function ($localstorage) {
    return function (tel) {

        var number = phoneFormat(tel);
        var position = Math.round(number.phone.length/2);
        var phone = number.phone.slice(0, position) + '-' + number.phone.slice(position);

        return ("(" + number.ddd + ") " + phone).trim();

    };
});


app.filter('telPost', function () {
    return function (tel) {
        if (!tel) {
            return '';
        }
        var number = phoneFormat(tel);
        return number.ddd + number.phone;

    };
});


app.filter('cards', function () {
    return function (cards) {
        var output = [];

        angular.forEach(cards, function(card){
            var filtered = $filter('filter')(card.panictypes, {val : true});

            console.log(filtered);

            if(filtered.length != 0) {
                if (card.phone != "") output.push(card)
            }
        });

        window.localStorage.setItem('panic', JSON.stringify(output));

        return output;
    };
});
