app.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $ionicNativeTransitionsProvider) {
    // $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.scrolling.jsScrolling(false);
    $ionicConfigProvider.views.maxCache(3);

    $ionicNativeTransitionsProvider.setDefaultOptions({
        duration: 400, // in milliseconds (ms), default 400,
        slowdownfactor: 4, // overlap views (higher number is more) or no overlap (1), default 4
        iosdelay: -1, // ms to wait for the iOS webview to update before animation kicks in, default -1
        androiddelay: -1, // same as above but for Android, default -1
        winphonedelay: -1, // same as above but for Windows Phone, default -1,
        fixedPixelsTop: 0, // the number of pixels of your fixed header, default 0 (iOS and Android)
        fixedPixelsBottom: 0, // the number of pixels of your fixed footer (f.i. a tab bar), default 0 (iOS and Android)
        triggerTransitionEvent: '$ionicView.afterEnter', // internal ionic-native-transitions option
        backInOppositeDirection: false // Takes over default back transition and state back transition to use the opposite direction transition to go back
    });
    // @todo: REMOVER EM PRODUÇÃO
    // desabilita o cache global
    // $ionicConfigProvider.views.maxCache(0);


    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html'
        })
        .state('home', {
            url: '/home',
            templateUrl: 'templates/tab-home.html'
        })
        .state('delivery', {
            url: '/delivery',
            templateUrl: 'templates/tab-delivery.html',
            controller: 'deliveryController',
            nativeTransitionsAndroid: {
                "type": "flip",
                "direction": "right"
            },
        })
        .state('festas', {
            url: '/festas',
            templateUrl: 'templates/tab-festas.html',
            controller: 'partiesController',
            nativeTransitionsAndroid: {
                "type": "flip",
                "direction": "right"
            },
        })
        .state('zelador', {
            url: '/zelador',
            templateUrl: 'templates/tab-zelador.html'
        })
        .state('relatorios', {
            url: '/relatorios',
            templateUrl: 'templates/tab-relatorios.html'
        })
        .state('change', {
            url: '/change',
            templateUrl: 'templates/change-pass.html'
        })
        .state('choosePhone', {
            url: '/choosePhone',
            templateUrl: 'templates/choose-phone.html'
        });

    // if none of the above states are matched, use this as the fallback
    // $urlRouterProvider.when("", "/login");


    //$urlRouterProvider.when("/", "/login");
    //$urlRouterProvider.otherwise('/login');
});
