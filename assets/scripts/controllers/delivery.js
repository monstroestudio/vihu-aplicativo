
app.controller("deliveryController", function ($scope, $ionicPopup, $rootScope, $http) {
    $scope.showCodes = false;

    // DELIVERY FORM
    $scope.deliveries = [];
    $scope.requestDelivery = function () {
        var user = JSON.parse(localStorage.getItem('user'));
        var device = device || 'device';
        var platform = platform || 'platform';
        if (platform.indexOf("iPhone") == 0) {
            platform = 'iPhone'
        } else {
            platform = 'Android'
        }

        $scope.deliveryForm = {
            user: user.data.USER_ID,
            dataIni: moment().format('YYYY/MM/DD'),
            horaIni: moment().format('HH:mm'),
            qtde: 1,
            celular: user.data.USER_CEL,
            tipo: 'D',
            plataforma: platform,
            device: device
        }
        $scope.savingDelivery = true;

        $http({
            url: domain + '/access',
            method: 'POST',
            data: $scope.deliveryForm
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                // senha solicitada

                var alertPopup = $ionicPopup.alert({
                    title: " ",
                    template: data.message,
                    cssClass: 'popStyle'
                });
                alertPopup.then(function (res) {
                    $scope.showCodes = true;

                    var delivery = {code: data.code, codeTime: moment().format('DD/MM/YYYY HH:mm')};
                    $scope.deliveries.push(delivery);

                    var itemsFromLocalStorage = [];

                    if (JSON.parse(localStorage.getItem('deliveryCode')) == null) {
                        localStorage.setItem('deliveryCode', JSON.stringify(itemsFromLocalStorage));
                        // console.log('criou localStorage');
                    }

                    var newCodes = [];
                    newCodes = JSON.parse(localStorage.getItem('deliveryCode'));
                    newCodes.push(delivery);

                    // console.log(newCodes);
                    localStorage.setItem('deliveryCode', JSON.stringify(newCodes));
                });
                $scope.savingDelivery = false;

            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingDelivery = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingDelivery = false;
        });
    };

    // LOAD CODES FROM LOCALSTORAGE
    if (JSON.parse(localStorage.getItem('deliveryCode')) !== null) {
        $scope.showCodes = true;
        $scope.deliveries = JSON.parse(localStorage.getItem('deliveryCode'));

        var i;
        for (i = $scope.deliveries.length - 1; i >= 0; i -= 1) {
            if (moment($scope.deliveries[i].codeTime, 'DD/MM/YYYY HH:mm').add(8, 'h').unix() < moment().unix()) {
                $scope.deliveries.splice(i, 1);

                console.log('perdeu o prazo ' + i);
                localStorage.setItem('deliveryCode', JSON.stringify($scope.deliveries));
            }
        }

    } else {
        $scope.showCodes = false;
    }

});
