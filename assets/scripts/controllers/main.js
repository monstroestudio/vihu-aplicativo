app.controller('MainController', function ($scope,
                                           $rootScope,
                                           $http,
                                           $timeout,
                                           $ionicLoading,
                                           $ionicPopup,
                                           $ionicLoading,
                                           $ionicTabsDelegate,
                                           $state,
                                           $ionicModal,
                                           $ionicSideMenuDelegate,
                                           $ionicHistory,
                                           $sce,
                                           $filter,
                                           $cordovaContacts,
                                           $ionicSlideBoxDelegate,
                                           $log,
                                           $window,
                                           CallService,
                                           PanicService,
                                           UserService) {

    window.http = $http;
    window.CallService = CallService;
    window.PanicService = PanicService;
    window.UserService = UserService;

    /**
     var req = {
 method: 'POST',
 url: 'https://rest.nexmo.com/call/json',
 headers: {
   'Content-Type': 'application/x-www-form-urlencoded'
 },
 data: {
        "api_secret": "f17108ba7fbb1227",
        "to": "5511960540130",
        "api_key": "0f398013",
        "answer_url": "http://vihu.monstro.io/ura.xml",
        "from": "551149331221",
  }
}

     http(req).then(function(){console.log(arguments)}, function(){console.error(arguments)});
     */
    // $log.debug("$window.device", $window.device);
    // $log.debug("$window.device as Boolean", $window.device ? true : false);
    // $log.debug("ionic.Platform.device()", ionic.Platform.device());
    // $log.debug("ionic.Platform.device() === {}", ionic.Platform.device() === {});


    window._scopeMain = $scope;

    $scope.accessOptions = [
        {name: '1', qtd: '1'},
        {name: '2', qtd: '2'},
        {name: '3', qtd: '3'},
        {name: '4', qtd: '4'},
        {name: '5', qtd: '5'},
    ];

    // $scope.onlyExiting = false;
    // $scope.onlyExiting = { checked: true };

    //
    $scope.$watch($scope.onlyExiting, function () {
        console.log(arguments);
    })

    $scope.acessos = {qtd: $scope.accessOptions[0].qtd};


    var showChoosePhonePopup = function (contact) {

        $scope.data = {};

        $scope.data.phones = $scope.contatoSelecionado.numeros;

        var myPopup = $ionicPopup.show({
            templateUrl: 'templates/choose-phone.html',
            title: 'Selecione o número de telefone',
            subTitle: 'Este contato tem mais de um número associado a ele',
            scope: $scope,
            buttons: [
                {text: 'Cancelar'},
                {
                    text: '<b>Escolher</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        if (!$scope.data.phoneSelected) {
                            $scope.invalidPhone = true;
                            console.error('if, $scope.data =>', $scope.data)
                            e.preventDefault();
                        } else {
                            $scope.invalidPhone = false;
                            console.error('else, $scope.data =>', $scope.data)
                            return $rootScope.accessVisitorsSelected.phone = $scope.data.phone;
                        }
                    }
                }
            ]
        });


        myPopup.then(function () {
            // $scope.data.phone.replace(/[^\d]/g,'');
        });
    }

    $scope.selecionarContato = function () {
        $cordovaContacts.pickContact().then(function (contato) {
            console.log(contato);

            if (typeof contato.phoneNumbers === "undefined" || contato.phoneNumbers == null || contato.phoneNumbers == "null" || !Array.isArray(contato.phoneNumbers)) {
                $ionicPopup.alert({
                    title: 'Contato Inválido',
                    template: 'Não há nenhum número de celular associado a esse contato. Por favor, selecione outro.'
                });
                return;
            }

            $scope.contatoSelecionado = contato;
            $rootScope.accessVisitorsSelected.name = contato.name.formatted;

            if (contato.phoneNumbers.length > 1) {

                $scope.contatoSelecionado.numeros = [];

                angular.forEach(contato.phoneNumbers, function (v, k) {
                    $scope.contatoSelecionado.numeros.push(contato.phoneNumbers[k].value);

                    console.log(contato.phoneNumbers[k].value);
                })

                showChoosePhonePopup(contato);

                return;
            } else {

                $scope.contatoSelecionado = contato;
                $rootScope.accessVisitorsSelected.name = contato.name.formatted;

                console.error("O BAGULHO FICOU LOUCO!!!!!", contato)
                if (typeof $scope.data == "undefined") $scope.data = {};
                $scope.data.phoneSelected = $rootScope.accessVisitorsSelected.phone = $scope.contatoSelecionado.phoneNumbers[0].value

                console.log($scope.data.phoneSelected, $rootScope.accessVisitorsSelected.phone, $scope.contatoSelecionado.phoneNumbers[0].value);

            }

            // console.log($scope.contatoSelecionado.photos);

            if ($scope.contatoSelecionado.photos !== null) {
                if ($scope.contatoSelecionado.photos instanceof Array) {
                    $scope.contatoSelecionado.foto = $scope.contatoSelecionado.photos[0].value;
                }
            } else {
                $scope.contatoSelecionado.foto = 'img/user.jpg';
            }
        });
    }

    window.scope = $scope;

    $scope.logged = false;
    $scope.password;
    $scope.activeMenu = 0;
    $scope.setInvalidAmount = false;
    $scope.invalidPhone;

    $scope.validateAmount = function (qtd) {
        if (qtd < 1 || qtd > 5) {
            $scope.setInvalidAmount = true;
            $scope.setInvalidQtd = true;
        } else {
            $scope.setInvalidAmount = false;
            $scope.setInvalidQtd = false;
        }
    }

// CHECK LOGIN
    $scope.checkLogin = function () {
        var user = JSON.parse(localStorage.getItem('user'));

        //var user = {
        //    status: "SUCCESS",
        //    data: {
        //        CELULAR_ZEL: "11947929861",
        //        CONDOMINIO: "Mundial Ecológica",
        //        EMAIL_ZEL: "eduardo@mundialecologica.com.br",
        //        ID_CONDOMINIO: "2",
        //        ID_TIPO_USUARIO: "1",
        //        NOME_ZEL: "Eduardo",
        //        TORRE_APTO: "Bloco A - 323",
        //        USER_CEL: "11982616336",
        //        USER_EMAIL: "alessandro@monstroestudio.com.br",
        //        USER_ID: "16",
        //        USER_NAME: "Alessandro Luz"
        //    }
        //};

        //localStorage.setItem('user', JSON.stringify(user));

        //document.location.href = '#/festas';

        if (user == null) {
            $scope.logged = false;
            console.log('deslogado');
            document.location.href = '#/login';
        } else {
            $scope.logged = true;
            document.location.href = '#/home';
            console.log('logado');
            console.log(user);
        }
    }
// LOGOUT
    $scope.sair = function () {
        localStorage.removeItem('user');
        $scope.checkLogin();
    }

// MENU
    $scope.toggleLeftSideMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
    };
    $scope.callChangePass = function () {
        $state.go('change');
    }
    $scope.callTab = function (index) {
        $ionicTabsDelegate.select(index);
    }

// MODAL FORGOT PASS
    $ionicModal.fromTemplateUrl('templates/forgot-pass.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modalForgot) {
        $scope.modalForgot = modalForgot;
    });
    $scope.openModalForgot = function () {
        $scope.modalForgot.show();
    };
    $scope.closeModalForgot = function () {
        $scope.modalForgot.hide();
    };

// MODAL CHANGE PASS
    $ionicModal.fromTemplateUrl('templates/change-pass.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modalChange) {
        $scope.modalChange = modalChange;
    });
    $scope.openModalChange = function () {
        $scope.modalChange.show();
    };
    $scope.closeModalChange = function () {
        $scope.modalChange.hide();
    };

// MODAL SETTINGS
    $ionicModal.fromTemplateUrl('templates/settings.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modalSettings) {
        $scope.modalSettings = modalSettings;
    });
    $scope.openModalSettings = function () {
        $scope.modalSettings.show();
        $scope.loadSettings();
    };
    $scope.closeModalSettings = function () {
        $scope.modalSettings.hide();
    };


// DATE PICKER
    $scope.datepickerObject = {};
    $scope.datepickerObject.inputDate = new Date();

    $scope.datepickerStart = {};
    $scope.datepickerStart.inputDate = new Date();

    $scope.datepickerEnd = {};
    $scope.datepickerEnd.inputDate = new Date();

    $scope.datepickerParty = {};
    $scope.datepickerParty.inputDate = new Date();

    var monthList = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
    var weekDaysList = ["D", "S", "T", "Q", "Q", "S", "S"];

    $scope.datepickerRelEnd = {
        titleLabel: 'Data Final',
        todayLabel: 'Hoje',
        closeLabel: 'Fechar',
        setLabel: 'Ok',
        setButtonType: 'button-assertive',
        todayButtonType: 'button-default',
        closeButtonType: 'button-default',
        inputDate: $scope.datepickerEnd.inputDate,
        mondayFirst: true,
        weekDaysList: weekDaysList,
        monthList: monthList,
        templateType: 'popup',
        showTodayButton: 'true',
        modalHeaderColor: 'bar-positive',
        modalFooterColor: 'bar-positive',
        from: new Date(2015, 10, 1),
        callback: function (val) {  //Mandatory
            datePickerFinal(val);
        },
        dateFormat: 'dd-MM-yyyy',
        closeOnSelect: false,
    };
    $scope.datepickerRelStart = {
        titleLabel: 'Data Inicial',
        todayLabel: 'Hoje',
        closeLabel: 'Fechar',
        setLabel: 'Ok',
        setButtonType: 'button-assertive',
        todayButtonType: 'button-default',
        closeButtonType: 'button-default',
        inputDate: $scope.datepickerStart.inputDate,
        mondayFirst: true,
        weekDaysList: weekDaysList,
        monthList: monthList,
        templateType: 'popup',
        showTodayButton: 'true',
        modalHeaderColor: 'bar-positive',
        modalFooterColor: 'bar-positive',
        from: new Date(2015, 10, 1),
        callback: function (val) {  //Mandatory
            datePickerInitial(val);
        },
        dateFormat: 'dd-MM-yyyy',
        closeOnSelect: false,
    };
    $scope.datepickerObjectPopup = {
        titleLabel: 'Data de Acesso',
        todayLabel: 'Hoje',
        closeLabel: 'Fechar',
        setLabel: 'Ok',
        setButtonType: 'button-assertive',
        todayButtonType: 'button-default',
        closeButtonType: 'button-default',
        inputDate: $scope.datepickerObject.inputDate,
        mondayFirst: true,
        weekDaysList: weekDaysList,
        monthList: monthList,
        templateType: 'popup',
        showTodayButton: 'true',
        modalHeaderColor: 'bar-positive',
        modalFooterColor: 'bar-positive',
        from: new Date(2015, 10, 1),
        callback: function (val) {  //Mandatory
            datePickerCallbackPopup(val);
        },
        dateFormat: 'dd-MM-yyyy',
        closeOnSelect: false,
    };
    $scope.datepickerFesta = {
        titleLabel: 'Data da Festa',
        todayLabel: 'Hoje',
        closeLabel: 'Fechar',
        setLabel: 'Ok',
        setButtonType: 'button-assertive',
        todayButtonType: 'button-default',
        closeButtonType: 'button-default',
        inputDate: $scope.datepickerParty.inputDate,
        mondayFirst: true,
        weekDaysList: weekDaysList,
        monthList: monthList,
        templateType: 'popup',
        showTodayButton: 'true',
        modalHeaderColor: 'bar-positive',
        modalFooterColor: 'bar-positive',
        from: new Date(2015, 10, 1),
        callback: function (val) {  //Mandatory
            datePickerParty(val);
        },
        dateFormat: 'dd-MM-yyyy',
        closeOnSelect: false,
    };

    var datePickerParty = function (val) {
        if (typeof(val) === 'undefined') {
            console.log('No date selected');
        } else {
            $scope.datepickerParty.inputDate = val;
        }
    };
    var datePickerCallbackPopup = function (val) {
        if (typeof(val) === 'undefined') {
            console.log('No date selected');
        } else {
            $scope.datepickerObject.inputDate = val;
        }
    };
    var datePickerInitial = function (val) {
        if (typeof(val) === 'undefined') {
            console.log('No date selected');
        } else {
            $scope.datepickerStart.inputDate = val;
        }
    };
    var datePickerFinal = function (val) {
        if (typeof(val) === 'undefined') {
            console.log('No date selected');
        } else {
            $scope.datepickerEnd.inputDate = val;
        }
    };

    $scope.showLoading = function () {
        $ionicLoading.show({
            template: '<span class="loading-puff modal-anim"><img src="img/puff.svg" alt=""></span>'
        });
    };
    $scope.hideLoading = function () {
        $ionicLoading.hide();
    };


    $scope.mainPanics = [];
    $scope._cards = {
        phone: "",
        "panictypes": [{
            label: "Chamar Supervisor",
            val: false
        }, {
            label: "Assalto em andamento",
            val: false
        }, {
            label: "Problema de saúde",
            val: false
        }]
    };

    angular.forEach($scope._cards, function (valor, index) {
        angular.forEach(valor, function (v, i) {
            $scope.mainPanics.push(v.label);
        });
    });

    $scope.cards = [{
        phone: "",
        "panictypes": [{
            label: "Chamar Supervisor",
            val: false
        }, {
            label: "Assalto em andamento",
            val: false
        }, {
            label: "Problema de saúde",
            val: false
        }]
    }];

    $scope.noPanicType = true;
    $scope.invalidForm = true;
    $scope.addCard = function () {
        $scope.cards.push(angular.copy($scope._cards));
        $scope.noPanicType = true;
    }

    $scope.savePanic = function (phoneValid, formInvalid) {
        console.log(formInvalid);

        if (formInvalid == false) {
            $scope.invalidForm = false;
        } else {
            $scope.invalidForm = true;
        }

        if (typeof phoneValid !== 'undefined') {

            //
            //$timeout(function () {
            //    // console.log(phoneValid);
            $scope.sendSettings();
            //}, 2000);
        }
    }


    $scope.panicOpt = function () {
        angular.forEach($scope.cards, function (key, value) {

            $scope.sendSettings();

            var trues = $filter("filter")(key.panictypes, {val: true});
            console.log(trues.length);

            if (trues.length >= 1) {
                $scope.noPanicType = false;
            } else {
                $scope.noPanicType = true;
            }
        });
        // $scope.sendSettings();
    }

    $rootScope.panicWarning = $scope.panicWarning = function () {
        var alertPopup = $ionicPopup.alert({
            title: 'Você precisa adicionar números para <b>' + missingLabel + '</b> no botão de pânico',
            cssClass: 'popStyle'
        });
        alertPopup.then(function (res) {
            $scope.openModalSettings();
        });
    }

    var savedPanics = [];
    if (JSON.parse(localStorage.getItem('panic')) == null) {
        localStorage.setItem('panic', JSON.stringify(savedPanics));
    }

    var trueAmount = [];
    var trueLabels = [];
    var onlyLabels = [];

    $scope.panicPopup;
    $scope.totaltrues;
    $scope.eachTrueLabel;
    $scope.cards = JSON.parse(localStorage.getItem('panic'));

    angular.forEach($scope.cards, function (value, key) {
        var eachTrue = $filter("filter")(value.panictypes, {val: true});

        trueLabels.push(eachTrue);
        trueAmount.push(eachTrue.length);

        $scope.totaltrues = trueAmount.reduce(function (a, b) {
            return a + b
        });
    });

    angular.forEach(trueLabels, function (v, k) {
        console.warn('---------------', v);
        angular.forEach(v, function (vl, ky) {
            onlyLabels.push(vl.label);
        });
    });

    var missingPanicArr = _.difference($scope.mainPanics, onlyLabels);
    var missingLabel = _.toString(missingPanicArr);

    missingLabel.replace(',', ', ');

    console.log(missingLabel);

    if ($scope.totaltrues < 3) {
        $scope.panicWarning();
    }


    /**
     * Define um listener para mudanças nos cards serem salvas imediatamente no localStorage
     */
    $scope.$watch(function (scope) {
        return scope.cards;
    }, function (a) {
        console.warn('Assistindo a mudanças no $scope.cards', a)
        //localStorage.setItem('panic', JSON.stringify($scope.cards));
        $scope.sendSettings();
    }, true);


    $scope.sendSettings = function () {
        if ($scope.invalidForm == false && $scope.noPanicType == false) {
            console.log($scope.cards);

            localStorage.setItem('panic', JSON.stringify($scope.cards));
        }

        // $http({
        //     url: domain + '/user/addPhones',
        //     method: 'POST',
        //     data: {
        //         cards: $scope.cards,
        //     }
        // }).then(function (data) {
        //     var data = data.data;

        //
        //     // console.log(data);
        // }, function (response) {
        //     $ionicPopup.alert({
        //         title: 'Ocorreu um erro, tente novamente mais tarde.',
        //         cssClass: 'popStyle'
        //     });
        // })
    }


    window.$filter = $filter;

    $scope.loadSettings = function () {
        $scope.cards = $filter('cards')(JSON.parse(localStorage.getItem('panic')));
        $scope.invalidForm = false;
        $scope.noPanicType = false;

        angular.forEach($scope.cards, function (value, key) {
            console.log(value);
        })
    }


    $scope.removeCard = function (card) {

        if (card.phone == "") return;
        if (card.phone.length < 10) return;
        var filtered = $filter('filter')(card.panictypes, {val: true});

        if (filtered.length == 0) {
            return;
        }


        console.log('remove card', card)

        $scope.cards = JSON.parse(localStorage.getItem('panic'));
        angular.forEach($scope.cards, function (i, v) {
            console.log(i.phone + i.panictypes == card.phone + card.panictypes);
            if (i.phone + i.panictypes == card.phone + card.panictypes) {
                $scope.cards.splice(v, 1);
            }
        });
        localStorage.setItem('panic', JSON.stringify($scope.cards));
    }

    $scope.panicPress = function () {

        var panic = JSON.parse(localStorage.getItem('panic'));

        /**

         @todo
         **/


        if (panic.length == 0) {
            $rootScope.panicWarning();
            return;
        }


        $rootScope.panicPopup = $ionicPopup.show({
            templateUrl: 'templates/choose-panic.html',
            scope: $scope,
            cssClass: 'panicPopStyle'
        });
    }

// LOGIN FORM
    $scope.processForm = function () {
        var username = this.loginForm.user;
        $scope.password = this.loginForm.password;

        $scope.isSaving = true;

        $http({
            url: domain + '/login',
            method: 'POST',
            data: {
                login: username,
                pwd: $scope.password
            }
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                localStorage.setItem('user', JSON.stringify(data));
                $scope.logged = true;
                $scope.checkLogin();
                $scope.isSaving = false;
            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.isSaving = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.isSaving = false;
        })
    }

// CONCIERGE FORM - ZELADORIA
    $scope.requestConcierge = function () {
        var user = JSON.parse(localStorage.getItem('user'));

        $scope.savingConcierge = true;

        $http({
            url: domain + '/concierge',
            method: 'POST',
            data: {
                userObj: user,
                uID: user.data.USER_ID,
                uName: user.data.USER_NAME,
                uEmail: user.data.USER_EMAIL,
                uMobile: user.data.USER_CEL,
                cName: user.data.NOME_ZEL,
                cEmail: user.data.EMAIL_ZEL,
                cMobile: user.data.CELULAR_ZEL,
                condominium: user.data.ID_CONDOMINIO
            }
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {

                // senha solicitada
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingConcierge = false;
            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingConcierge = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingConcierge = false;
        })
    };

// ACESSOS FORM
    $scope.allowForm = function (isValid) {
        var thisForm = this;

        console.groupCollapsed('$scope.allowForm ==> this');
        console.log('$scope.allowForm ==> this', this)
        console.warn(' this.acessosForm', this.acessosForm)
        console.warn(' this.onlyExiting', this.onlyExiting)
        console.groupEnd();


        var user = JSON.parse(localStorage.getItem('user'));

        var allowDateVal = $scope.datepickerObject.inputDate
        var horarioVal = this.acessos.hora;

        var allowDate = moment(allowDateVal).format('YYYY/MM/DD');
        var horario = moment(horarioVal).format('HH:mm');
        var phone = $scope.data.phoneSelected;
        var qtdade = this.acessos.qtd;

        console.log(phone);

        var platform = platform || 'platform';
        var device = device || 'device';

        $scope.savingAcess = true;


        if (platform.indexOf("iPhone") == 0) {
            platform = 'iPhone';
        } else {
            platform = 'Android';
        }

        $scope.reset = function () {

            thisForm.acessos.hora = "";
            thisForm.acessos.phone = "";

            thisForm.onlyExiting = false;


            $scope.accessOptions = [
                {name: '1', qtd: '1'},
                {name: '2', qtd: '2'},
                {name: '3', qtd: '3'},
                {name: '4', qtd: '4'},
                {name: '5', qtd: '5'},
            ];

            $scope.acessos = {qtd: $scope.accessOptions[0].qtd};

            $scope.contatoSelecionado = '';
            $rootScope.contatoSelecionado = '';

        }

        $scope.allowAcessosForm = {
            userObj: user,
            user: user.data.USER_ID,
            dataIni: allowDate,
            horaIni: horario,
            qtde: qtdade,
            celular: $filter('telPost')($scope.data.phoneSelected),
            tipo: 'A',
            plataforma: platform,
            device: device,
            name: $rootScope.accessVisitorsSelected.name,
            onlyExiting: this.onlyExiting
        }


        $http({
            url: domain + '/access',
            method: 'POST',
            data: $scope.allowAcessosForm
        }).then(function (data) {

            console.warn('HTTP enviando esses dados:', $scope.allowAcessosForm)


            var data = data.data;

            if (data.status == 'SUCCESS') {
                // acesso liberado
                var alertPopup = $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                alertPopup.then(function (res) {
                    $scope.reset();
                });
                $scope.savingAcess = false;

            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingAcess = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingAcess = false;
        })
    };

// RELATORIOS FORM
    $scope.relatorioForm = {};
    $scope.gerarRelatorio = function () {
        var user = JSON.parse(localStorage.getItem('user'));

        $scope.relatorioForm = {
            user: user.data.USER_ID,
            nome: user.data.USER_NAME,
            email: user.data.USER_EMAIL,
            celular: user.data.USER_CEL,
            dataIni: moment($scope.datepickerStart.inputDate).format('YYYY/MM/DD'),
            dataFim: moment($scope.datepickerEnd.inputDate).format('YYYY/MM/DD')
        };

        $scope.savingReport = true;

        $http({
            url: domain + '/reports',
            method: 'POST',
            data: $scope.relatorioForm
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                // sucesso
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingReport = false;
            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingReport = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingReport = false;
        })
    }

// CHANGE PASS FORM
    $scope.changeForm = {};
    $scope.changePassForm = function () {
        var user = JSON.parse(localStorage.getItem('user'));

        $scope.changeForm = {
            login: user.data.USER_CEL,
            pwd: this.changeForm.pass,
            newPwd: this.changeForm.passNew
        };
        $scope.savingPass = true;

        $http.post(
            domain + '/changepwd',
            $scope.changeForm
        ).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                var alertPopup = $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                alertPopup.then(function (res) {
                    $scope.closeModalChange();
                });
                $scope.savingPass = false;

                document.getElementById('currentPass').value = '';
                document.getElementById('newPass').value = '';
                document.getElementById('confirmPass').value = '';

            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.savingPass = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.savingPass = false;
        });
    }

// RECOVER PASS FORM
    $scope.forgotForm = {};

    $scope.recoverForm = function () {

        $scope.forgotForm = {
            email: this.forgotForm.passEmail
        };
        $scope.sendingPass = true;

        $http({
            url: domain + '/forgot',
            method: 'POST',
            data: $scope.forgotForm
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                var alertPopup = $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                alertPopup.then(function (res) {
                    $scope.closeModalForgot();
                });
                $scope.sendingPass = false;

                document.getElementById('emailForgot').value = '';

            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.sendingPass = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });
            $scope.sendingPass = false;
        });
    }
})
