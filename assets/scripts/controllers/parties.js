app.controller("partiesController", function ($scope, $ionicPopup, $http, $rootScope, $localstorage, $filter) {

    window.scopeParties = $scope;

    $rootScope.$watch('visitorsSelected', function(visitors){
        $scope.visitorsSelected = visitors;
    });


    window.$local = $localstorage.getObject;

    //console.log('after', $scope)

    // navigator.contacts.pickContact(function(contact){
    //     console.log('The following contact has been selected:' + JSON.stringify(contact));
    // },function(err){
    //     console.log('Error: ' + err);
    // });

    $scope.partyForm = {};
    $scope.inputs = [];
    $scope.addBlocker = false;
    $scope.addMoreBlocker = false;
    $scope.invalidField = false;
    $scope.setInvalidQtd = false;
    $scope.emptyContact = true;

    if($scope.visitorsSelected.length < 1){
        $scope.emptyContact = true;
    } else {
        $scope.emptyContact = false;
    }

    $scope.addRow = function (form) {
        if (form.amount.$modelValue < 1 || form.amount.$modelValue > 5) {
            $scope.invalidField = true;
        } else {
            $scope.invalidField = false;
        }

        if (form.celular.$valid && $scope.addBlocker == false) {
            $scope.inputs.push({});
            $scope.addBlocker = true;
        }
    }

    $scope.addMoreRows = function (form, quant) {
        var phone = form.cellphone.$modelValue;
        var qtd = form.qtdade.$modelValue;
        $scope.addedFirst = false;

        if (phone !== '' && qtd !== null && $scope.addMoreBlocker == false) {
            $scope.inputs.push({});
            $scope.addMoreBlocker = true;
            $scope.addedFirst = true;
        }

        if (form.cellphone.$dirty && form.qtdade.$dirty && $scope.addedFirst == false) {
            $scope.inputs.push({});
        }

        angular.forEach($scope.inputs, function (v, k) {
            if (v.phone == undefined && v.qtd < 1 || v.phone == undefined && v.qtd > 5) {
                $scope.addMoreBlocker = true;
            }
        });

        if (quant < 1 || quant > 5) {
            $scope.setInvalidQtd = true;
        } else {
            $scope.setInvalidQtd = false;
        }

    }

    var $_parentScope = $scope;

    $scope.partyList = function (isValid, form) {
        var thisForm = this;

        //console.log($_parentScope.visitorsSelected)
        var user = JSON.parse(localStorage.getItem('user'));
        var convidadosPost = '';

        $scope.convidados = [];


        if ($scope.visitorsSelected.length < 1) {
            $ionicPopup.alert({
                title: 'Por favor, selecione, pelo menos, um contato.',
                cssClass: 'popStyle'
            });

            return false
        }


        //$scope.convidados = [{
        //phone: this.partyForm.phone,
        //qtd: this.partyForm.qtd
        //}];

        //convidadosPost = this.partyForm.phone + "," + this.partyForm.qtd + ";";

        //console.error('convidadosPost', convidadosPost)

        angular.forEach($scope.visitorsSelected, function (value, key) {
            $scope.moreGuests = {
                phone: value.phoneSelected,
                qtd: value.tickets
            };

            if (value.phoneSelected !== null && typeof value.phoneSelected !== "undefined" && typeof value.tickets !== "undefined") {
                $scope.convidados.push($scope.moreGuests);

                console.log('==========================')
                clog(value)

                convidadosPost += $filter('telPost')(value.phoneSelected) + "," + value.tickets + "," + value.name + ";";
            }
        });

        console.warn('SCOPE.CONVIDADOSS', $scope.convidados)
        console.warn('convidadosPost', convidadosPost)

        //
        //angular.forEach($scope.inputs, function (value, key) {
        //    $scope.moreGuests = {
        //        phone: value.phone,
        //        qtd: value.qtd
        //    };
        //
        //    if (value.phone !== undefined || value.qtd !== undefined) {
        //        $scope.convidados.push($scope.moreGuests);
        //        convidadosPost += value.phone + "," + value.qtd + ";";
        //    }
        //});

        $scope.reset = function () {
            thisForm.partyForm.hora = "";
            thisForm.partyForm.phone = "";
            thisForm.partyForm.qtd = "";
            thisForm.partyForm.horario.$touched = false;
            $scope.visitorsSelected = [];
            $rootScope.visitorsSelected = [];
            $scope.addBlocker = false;
            $scope.addMoreBlocker = false;
        }

        var platform = platform || 'plataforma';
        var device = device || 'device';
        if (platform.indexOf("iPhone") == 0) {
            platform = 'iPhone'
        } else {
            platform = 'Android'
        }

        $scope.festaForm = {
            userObj: user,
            user: user.data.USER_ID,
            dataIni: moment($scope.datepickerParty.inputDate).format('YYYY/MM/DD'),
            horaIni: moment(this.partyForm.hora).format('HH:mm'),
            celular: convidadosPost,
            tipo: 'F',
            plataforma: platform,
            device: device
        };

        console.log($scope.festaForm);

        $rootScope.savingParty = true;

        $http({
            url: domain + '/access',
            method: 'POST',
            data: $scope.festaForm
        }).then(function (data) {
            var data = data.data;

            if (data.status == 'SUCCESS') {
                // sucesso
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $scope.reset();

                $rootScope.visitorsSelected = [];

                $rootScope.savingParty = false;
            } else {
                $ionicPopup.alert({
                    title: data.message,
                    cssClass: 'popStyle'
                });
                $rootScope.savingParty = false;
            }
        }, function (response) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro, tente novamente mais tarde.',
                cssClass: 'popStyle'
            });

            $rootScope.savingParty = false;
            $scope.inputs = [];
            $scope.visitorsSelected = [];
            $scope.addBlocker = false;
            $scope.addMoreBlocker = false;
        });
    }


});
