app.service('PanicService', PanicService);

PanicService.$inject = ['$localstorage', 'CallService'];

function PanicService($localstorage, CallService) {
    return {
        phones: null,

        /**
         *
         * @returns {Array}
         */
        get: function () {
            var panic = $localstorage.getObject('panic'),
                phones = [];

            _.each(panic, function (obj) {
                phones.push({
                    'phone': obj.phone,
                    'actions': _.map(_.filter(_.each(obj.panictypes, function (type) {
                        type.label = type.label.slugify();
                        return type;
                    }), {val: true}), 'label')
                });
            });

            return this.phones = phones;
        },

        /**
         *
         * @param type
         * @returns {Array}
         */
        getPhonesByAction: function (type) {
            var type = type.slugify(),
                panic = this.get();

            var filter = _.filter(panic, function (val) {
                return val.actions.indexOf(type) !== -1;
            });

            if (Object.prototype.toString.apply(filter) === '[object Array]') {
                var phones = [];
                filter.forEach(function (value, key) {
                    phones.push(value.phone);
                })
                return phones;
            }

            return filter;
        },

        /**
         *
         * @param panicType
         * @returns {*}
         */
        call: function (panicType) {
            console.info('PanicService.call() called');

            return CallService.call({
                action: 'panic',
                reason: panicType.slugify(),
                phones: this.getPhonesByAction(panicType)
            });
        }
    }
}