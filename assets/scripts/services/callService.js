app.factory('CallService', CallService);

CallService.$inject = ['$httpParamSerializerJQLike', 'UserService'];

function CallService($httpParamSerializerJQLike, UserService) {
    return {

        /**
         * @returns Object
         */
        user: function () {
            return UserService.get().data;
        }(),

        /**
         * @returns Object
         */
        panic: function () {
            return UserService.panic()
        }(),

        /**
         *
         * @param type
         * @param dataToUrl
         * @param phone
         * @returns Promise
         */
        request: function (type, dataToUrl, phone) {
            console.info('CallService.request() called with params:', JSON.stringify(arguments, null, 2));

            // var answerUrl = 'http://vihu.monstro.io/api/say/panic?' + dataToUrl;
            var answerUrl = 'http://vihu.monstro.io/calls/say?' + dataToUrl;
            // var answerUrl = 'http://vihu.monstro.io/ura.xml';

            console.warn(answerUrl);

            return this.requested = jQuery.ajax({
                url: "https://rest.nexmo.com/call/json",
                type: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                contentType: "application/x-www-form-urlencoded",
                data: {
                    "api_secret": "f17108ba7fbb1227",
                    "to": phone,
                    "api_key": "0f398013",
                    "answer_url": answerUrl,
                    "from": "551149331221",
                    'machine_detection': true,
                    'machine_timeout': '400ms',
                    'status_method': 'POST',
                    'status_url': 'http://vihu.monstro.io/calls/callback',
                    'error_method': 'POST',
                    'error_url': 'http://vihu.monstro.io/calls/callback'
                },
            })
        },

        /**
         * Hora da piadinha
         *
         * @param params
         * @returns {*}
         */
        call: function (params) {
            console.info('CallService.call() called with params:', params);

            if (params.action == "panic") {
                return this.callPanic(params);
            }
        },

        /**
         * Leandrão reclamão
         *
         * @param params
         */
        callPanic: function (params) {
            var $this = this;

            params['message'] = 'x';
            params['name'] = this.user.USER_NAME;
            params['apartamento'] = this.user.TORRE_APTO;
            params['condominio'] = this.user.CONDOMINIO;
            params['type'] = params['reason']; // @todo mover para reason


            console.log('================================================================', params);

            _.each(params.phones, function (phone, key) {
                phone = '55' + phone.toString();

                var request = $this.request('panic', $httpParamSerializerJQLike(params), phone);

                console.warn('Phone ' + key + ' called', '\nPhone = ', phone, '\nRequest = ', request)

            })
        },
    }
}