app.service('UserService', function ($localstorage) {
    return {
        get: function () {
            return $localstorage.getObject('user');
        },
        panic: function () {
            return $localstorage.getObject('panic');
        }
    }
})